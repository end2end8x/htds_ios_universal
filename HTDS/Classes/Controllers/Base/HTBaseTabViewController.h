//
//  HTBaseTabViewController.h
//  HTDS
//
//  Created by Anhpt67 on 7/6/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "InputViewController.h"
#import "PLSegmentView.h"
#import "HTDoccumentTableView.h"
#import "LGActionSheet.h"


@interface HTBaseTabViewController : InputViewController <PLSegmentedViewDelegate,UIActionSheetDelegate,LGActionSheetDelegate> {
    NSString *dateCharg;
}


@property (weak, nonatomic) IBOutlet UIScrollView *homeScrollView;
@property (weak, nonatomic) IBOutlet PLSegmentView *segmentView;

@property (weak, nonatomic) IBOutlet HTDoccumentTableView *reportTableView;
@property (weak, nonatomic) IBOutlet HTDoccumentTableView *reportBookMarkTableView;
@property (weak, nonatomic) IBOutlet HTDoccumentTableView *reportHistoryTableView;


@property (weak, nonatomic) IBOutlet UILabel *dateChargLable;
@property (weak, nonatomic) IBOutlet UILabel *actionLable;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIButton *dateChargeButton;



- (void) getListReports;

- (void) getListReportHistory;


// refresh control

@property (nonatomic,strong) UIRefreshControl *refreshReportData;
@property (nonatomic,strong) UIRefreshControl *refreshReportBookMarkData;
@property (nonatomic,strong) UIRefreshControl *refreshReportHistoryData;

- (void) processResponsiveWithTableView:(BaseTableView*) tableView
                            andResponse:(id) responseObject
                      andRefreshControl:(UIRefreshControl*) refresshControl;

- (void) hidenloadingAndRefressh:(BaseTableView*) tableView
               andRefreshControl:(UIRefreshControl*) refresshControl;

@end
