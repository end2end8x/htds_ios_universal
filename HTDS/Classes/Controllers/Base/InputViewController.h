//
//  PLInputViewController.h
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseTextField.h"
#import "HTTextFieldLoggin.h"


@interface InputViewController : BaseViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSArray *textFields;

@property (weak, nonatomic) IBOutlet BaseTextField *emailTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *passwordTextField;


@property (nonatomic, weak) IBOutlet BaseTextField *confirmPasswordTextField;
@property (nonatomic, weak) IBOutlet BaseTextField *oldPasswordTextField;
@property (strong, nonatomic) IBOutlet BaseTextField *viewSearch;

@end
