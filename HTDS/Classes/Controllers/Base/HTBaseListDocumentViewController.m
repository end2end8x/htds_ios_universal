//
//  HTBaseListDocumentViewController.m
//  HTDS
//
//  Created by TuanTD on 7/14/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseListDocumentViewController.h"
#import "MBProgressHUD.h"
#import "SVPullToRefresh.h"
#import "HTDocumentModel.h"
#import "HTReportBookMarkManaged.h"
#import "PLFunctions.h"
#import "PLConstants.h"

@interface HTBaseListDocumentViewController ()

@end

@implementation HTBaseListDocumentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) registerNotification {
    [super registerNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBookMarks:) name:kPLCheckBookMarksNotification object:nil];
}

- (void) unregisterNotification {
    [super unregisterNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) updateBookMarks:(NSNotification*) notf {
    [self unregisterNotification];
    [self registerNotification];
    HTDocumentModel *document = (HTDocumentModel*)notf.object;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(icReportId = %@)",[NSNumber numberWithInteger:document.icReportId]];
    
    NSArray *listReport = [self.tableView.listData filteredArrayUsingPredicate:predicate];
    if (listReport.count > 0) {
        HTDocumentModel *reportModel = (HTDocumentModel*)[listReport firstObject];
        reportModel.isBookmark = document.isBookmark;
        [_tableView reloadData];
    }
}

- (void) setupUI {
    [super setupUI];
    self.tableView.controller = self;
    
    __weak HTBaseListDocumentViewController *weakSelf = self;
    [weakSelf.tableView addInfiniteScrollingWithActionHandler:^{ // load more news
        [weakSelf loadMoreData];
        weakSelf.tableView.showsInfiniteScrolling = YES;
    }];
    
    // refres datata
    self.refreshReportData = [[UIRefreshControl alloc] init];
    [self.refreshReportData addTarget:self action:@selector(pullToRefressData) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:self.refreshReportData];
    
}

- (void) pullToRefressData {
    [super pullToRefressData];
    [_tableView.listData removeAllObjects];
    [_tableView reloadData];
    [self loadMoreData];
}

- (void) refreshData {
    [super refreshData];
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    
    [self.tableView.listData removeAllObjects];
    [self.tableView reloadData];
    [self loadMoreData];
}

- (void) loadMoreData {
    [super loadMoreData];
}

#pragma mark - respont from server

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    [_tableView.listData addObjectsFromArray:[HTDocumentModel parserListObject:responseObject]];
    [_tableView reloadData];
    [self hidenloadingAndRefressh];
    
    
    NSMutableArray *array = (NSMutableArray*) responseObject;
    if (array && [array count] ==0) {
         showMBProgressHUD(self.view, @"MESSAGE_NODATA", NO, MBProgressHUDModeText, HTToastTimeOut);
    }
   
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    [self hidenloadingAndRefressh];
}

- (void) hidenloadingAndRefressh {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [_refreshReportData endRefreshing];
    [_tableView.infiniteScrollingView stopAnimating];
    _tableView.showsInfiniteScrolling = !(_tableView.listData.count%LIMIT > 0);
}


@end
