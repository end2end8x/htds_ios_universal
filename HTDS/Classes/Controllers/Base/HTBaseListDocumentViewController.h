//
//  HTBaseListDocumentViewController.h
//  HTDS
//
//  Created by TuanTD on 7/14/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseViewController.h"
#import "HTDoccumentTableView.h"

@interface HTBaseListDocumentViewController : BaseViewController

@property (weak, nonatomic) IBOutlet HTDoccumentTableView *tableView;
@property (nonatomic,strong) UIRefreshControl *refreshReportData;

- (void) hidenloadingAndRefressh;

@end
