//
//  PLBaseViewController.h
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LostConnectionView.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "SlideNavigationController.h" 
#import "RequestOperationManager.h"
#import "MBProgressHUD.h"



@interface BaseViewController : UIViewController <LostConnectionViewDelegate, GCDAsyncSocketDelegate,SlideNavigationControllerDelegate> {
    BOOL isRefreshData;
}

@property (nonatomic, assign) BOOL shouldDisplayLostConnectionView;
@property (nonatomic, assign) BOOL shouldDisplayBackButton;

- (void) executeWithWithParams:(NSData*) params
                       withTag:(NSInteger) tagRequest;

#pragma mark - Timer
- (void) stopTimer;
- (void) onTick;

#pragma mark -  NavigationBar Items and Actions
- (void)setNavigationBarTitleImage;
- (void)addBackButtonItem;
- (void)addSearchButtonItem;
- (void) addPenSignButtonItem;
- (void)addCloseButtonItem;
- (void)addRightBarItems;
- (IBAction)backButtonPressed;
- (IBAction)searchButtonPressed;
- (IBAction)closeButtonPressed;
- (IBAction)penSignButtonPressed;
- (void) downLoadDoccument;

#pragma mark -  Notification and observer
- (void)setupData;
- (void)setupUI;
- (void)registerNotification;
- (void)unregisterNotification;

- (void) showBanerErrorNetWork;
- (void)checkInternetAndRefreshData;
- (void)refreshData;
- (void) pullToRefressData;
- (void)loadMoreData;

#pragma mark -  GoogleAnalytics
- (void)sendScreenViewWithName:(NSString *)screenName;
- (void)sendHitForCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value;

#pragma mark - respont request

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag;

- (void) requestSuccess:(NSInteger) errorCode
             andMessage:(NSString*) message
      andResponseObject:(id) responseObject
          andTagRequest:(NSInteger) tag;

- (void) requestFail:(NSInteger) errorCode
          andMessage:(NSString*) message
   andResponseObject:(id) responseObject
       andTagRequest:(NSInteger)tag;

#pragma mark - file

- (void) showFileWithPath:(NSString*) strPath andShowSingButton:(BOOL) isShow andDocument:(id) documentModel;

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;

@end
