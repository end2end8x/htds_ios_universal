//
//  HTBaseTabViewController.m
//  HTDS
//
//  Created by Anhpt67 on 7/6/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseTabViewController.h"
#import "MBProgressHUD.h"
#import "SVPullToRefresh.h"
#import "HTDocumentModel.h"
#import "HTCategoriesViewController.h"
#import "HTBaseCategories.h"
#import "HTReportBookMarkManaged.h"
#import "UIViewController+MJPopupViewController.h"
#import "DictionaryBuilder.h"


#import "PLConstants.h"
#import "HTPreferences.h"
#import "HTAppDelegate.h"
#import "PLFunctions.h"
#import "Defines.h"
#import "HTEmployViewController.h"
#import "HTManagerViewController.h"
#import "HTHomeViewController.h"
#import "HTViettelSignViewController.h"

@interface HTBaseTabViewController ()

@end

@implementation HTBaseTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.reportTableView.controller = self;
    self.reportBookMarkTableView.controller = self;
    self.reportHistoryTableView.controller = self;
    
    
    __weak HTBaseTabViewController *weakSelf = self;
    [weakSelf.reportTableView addInfiniteScrollingWithActionHandler:^{ // load more news
        [weakSelf loadMoreData];
        weakSelf.reportTableView.showsInfiniteScrolling = YES;
    }];
    
    [weakSelf.reportHistoryTableView addInfiniteScrollingWithActionHandler:^{ // load more mostview
        [weakSelf loadMoreData];
        weakSelf.reportHistoryTableView.showsInfiniteScrolling = YES;
    }];
    
    // refres datata
    self.refreshReportData = [[UIRefreshControl alloc] init];
    [self.refreshReportData addTarget:self action:@selector(pullToRefressData) forControlEvents:UIControlEventValueChanged];
    [self.reportTableView addSubview:self.refreshReportData];
    
    self.refreshReportBookMarkData = [[UIRefreshControl alloc] init];
    [self.refreshReportBookMarkData addTarget:self action:@selector(pullToRefressData) forControlEvents:UIControlEventValueChanged];
    [self.reportBookMarkTableView addSubview:self.refreshReportBookMarkData];
    
    self.refreshReportHistoryData = [[UIRefreshControl alloc] init];
    [self.refreshReportHistoryData addTarget:self action:@selector(pullToRefressData) forControlEvents:UIControlEventValueChanged];
    [self.reportHistoryTableView addSubview:self.refreshReportHistoryData];
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpUIforRotate];
}

- (void) registerNotification {
    [super registerNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBookMarks:) name:kPLCheckBookMarksNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetAllDataAndBackStatus) name:kPLUserDidLoginNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetAllDataAndRefressh) name:kPLSignatureReportNotification object:nil];
}

- (void) unregisterNotification {
    [super unregisterNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) updateBookMarks:(NSNotification*) notf {
    [self unregisterNotification];
    [self registerNotification];
    HTDocumentModel *document = (HTDocumentModel*)notf.object;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(icReportId = %@)",[NSNumber numberWithInteger:document.icReportId]];
    
    NSArray *listReport = [self.reportTableView.listData filteredArrayUsingPredicate:predicate];
    if (listReport.count > 0) {
        HTDocumentModel *reportModel = (HTDocumentModel*)[listReport firstObject];
        reportModel.isBookmark = document.isBookmark;
        [_reportTableView reloadData];
    }
    
    NSArray *listReportHistory = [self.reportHistoryTableView.listData filteredArrayUsingPredicate:predicate];
    if (listReportHistory.count > 0) {
        HTDocumentModel *reportHistoryModel = (HTDocumentModel*)[listReportHistory firstObject];
        reportHistoryModel.isBookmark = document.isBookmark;
        [_reportHistoryTableView reloadData];
    }
    
    [self getListReportBookMark];
}

- (void) setupUI {
    [super setupUI];
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    if ([self isKindOfClass:[HTViettelSignViewController class]]) {
        [self.segmentView setButtonTitles:@[[NSLocalizedString(@"NO_INVOICE", nil) uppercaseString],[NSLocalizedString(@"INVOICE", nil) uppercaseString]]];
        
        [self.headerView removeFromSuperview];
        
    } else {
        self.navigationItem.titleView = self.headerView;
        
        if (account.partnerUserType == PARTNER_TYPE_EMPLOY) {
            [self.segmentView setButtonTitles:@[[NSLocalizedString(@"DOCCUMENT", nil) uppercaseString],[NSLocalizedString(@"DOCCUMENT_REVIEWED", nil) uppercaseString]]];
        } else if (account.partnerUserType == PARTNER_TYPE_ADMIN) {
            if ([self isKindOfClass:[HTEmployViewController class]]) {
                [self.segmentView setButtonTitles:@[[NSLocalizedString(@"DOCCUMENT", nil) uppercaseString],[NSLocalizedString(@"DOCCUMENT_REVIEWED", nil) uppercaseString]]];
            } else {
                [self.segmentView setButtonTitles:@[[NSLocalizedString(@"DOCCUMENT", nil) uppercaseString],[NSLocalizedString(@"DOCCUMENT_SIGNED", nil) uppercaseString]]];
            }
        } else {
            [self.segmentView setButtonTitles:@[[NSLocalizedString(@"DOCCUMENT", nil) uppercaseString],[NSLocalizedString(@"DOCCUMENT_SIGNED", nil) uppercaseString]]];
        }
        
    }
    
    CGRect frame = self.reportHistoryTableView.frame;
    frame.origin.x = CGRectGetMaxX(self.reportTableView.frame);
    self.reportHistoryTableView.frame = frame;
    
    frame = self.reportBookMarkTableView.frame;
    frame.origin.x = CGRectGetMaxX(self.reportBookMarkTableView.frame);
    self.reportBookMarkTableView.frame = frame;
    self.homeScrollView.contentSize = CGSizeMake(self.homeScrollView.frame.size.width*2, 1);
    
}

- (void) setupData {
    [super setupData];
    dateCharg = @"";
}

#pragma mark - requestDataFromServer

- (void) resetDataWithTab {
    switch (self.segmentView.tag) {
        case TAB_REPORT: {
            [self.reportTableView.listData removeAllObjects];
            [self.reportTableView reloadData];
        }
            break;
        case TAB_REPORT_BOOKMARK: {
            [self.reportBookMarkTableView.listData removeAllObjects];
            [self.reportBookMarkTableView reloadData];
            
        }
            
            break;
        case TAB_REPORT_HISTORY: {
            [self.reportHistoryTableView.listData removeAllObjects];
            [self.reportHistoryTableView reloadData];
        }
            
            break;
            
        default:
            break;
    }
}

- (void) resetAllDataAndBackStatus {
//    showMBProgressHUD(self.view, @"resetAllDataAndBackStatus", NO, MBProgressHUDModeText, HTToastTimeOut);    
    [self.segmentView setSelectButtonAtIndex:TAB_REPORT userPressed:NO];
    dateCharg = @"";
    self.dateChargLable.text = NSLocalizedString(@"TITLE_DATE_CHARG", nil);
    [self resetAllDataAndRefressh];
}

- (void) resetAllDataAndRefressh {
    [self.reportTableView.listData removeAllObjects];
    [self.reportTableView reloadData];
    [self.reportBookMarkTableView.listData removeAllObjects];
    [self.reportBookMarkTableView reloadData];
    [self.reportHistoryTableView.listData removeAllObjects];
    [self.reportHistoryTableView reloadData];
    [self refreshData];
}

- (void) pullToRefressData {
    [super pullToRefressData];
    [self resetDataWithTab];
    [self loadMoreData];
    
}

- (void) refreshData {
    [super refreshData];
    [self unregisterNotification];
    [self registerNotification];
    BOOL shouldLoadData = NO;
    if (self.segmentView.tag == TAB_REPORT) {
        if ([HTPreferences sharedPreferences].loggedIn) {
            if (self.reportTableView.listData.count == 0) {
                shouldLoadData = YES;
                showMBProgressHUD(self.reportTableView, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
            }
        }
    } else if (self.segmentView.tag == TAB_REPORT_BOOKMARK) {
        if (self.reportBookMarkTableView.listData.count == 0) {
            shouldLoadData = YES;
            showMBProgressHUD(self.reportBookMarkTableView, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
        }
    } else if (self.segmentView.tag == TAB_REPORT_HISTORY) {
        if (self.reportHistoryTableView.listData.count == 0) {
            shouldLoadData = YES;
            showMBProgressHUD(self.reportHistoryTableView, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
        }
    }
    
    if (shouldLoadData) {
        [self loadMoreData];
    }
}

- (void) loadMoreData {
    [super loadMoreData];
    
    if (self.segmentView.tag == TAB_REPORT) {
        [self getListReports];
    } else if (self.segmentView.tag == TAB_REPORT_BOOKMARK) {
        [self getListReportBookMark];
    } else if (self.segmentView.tag == TAB_REPORT_HISTORY) {
        [self getListReportHistory];
    }
}

- (void) getListReports {
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSString *status = @"";
    
    if (account.partnerUserType == PARTNER_TYPE_EMPLOY) {
        status = [NSString stringWithFormat:@"%d", STATUS_WAITING_REVIEW];
    } else if (account.partnerUserType == PARTNER_TYPE_MANAGER) {
        status = [NSString stringWithFormat:@"%d", STATUS_WAITING_SIGNATURE];
    } else if (account.partnerUserType == PARTNER_TYPE_ADMIN) {
        if ([self isKindOfClass:[HTEmployViewController class]]) {
            status = [NSString stringWithFormat:@"%d", STATUS_WAITING_REVIEW];
        } else if ([self isKindOfClass:[HTManagerViewController class]]) {
            status = [NSString stringWithFormat:@"%d", STATUS_WAITING_SIGNATURE];
        } else if ([self isKindOfClass:[HTHomeViewController class]]) {
            status = [NSString stringWithFormat:@"%d,%d", STATUS_WAITING_REVIEW, STATUS_WAITING_SIGNATURE];
        }
    } else if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        status = [NSString stringWithFormat:@"%d", STATUS_MANAGER_SIGN];
    }
    
    NSData *params = [DictionaryBuilder dictionarySearchAllReport:self.reportTableView.listData.count andLimit:LIMIT  andCycleDate:dateCharg andStatus:status];
    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_LIST_REPORT];
}

- (void) getListReportBookMark {
    [self.reportBookMarkTableView.listData removeAllObjects];
    [self.reportBookMarkTableView reloadData];
    self.reportBookMarkTableView.listData = [HTDocumentModel getListReportBookMark:dateCharg];
    [self.reportBookMarkTableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.reportBookMarkTableView animated:YES];
    [self.refreshReportBookMarkData endRefreshing];
}

- (void) getListReportHistory {
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSString *status = @"";
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        status = [NSString stringWithFormat:@"%d", STATUS_VIETTEL];
        NSData *params =   [DictionaryBuilder dictionarySearchAllReport:self.reportHistoryTableView.listData.count andLimit:LIMIT andCycleDate:dateCharg andStatus:status];
        [self executeWithWithParams:params withTag:REQUEST_FOR_GET_REPORT_HISTORY];
    } else {
        NSData *params = [DictionaryBuilder dictionarySearchUserHistory:self.reportHistoryTableView.listData.count andLimit:LIMIT andCycleDate:dateCharg andStatus:status];
        [self executeWithWithParams:params withTag:REQUEST_FOR_GET_REPORT_HISTORY];
    }
}

#pragma mark - scrollViewdelegate

- (void) segmentedViewDidSelectItemAtIndex:(NSInteger)index {
    self.segmentView.tag = index;
    CGRect frame = self.homeScrollView.frame;
    frame.origin.x = frame.size.width * index;
    frame.origin.y = 0;
    [self.homeScrollView scrollRectToVisible:frame animated:YES];
    [self refreshData];
}

// user scroll view
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.homeScrollView) {
        NSInteger selectedTab = scrollView.contentOffset.x / self.homeScrollView.frame.size.width;
        [self.segmentView setSelectButtonAtIndex:selectedTab userPressed:YES];
    }
}

#pragma mark - action

- (IBAction)dateButtonPressed:(id)sender {
    UIDatePicker *datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 100.f);
    
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:today];
    components.month--;
    components.day = 1;
    NSDate *dayOneInCurrentMonth = [gregorian dateFromComponents:components];
    
    [datePicker setDate:dayOneInCurrentMonth];
    
    
    [[[LGActionSheet alloc] initWithTitle:nil view:datePicker buttonTitles:@[@"Tất cả",@"Chọn"] cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil actionHandler:^(LGActionSheet *actionSheet, NSString *title, NSUInteger index) {
        
        if (index == 0) {
            dateCharg = @"";
            self.dateChargLable.text = title;
        } else {
            dateCharg = convertDateToString(datePicker.date,kPLFormatDateDD_MM_YY);
            self.dateChargLable.text = convertDateToString(datePicker.date,kPLFormatDateDD_MM_YY);
        }
        [self resetAllDataAndRefressh];
    } cancelHandler:^(LGActionSheet *actionSheet, BOOL onButton) {
        
    } destructiveHandler:nil] showAnimated:YES completionHandler:nil];
}

#pragma mark - respont from server

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    
    if (tag == REQUEST_FOR_GET_LIST_REPORT) {
        [self processResponsiveWithTableView:self.reportTableView andResponse:responseObject andRefreshControl:self.refreshReportData];
    } else if (tag == REQUEST_FOR_GET_REPORT_BOOKMARK) {
        [self processResponsiveWithTableView:self.reportBookMarkTableView andResponse:responseObject andRefreshControl:self.refreshReportBookMarkData];
    } else if (tag == REQUEST_FOR_GET_REPORT_HISTORY) {
        [self processResponsiveWithTableView:self.reportHistoryTableView andResponse:responseObject andRefreshControl:self.refreshReportHistoryData];
    }
    
    NSMutableArray *array = (NSMutableArray*) responseObject;
    if (array && [array count] ==0) {
        showMBProgressHUD(self.view, @"MESSAGE_NODATA", NO, MBProgressHUDModeText, HTToastTimeOut);
    }
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    if (tag == REQUEST_FOR_GET_LIST_REPORT) {
        [self hidenloadingAndRefressh:self.reportTableView andRefreshControl:self.refreshReportData];
    } else if (tag == REQUEST_FOR_GET_REPORT_BOOKMARK) {
        [self hidenloadingAndRefressh:self.reportBookMarkTableView andRefreshControl:self.refreshReportBookMarkData];
    } else if (tag == REQUEST_FOR_GET_REPORT_HISTORY) {
        [self hidenloadingAndRefressh:self.reportHistoryTableView andRefreshControl:self.refreshReportHistoryData];
    }
    showAleartWithMessage(message);
}

- (void) processResponsiveWithTableView:(BaseTableView*) tableView
                            andResponse:(id) responseObject
                      andRefreshControl:(UIRefreshControl*) refresshControl {
    [tableView.listData addObjectsFromArray:[HTDocumentModel parserListObject:responseObject]];
    [tableView reloadData];
    [self hidenloadingAndRefressh:tableView andRefreshControl:refresshControl];
}

- (void) hidenloadingAndRefressh:(BaseTableView*) tableView
               andRefreshControl:(UIRefreshControl*) refresshControl {
    [MBProgressHUD hideAllHUDsForView:tableView animated:YES];
    [refresshControl endRefreshing];
    [tableView.infiniteScrollingView stopAnimating];
    tableView.showsInfiniteScrolling = !(tableView.listData.count%LIMIT > 0);
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        
    } else {
        
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self setUpUIforRotate];
}

- (void) setUpUIforRotate {
    NSInteger index = self.segmentView.tag;
    [self setupUI];
    [self.segmentView setSelectButtonAtIndex:index userPressed:NO];
    self.segmentView.tag = index;
    CGRect frame = self.homeScrollView.frame;
    frame.origin.x = frame.size.width * index;
    frame.origin.y = 0;
    [self.homeScrollView scrollRectToVisible:frame animated:NO];
}

@end
