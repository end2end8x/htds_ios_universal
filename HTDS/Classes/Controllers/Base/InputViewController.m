//
//  PLInputViewController.m
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "InputViewController.h"
#import "HTSearchViewController.h"
#import "PLConstants.h"
#import "PLFunctions.h"

@interface InputViewController ()


@end


@implementation InputViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.shouldDisplayLostConnectionView = NO;
    }
    return self;
}

- (void) registerNotification {
    [super registerNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hidenKeybroad) name:kPLLeftMenuOpenNotification object:nil];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - action barbuttonItem

- (void) searchButtonPressed {
    [super searchButtonPressed];
    
    UIStoryboard *mainStoryBoad = [UIStoryboard storyboardWithName:@"Main" bundle:nil]
    ;    HTSearchViewController *searchViewController = [mainStoryBoad instantiateViewControllerWithIdentifier:NSStringFromClass([HTSearchViewController class])];
    searchViewController.shouldDisplayBackButton = YES;
    searchViewController.delegate = self;
    [self.navigationController pushViewController:searchViewController animated:YES];
}

//- (void) searchRequestKeyWord:(NSString*) keyWord {
//      // MARK: thực hiện search gọi API tại đây
//}

//- (void) hidenBlurView:(UITapGestureRecognizer *)recognizer {
//    [self.viewSearch resignFirstResponder];
//    UIView *blurView = [[PLAppDelegate sharedDelegate].window viewWithTag:kSearchBlurViewTag];
//    [blurView removeFromSuperview];
//}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger index = [self.textFields indexOfObject:textField];
    if (index < self.textFields.count - 1) {
        UITextField *nextField = [self.textFields objectAtIndex:index + 1];
        [nextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
//    if (textField == self.viewSearch) {
//        [self searchButtonPressed];
//    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    if (textField == self.viewSearch) {
//        [self searchButtonPressed];
//    } else {
        textFieldDidBeginEditing(textField, self.view);
//    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
//    if (textField == self.viewSearch) {
//        [self hidenBlurView:nil];
//    } else {
        textFieldDidEndEditing(textField, self.view);
//    } 
}

- (void) hidenKeybroad {
    [self.view endEditing:YES];
//    [self hidenBlurView:nil];
}

@end
