//
//  PLBaseViewController.m
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "BaseViewController.h"
#import "ALAlertBanner.h"
#import "HTDoccumentDetailViewController.h"
#import "NSDictionary+PL.h"
#import "NSString+SBJSON.h"
#import "ReaderViewController.h"
#import "PLConstants.h"
#import "Reachability.h"
#import "PLFunctions.h"
#import "HTAppDelegate.h"
#import "Defines.h"
#import "HTBaseTabViewController.h"
#import "HTViettelSignViewController.h"
#import "HTLoginViewController.h"
#import "HTDoccumentDetailViewController.h"
#import "HTPreferences.h"


@interface BaseViewController ()<ReaderViewControllerDelegate>

@end

@implementation BaseViewController

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.shouldDisplayLostConnectionView = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    RequestOperationManager *asynSocket = [RequestOperationManager sharedManager];
    asynSocket.delegate = self;
    if (![asynSocket isConnected] || [asynSocket isDisconnected]) {
        [asynSocket connecttoHost:self];
    }
    
    [self setupData];
    
    [self registerNotification];
    
    [self setupUI];
    
    
    
    [self checkInternetAndRefreshData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChangedNotification:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *controllerName = NSStringFromClass([self class]);
    NSString *controllerNameWithoutPrefixAndSuffix = [controllerName substringWithRange:NSMakeRange(2, controllerName.length - 14)];
    NSString *screenName = [NSString stringWithFormat:@"%@ screen", controllerNameWithoutPrefixAndSuffix];
    
    [[GAI sharedInstance].defaultTracker send:[[[GAIDictionaryBuilder createAppView] set:screenName
                                                                                  forKey:kGAIScreenName] build]];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.shouldDisplayBackButton) {
        [self addBackButtonItem];
    }
    //    if ([self.navigationController.viewControllers indexOfObject:self] > 0) {
    //        [self addBackButtonItem];
    //    } else {
    //        self.navigationItem.leftBarButtonItem = nil;
    //    }
}

- (void)dealloc{
    [self unregisterNotification];
}

#pragma mark - Orientations
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationPortrait;
//}
//
//- (BOOL)shouldAutorotate {
//    return NO;
//}
//
//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskPortrait;
//}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration

{
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        
    } else {
        
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
}

#pragma mark - Timer
- (void) stopTimer {
    if ([HTAppDelegate sharedDelegate].timer) {
        [[HTAppDelegate sharedDelegate].timer invalidate];
        [HTAppDelegate sharedDelegate].timer = nil;
        NSLog(@"stopTimer");
    }
}
- (void) onTick {
    if(self.view) {
        MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
        if (hud && hud.mode == MBProgressHUDModeDeterminate) {
            float progess = hud.progress;
            progess = progess + (0.1f / (float) HTTimerTimeOut);
            if (progess >= 1 || progess < 0) {
                [self stopTimer];
                return;
            }
            
            NSString *title = [NSString stringWithFormat:@"%@ %.01f", NSLocalizedString(@"LOADDING_LOGGING", nil), (HTTimerTimeOut  - (progess * HTTimerTimeOut))];
            
            if([self isKindOfClass:[HTDoccumentDetailViewController class]]) {
                title = [NSString stringWithFormat:@"%@ %.01f", NSLocalizedString(@"LOADDING_LOGGING_SIGNATURING", nil), (HTTimerTimeOut  - (progess * HTTimerTimeOut))];
            }
            
            hud.progress = progess;
            hud.labelText = title;
        } else {
            [self stopTimer];
        }
    } else {
        [self stopTimer];
    }
}

#pragma mark - NavigationBar Items
- (void)setNavigationBarTitleImage {
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_logo"]];
    self.navigationItem.titleView = titleImageView;
}

- (void)addBackButtonItem {
    UIImage *image = [UIImage imageNamed:@"img_ico_back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(11, 0, fminf(image.size.width * 2, 32), image.size.height)];
    [button addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [button setContentEdgeInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
    }
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)addSearchButtonItem {
    UIImage *image = [UIImage imageNamed:@"btn-search"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, fminf(image.size.width * 2, 32), image.size.height)];
    [button addTarget:self action:@selector(searchButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    if (_IsOS7OrNewer()) {
        [button setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -13)];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void) addPenSignButtonItem {
    UIImage *image = [UIImage imageNamed:@"icon_pen_sign"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, fminf(image.size.width * 2, 32), image.size.height)];
    [button addTarget:self action:@selector(penSignButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    if (_IsOS7OrNewer()) {
        [button setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -13)];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)addCloseButtonItem {
    UIImage *image = [UIImage imageNamed:@"icon-close"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, fminf(image.size.width * 2, 44), image.size.height)];
    [button addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    if (_IsOS7OrNewer()) {
        [button setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -15)];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)addRightBarItems {
    UIImage *penSignImage = [UIImage imageNamed:@"icon_pen_sign"];
    UIButton *buttonPenSign = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonPenSign setImage:penSignImage forState:UIControlStateNormal];
    [buttonPenSign setFrame:CGRectMake(0, 0, fminf(penSignImage.size.width * 2, 44), penSignImage.size.height)];
    [buttonPenSign addTarget:self action:@selector(penSignButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    if (_IsOS7OrNewer()) {
        [buttonPenSign setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -25)];
    }
    UIBarButtonItem *penSignItem = [[UIBarButtonItem alloc] initWithCustomView:buttonPenSign];
    
    UIImage *downloadImage = [UIImage imageNamed:@"icon_download"];
    UIButton *buttonDownload = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonDownload setImage:downloadImage forState:UIControlStateNormal];
    [buttonDownload setFrame:CGRectMake(0, 0, fminf(downloadImage.size.width * 2, 44), downloadImage.size.height)];
    [buttonDownload addTarget:self action:@selector(downloadButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    if (_IsOS7OrNewer()) {
        [buttonDownload setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -35)];
    } else {
        [buttonDownload setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -25)];
    }
    UIBarButtonItem *downloadItem = [[UIBarButtonItem alloc] initWithCustomView:buttonDownload];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:penSignItem,
                                               downloadItem, nil];
}

- (IBAction)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchButtonPressed {
    
}

- (IBAction)penSignButtonPressed {
    
}

- (IBAction)closeButtonPressed {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)downloadButtonPressed {
    
}

- (void) downLoadDoccument {
    
}

#pragma mark - Setup
- (void)setupData {
    
}

- (void)setupUI {
    if ([self isKindOfClass:[HTBaseTabViewController class]] ||
        [self isKindOfClass:[HTViettelSignViewController class]]) {
        [self addSearchButtonItem];
    }
}

- (void)registerNotification {
    
}

- (void)unregisterNotification {
    
}

#pragma mark - Load data
- (void)checkInternetAndRefreshData {
    
    BOOL hasInternetConnection = [[HTAppDelegate sharedDelegate] hasInternetConnection];
    if (hasInternetConnection) {
        [self refreshData];
    } else {
        if (self.shouldDisplayLostConnectionView) {
            [LostConnectionView showInController:self];
        }
    }
}

- (void)refreshData {
    // mang A
    // self.A = [NSArray new];
    //    [self loadMoreData];
}

- (void) pullToRefressData {
    
}

- (void)loadMoreData {
    
}

- (void) executeWithWithParams:(NSData*) params
                       withTag:(NSInteger) tagRequest {
    RequestOperationManager *asynSocket = [RequestOperationManager sharedManager];
    asynSocket.delegate = self;
    if (![asynSocket isConnected] || [asynSocket isDisconnected]) {
        [asynSocket connecttoHost:self];
    }
    
    [asynSocket writeData:params withTimeout:HTRequestTimeOut tag:tagRequest];
}

#pragma mark - Lost Internet Connection
- (void)userDidPressRetry {
    [self checkInternetAndRefreshData];
}

- (void)reachabilityChangedNotification:(NSNotification *)notification {
    LostConnectionView *lostConnectionView = (LostConnectionView *)[self.view viewWithTag:kLostConnectionViewTag];
    
    BOOL hasInternetConnection = [[HTAppDelegate sharedDelegate] hasInternetConnection];
    
    if (!hasInternetConnection && [self isEqual:self.navigationController.topViewController] && lostConnectionView == nil) { // show banner mất mạng
        [self showBanerErrorNetWork];
    }
}

- (void) showBanerErrorNetWork {
    [ALAlertBanner forceHideAllAlertBannersInView:self.view];
    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.view style:ALAlertBannerStyleFailure position:ALAlertBannerPositionTop title:NSLocalizedString(@"Không có kết nối mạng!", @"") subtitle:nil tappedBlock:^(ALAlertBanner *alertBanner) {
        [alertBanner hide];
    }];
    banner.secondsToShow = 5;
    banner.showAnimationDuration = 0.25;
    banner.hideAnimationDuration = 0.2;
    [banner show];
}

#pragma mark - GoogleAnalytics
- (void)sendScreenViewWithName:(NSString *)screenName {
    [[GAI sharedInstance].defaultTracker send:[[[GAIDictionaryBuilder createAppView]
                                                set:screenName
                                                forKey:kGAIScreenName] build]];
}

- (void)sendHitForCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:category action:action label:label value:value] build]];
}


#pragma mark - SlideNavigationControllerDelegate
#pragma mark - SlideNavigationControllerDelegate
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return !self.shouldDisplayBackButton;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

#pragma mark - connection delegate

- (void) socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *) err {
    // retry request
    NSLog(@"BaseViewController socketDidDisconnect with error: %@", err);
    NSString *message = @"";
    
    if (![[HTAppDelegate sharedDelegate] hasInternetConnection]) {
        message = NSLocalizedString(@"MESSAGE_ERROR_NETWORK", nil);
        
    } else if (err.code == SOCKET_CONNECT_NETWORK_ERROR ) {
        message = NSLocalizedString(@"MESSAGE_SOCKET_CONNECT_ERROR", nil);
        
    } else if (err.code == SOCKET_READ_TIMEOUT) {
        message = NSLocalizedString(@"MESSAGE_SOCKET_READ_TIMEOUT", nil);
        
    } else if (err.code == SOCKET_CONNECT_DUPLICATE) {
        message = NSLocalizedString(@"MESSAGE_SOCKET_CONNECT_DUPLICATE", nil);
        
    } else if (err.code == SOCKET_CLOSE_BY_REMOTE) {
        message = NSLocalizedString(@"MESSAGE_SOCKET_CLOSE_BY_REMOTE", nil);
        
    } else if (err.code == SOCKET_CONNECT_REFUSED) {
        message = NSLocalizedString(@"MESSAGE_SOCKET_REFUSED", nil);
        
    } else {
        message = NSLocalizedString(@"MESSAGE_SOCKET_UNKNOWN", nil);
    }
    
//    showLocalNotification([NSString stringWithFormat:@"%@ %ld", message, err.code], 0, [NSDate dateWithTimeIntervalSinceNow:1]);
    
    [self requestFail:err.code andMessage:message andResponseObject:nil andTagRequest:REQUEST_FOR_DISCONNECT];
    //MARK: TUANTD7 tai sao requestFail tag lai bang 0 = REQUEST_FOR_LOGIN
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag {
    NSLog(@"BaseViewController sock didWriteDataWithTag %ld ", tag);
    if (tag != REQUEST_FOR_SIGNATURE_REPORT && tag != REQUEST_FOR_LOGIN) {
        [sock readDataToData:[GCDAsyncSocket LFData] withTimeout:HTRequestTimeOut tag:tag];
    } else {
        [sock readDataToData:[GCDAsyncSocket LFData] withTimeout:HTRequestSignTimeOut tag:tag];
    }
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    NSLog(@"BaseViewController sock didReadData %ld ", tag);
    
    NSString* rawJson = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", rawJson);
    
    NSDictionary *responseObject = [rawJson JSONValue];
    NSInteger errorCode = [responseObject integerForKey:kCode];
    NSString *message   = [responseObject stringForKey:kMessage];
    id respontData      = [responseObject valueForKey:kData];
    
    if (errorCode == NoError) {
        NSLog(@"BaseViewController requestSuccess");
        [self requestSuccess:errorCode andMessage:message andResponseObject:respontData andTagRequest:tag];
    } else if (errorCode == SESSION_TIMEOUT) {
        NSLog(@"BaseViewController requestFail SESSION_TIMEOUT");
        parseUnSubscribe();
        presentLoginViewController(self);
        [HTPreferences removeAccountFromPreferences];
        [self requestFail:errorCode andMessage:message andResponseObject:nil andTagRequest:tag];
    } else {
        NSLog(@"BaseViewController requestFail");
        [self requestFail:errorCode andMessage:message andResponseObject:nil andTagRequest:tag];
    }
    //    [[RequestOperationManager sharedManager] readDataWithTimeout:HTRequestTimeOut tag:tag];// bỏ comment khi muốn nhận data liên tục cho request tương ứng với tag
}

- (void) requestSuccess:(NSInteger) errorCode
             andMessage:(NSString*) message
      andResponseObject:(id) responseObject
          andTagRequest:(NSInteger) tag {
    
}

- (void) requestFail:(NSInteger) errorCode
          andMessage:(NSString*) message
   andResponseObject:(id) responseObject
       andTagRequest:(NSInteger)tag {
    
}

#pragma mark - show File

- (void) showFileWithPath:(NSString*) strPath andShowSingButton:(BOOL) isShow andDocument:(id) documentModel {
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    //
    //        NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    //
    //        strPath = [pdfs firstObject]; assert(strPath != nil); // Path to first PDF file
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:strPath password:phrase];
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.isShowSignButton = isShow;
        if (isShow) {
            readerViewController.doccumentModel = documentModel;
        }
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        
#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
        
        [self.navigationController pushViewController:readerViewController animated:YES];
        
#else // present in a modal view controller
        
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:readerViewController animated:YES completion:NULL];
        
#endif // DEMO_VIEW_CONTROLLER_PUSH
    }
    else // Log an error so that we know that something went wrong
    {
        NSLog(@"%s [ReaderDocument withDocumentFilePath:'%@' password:'%@'] failed.", __FUNCTION__, strPath, phrase);
    }
}

#pragma mark - ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(ReaderViewController *)viewController {
#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
    
    [self.navigationController popViewControllerAnimated:YES];
    
#else // dismiss the modal view controller
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
#endif // DEMO_VIEW_CONTROLLER_PUSH
}

@end
