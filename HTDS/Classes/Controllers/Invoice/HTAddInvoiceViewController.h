//
//  HTAddContractViewController.h
//  HTDS
//
//  Created by TuanTD on 7/13/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseViewController.h"
#import "SZTextView.h"
#import "BaseTextField.h"
#import "HTInvoiceModel.h"
#import "PLRoundedButton.h"
#import "HTTextFieldLoggin.h"
#import "HTDocumentModel.h"

enum {
    FILE_TYPE_PDF = 10,
    FILE_TYPE_IMAGE,
};

@interface HTAddInvoiceViewController : BaseViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    NSString *fileNameImage;
    
}

@property (nonatomic,strong) HTInvoiceModel *invoiceModel;
@property (nonatomic,strong) HTDocumentModel *doccumentModel;
@property (nonatomic,assign) BOOL isUpdate;


@property (weak, nonatomic) IBOutlet UILabel *titleInfoLable;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet BaseTextField *invoiceDateTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *invoiceSymbolTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *invoiceSerriTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *providerTextfield;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *addressTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *taxCodeTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *taxesTexField;

@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *amountNotTaxTextField;

@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *amountTaxTextfield;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *amountTotalTextField;

@property (weak, nonatomic) IBOutlet SZTextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIButton *dateInvoideBtn;

@property (weak, nonatomic) IBOutlet PLRoundedButton *addButton;
@property (weak, nonatomic) IBOutlet PLRoundedButton *previewButton;
@property (weak, nonatomic) IBOutlet PLRoundedButton *selectFileBtn;

@property (weak, nonatomic) IBOutlet UIImageView *firtImageView;
@property (weak, nonatomic) IBOutlet UIImageView *secondImageView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdImageView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *firtIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *secondIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *thirdIndicator;




- (IBAction)dateInvoiceButtonPressed:(id)sender;
- (IBAction)addInvoiceButtonPressed:(id)sender;
- (IBAction)selectFilebuttonPressed:(id)sender;
- (IBAction)previewButtonPressed:(id)sender;





@end
