//
//  HTUpLoadInfoViewController.m
//  HTDS
//
//  Created by TuanTD on 7/10/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTListInvoiceViewController.h"
#import "HTInvoiceTableViewCell.h"
#import "HTAddInvoiceViewController.h"
#import "DictionaryBuilder.h"
#import "SVPullToRefresh.h"
#import "MBProgressHUD.h"
#import "HTInvoiceModel.h"
#import "PLConstants.h"
#import "PLFunctions.h"


@interface HTListInvoiceViewController ()

@property (nonatomic,strong) NSMutableArray *listInvoice;
@end

@implementation HTListInvoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self addCloseButtonItem];
}

- (void) setupUI {
    [super setupUI];
    
    __weak HTListInvoiceViewController *weakSelf = self;
    [weakSelf.tableView addInfiniteScrollingWithActionHandler:^{ // load more news
        [weakSelf loadMoreData];
        weakSelf.tableView.showsInfiniteScrolling = YES;
    }];
    
    // refres datata
    self.refreshBillData = [[UIRefreshControl alloc] init];
    [self.refreshBillData addTarget:self action:@selector(pullToRefressData) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:self.refreshBillData];
}

- (void) pullToRefressData {
    [super pullToRefressData];
    [_listInvoice removeAllObjects];
    [_tableView reloadData];
    [self loadMoreData];
}

- (void) registerNotification {
    [super registerNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:kPLAddInvoiceNotification object:nil];
}

- (void) unregisterNotification {
    [super unregisterNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) refreshData {
    [super refreshData];
    showMBProgressHUD(self.tableView, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    self.listInvoice = [NSMutableArray array];
    [self loadMoreData];
}

- (void) loadMoreData {
    [super loadMoreData];
    NSInteger offset = _listInvoice.count;
    NSData *params = [DictionaryBuilder dictionaryGetListInvoice:_doccumentModel andOffset:offset andLimit:LIMIT];
    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_LIST_INVOICE];
}

- (IBAction)addContractButtonPressed:(id)sender {
    [self sWitchContractViewController:nil];
}

- (void) sWitchContractViewController:(HTInvoiceModel*) invoiceModel {
    HTAddInvoiceViewController *addContractViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([HTAddInvoiceViewController class])];
    addContractViewController.shouldDisplayBackButton = YES;
    addContractViewController.doccumentModel = _doccumentModel;
    addContractViewController.invoiceModel = invoiceModel;
    [self.navigationController pushViewController:addContractViewController animated:YES];
}

#pragma mark- delegate,datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  _listInvoice.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ROW_HEIGHT_104;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"HTInvoiceTableViewCell";
    HTInvoiceTableViewCell *cell = (HTInvoiceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.controller = self;
    HTInvoiceModel *invoiceModel = _listInvoice[indexPath.row];
    [cell setCellData:invoiceModel];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HTInvoiceModel *invoiceModel = _listInvoice[indexPath.row];
    [self sWitchContractViewController:invoiceModel];
}

#pragma mark - restpont delegate

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    [_listInvoice addObjectsFromArray:[HTInvoiceModel parserListObject:responseObject]];
    [_tableView reloadData];
    [self hidenloadingAndRefressh];
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    [self hidenloadingAndRefressh];
    showAleartWithMessage(message);
}

- (void) hidenloadingAndRefressh {
    [MBProgressHUD hideHUDForView:self.tableView animated:YES];
    [_refreshBillData endRefreshing];
    [_tableView.infiniteScrollingView stopAnimating];
    _tableView.showsInfiniteScrolling = !(_listInvoice.count%LIMIT > 0);
}


@end
