//
//  HTPreviewViewController.m
//  HTDS
//
//  Created by TuanTD on 7/30/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTPreviewViewController.h"
#import "HTInvoiceModel.h"
#import "PLFunctions.h"

@interface HTPreviewViewController ()

@end

@implementation HTPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void) setupUI {
    textFieldPaddingWithFrame(self.invoiceDateTextField,CGRectMake(0, 0, 10, 10));
    [self.invoiceDateTextField setLeftTextFieldWithImageView:@"ico_calendar" andFrameRightView:CGRectMake(0, 0, 40,30)];
    self.descriptionTextView.layer.borderWidth = 1.0f;
    self.descriptionTextView.layer.borderColor = [UIColor colorWithRed:226.0f/255.0f green:228.0f/255.0f blue:229.0f/255.0f alpha:1].CGColor;
    self.descriptionTextView.placeholder = NSLocalizedString(@"PLACE_HODER_DESCRIPTION", nil);
    self.scrolView.contentSize = CGSizeMake(self.scrolView.frame.size.width*2, 1);
    self.infoScrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.descriptionTextView.frame) + 40);
    [self.segmentView setButtonTitles:@[[NSLocalizedString(@"TITLE_INFOR_INVOICE", nil) uppercaseString],[NSLocalizedString(@"TITLE_FILE_IMAGE", nil) uppercaseString]]];
    [self.imageView setImage:self.image];
    [self setUpDataTextField:self.invoiceModel];
    CGRect frame = self.imageView.frame;
    frame.origin.x = CGRectGetMaxX(self.infoScrollView.frame);
    self.imageView.frame = frame;
}

- (void) segmentedViewDidSelectItemAtIndex:(NSInteger)index {
    
    self.segmentView.tag = index;
    CGRect frame = self.scrolView.frame;
    frame.origin.x = frame.size.width * index;
    frame.origin.y = 0;
    [self.scrolView scrollRectToVisible:frame animated:YES];
}

// user scroll view
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.scrolView) {
        NSInteger selectedTab = scrollView.contentOffset.x / self.scrolView.frame.size.width;
        [self.segmentView setSelectButtonAtIndex:selectedTab userPressed:YES];
    }
}

- (void) setUpDataTextField:(HTInvoiceModel*) invoiceModel {
    self.invoiceDateTextField.text = invoiceModel.invoiceDate;
    self.invoiceSymbolTextField.text = invoiceModel.invoiceSign;
    self.invoiceSerriTextField.text = invoiceModel.invoiceSerial;
    self.providerTextfield.text     = invoiceModel.unitIssue;
    self.addressTextField.text = invoiceModel.address;
    self.taxCodeTextField.text = invoiceModel.taxCode;
    self.taxesTexField.text    = [invoiceModel.vat stringValue];
    self.amountNotTaxTextField.text =  convertIntegerToString(invoiceModel.amountNotTax);
    self.amountTaxTextfield.text = convertIntegerToString(invoiceModel.amountTax);
    self.amountTotalTextField.text = convertIntegerToString(invoiceModel.amountTotal);
    self.descriptionTextView.text  = invoiceModel.content;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    NSInteger index = self.segmentView.tag;
    [self setupUI];
    [self.segmentView setSelectButtonAtIndex:index userPressed:NO];
}

@end















