//
//  HTPreviewViewController.h
//  HTDS
//
//  Created by TuanTD on 7/30/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "BaseViewController.h"
#import "PLSegmentView.h"
#import "SZTextView.h"
#import "BaseTextField.h"
#import "HTTextFieldLoggin.h"
#import "HTInvoiceModel.h"

@interface HTPreviewViewController : BaseViewController

@property (nonatomic,strong) UIImage *image;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet PLSegmentView *segmentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrolView;
@property (weak, nonatomic) IBOutlet UIScrollView *infoScrollView;
@property (weak, nonatomic) IBOutlet UIView *amountTotalView;
@property (weak, nonatomic) IBOutlet UILabel *infoLable;
@property (nonatomic,strong) HTInvoiceModel *invoiceModel;


@property (weak, nonatomic) IBOutlet BaseTextField *invoiceDateTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *invoiceSymbolTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *invoiceSerriTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *providerTextfield;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *addressTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *taxCodeTextField;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *taxesTexField;

@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *amountNotTaxTextField;

@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *amountTaxTextfield;
@property (weak, nonatomic) IBOutlet HTTextFieldLoggin *amountTotalTextField;
@property (weak, nonatomic) IBOutlet SZTextView *descriptionTextView;

@end
