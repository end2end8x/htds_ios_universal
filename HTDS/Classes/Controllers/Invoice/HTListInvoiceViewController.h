//
//  HTUpLoadInfoViewController.h
//  HTDS
//
//  Created by TuanTD on 7/10/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseViewController.h"
#import "HTDocumentModel.h"

@interface HTListInvoiceViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) HTDocumentModel *doccumentModel;
@property (nonatomic,strong) UIRefreshControl *refreshBillData;


@end
