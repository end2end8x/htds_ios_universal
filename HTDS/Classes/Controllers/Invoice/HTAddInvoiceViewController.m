//
//  HTAddContractViewController.m
//  HTDS
//
//  Created by TuanTD on 7/13/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTAddInvoiceViewController.h"
#import "FileUtil.h"
#import "NSString+MD5.h"
#import "NSData+MD5.h"
#import <CommonCrypto/CommonDigest.h>
#import "MBProgressHUD.h"
#import "Base64.h"
#import "ImageDetailView.h"
#import "LGActionSheet.h"
#import "HTPreviewViewController.h"

#import "DictionaryBuilder.h"
#import "Defines.h"
#import "PLConstants.h"
#import "PLFunctions.h"
#import "HTAccount.h"
#import "HTPreferences.h"

@interface HTAddInvoiceViewController ()<UIDocumentInteractionControllerDelegate,ImageDetailDelegate>

@end

@implementation HTAddInvoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.descriptionTextView.placeholder = NSLocalizedString(@"PLACE_HODER_DESCRIPTION", nil);
    [_firtIndicator stopAnimating];
    [_secondIndicator stopAnimating];
    [_thirdIndicator stopAnimating];
    //    self.previewButton.enabled = NO;
    
    if (_invoiceModel) {
        [self setUpDataTextField:_invoiceModel];
        [self toogle:1];
    } else {
        HTAccount *account = [HTPreferences sharedPreferences].account;
        _providerTextfield.text = account.provider;
        _addressTextField.text  = account.address;
        _taxCodeTextField.text  = account.taxCode;
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addCloseButtonItem];
}

- (void) setupUI {
    textFieldPaddingWithFrame(self.invoiceDateTextField,CGRectMake(0, 0, 10, 10));
    [self.invoiceDateTextField setLeftTextFieldWithImageView:@"ico_calendar" andFrameRightView:CGRectMake(0, 0, 40,30)];
    self.descriptionTextView.layer.borderWidth = 1.0f;
    self.descriptionTextView.layer.borderColor = [UIColor colorWithRed:226.0f/255.0f green:228.0f/255.0f blue:229.0f/255.0f alpha:1].CGColor;
    UITapGestureRecognizer *singleScrollTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidenKeybroad)];
    singleScrollTap.numberOfTapsRequired = 1;
    [_scrollView setUserInteractionEnabled:YES];
    [_scrollView addGestureRecognizer:singleScrollTap];
    self.scrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.addButton.frame) + 40);
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return [[textView text] length] - range.length + text.length  <= TEXT_VIEW_LENGTH;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)text {
    return [[textField text] length] - range.length + text.length  <= TEXT_FIELD_LENGTH;
}

- (void) refreshData {
    [super refreshData];
    if (_invoiceModel) {
        NSString * strPath = [self getFilePathFromDownloadToUpdate];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:strPath];
        NSString *executableFileMD5Hash = @"";
        if (fileExists) {
            NSData *nsData = [NSData dataWithContentsOfFile:strPath];
            executableFileMD5Hash = [nsData MD5];
        }
        [_firtIndicator startAnimating];
        _firtImageView.hidden = YES;
        NSData *parmas = [DictionaryBuilder dictionaryForDowloadReportInvoice:_invoiceModel andMd5File:executableFileMD5Hash andReportModel:_doccumentModel];
        [self executeWithWithParams:parmas withTag:REQUEST_FOR_DOWNLOAD_FILE_INVOICE];
    }
}

- (NSString*) getFilePathFromDownloadToUpdate { // create foder and get file path
    NSString *fileName = [NSString stringWithFormat:@"%@-%@",convertIntegerToString(_invoiceModel.invoiceId),_invoiceModel.attachFileName];
    
    NSString *strPath = [[FileUtil createDirectoryWithName:kFolderReportInvoice] stringByAppendingPathComponent:fileName];
    return strPath;
}


- (void) setUpDataTextField:(HTInvoiceModel*) invoiceModel {
    NSDate* date = convertToDate(invoiceModel.invoiceDate, kPLFormatDate_MMDD_YY);
    NSString* dateStr = convertDateToString(date, kPLFormatDateDD_MM_YY);
    
    self.invoiceDateTextField.text = dateStr;//invoiceModel.invoiceDate;
    self.invoiceSymbolTextField.text = invoiceModel.invoiceSign;
    self.invoiceSerriTextField.text = invoiceModel.invoiceSerial;
    self.providerTextfield.text     = invoiceModel.unitIssue;
    self.addressTextField.text = invoiceModel.address;
    self.taxCodeTextField.text = invoiceModel.taxCode;
    self.taxesTexField.text    = [NSString stringWithFormat:@"%@", invoiceModel.vat];//convertIntegerToString(invoiceModel.vat);
    self.amountNotTaxTextField.text = convertIntegerToString(invoiceModel.amountNotTax);
    self.amountTaxTextfield.text = convertIntegerToString(invoiceModel.amountTax);
    self.amountTotalTextField.text = convertIntegerToString(invoiceModel.amountTotal);
    self.descriptionTextView.text  = invoiceModel.content;
}

- (void) toogle:(NSInteger ) tag {
    BOOL enabled = tag == 1 ? NO : YES;
    self.invoiceDateTextField.userInteractionEnabled = enabled;
    self.invoiceSymbolTextField.userInteractionEnabled = enabled;
    self.invoiceSerriTextField.userInteractionEnabled = enabled;
    self.providerTextfield.userInteractionEnabled = enabled;
    self.addressTextField.userInteractionEnabled = enabled;
    self.taxCodeTextField.userInteractionEnabled = enabled;
    self.taxesTexField.userInteractionEnabled = enabled;
    self.amountNotTaxTextField.userInteractionEnabled = enabled;
    self.amountTaxTextfield.userInteractionEnabled = enabled;
    self.amountTotalTextField.userInteractionEnabled = enabled;
    self.descriptionTextView.userInteractionEnabled = enabled;
    
    self.descriptionTextView.editable = enabled;
    
    self.selectFileBtn.hidden = !enabled;
    self.dateInvoideBtn.userInteractionEnabled = enabled;
    
//    UIColor *color = tag == 1 ? [UIColor whiteColor] : [UIColor greenColor];
//    
//    _selectFileBtn.layer.backgroundColor= (__bridge CGColorRef)(color);
    
    if (tag == 1) {
        self.title = NSLocalizedString(@"TITLE_DETAIL_INVOICE", nil);
        [_addButton setTitle:NSLocalizedString(@"BUTTON_TITLE_EDIT", nil) forState:UIControlStateNormal];
        [_addButton setTag:1];
    } else {
        self.title = NSLocalizedString(@"TITLE_UPDATE_INVOICE", nil);
        [_addButton setTitle:NSLocalizedString(@"BUTTON_TITLE_UPDATE", nil) forState:UIControlStateNormal];
        [_addButton setTag:2];
    }
}

#pragma mark - action

- (IBAction)dateInvoiceButtonPressed:(id)sender {
    UIDatePicker *datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 100.f);
    
    [[[LGActionSheet alloc] initWithTitle:nil view:datePicker buttonTitles:@[@"Chọn"] cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil actionHandler:^(LGActionSheet *actionSheet, NSString *title, NSUInteger index) {
        _invoiceDateTextField.text = convertDateToString(datePicker.date,kPLFormatDateDD_MM_YY);
    } cancelHandler:^(LGActionSheet *actionSheet, BOOL onButton) {
        
    } destructiveHandler:nil] showAnimated:YES completionHandler:nil];
}

- (IBAction)addInvoiceButtonPressed:(id)sender {
    NSInteger tag = [self.addButton tag];
    if (tag == 1) {
        [self toogle:2];
        return;
    }
    
    [self.view endEditing:YES];
    
    NSData *imageData = nil;
    if (_firtImageView.tag == FILE_TYPE_IMAGE) {
        imageData = UIImagePNGRepresentation(_firtImageView.image);
    } else if (_firtImageView.tag == FILE_TYPE_PDF) {
        NSString * strPath = [self getFilePathFromDownloadToUpdate];
        imageData = [NSData dataWithContentsOfFile:strPath];
    }
    
    NSString *message = nil;
    if (isEmptyString(_invoiceDateTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_DDATE_INVOICE", nil);
    } else if (isEmptyString(_invoiceSymbolTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_SYMBOL_INVOICE", nil);
    } else if (isEmptyString(_invoiceSerriTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_SERIA_INVOICE", nil);
    } else if (isEmptyString(_providerTextfield.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Provider_Invoice", nil);
    } else if (isEmptyString(_addressTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Address_Invoice", nil);
    } else if (isEmptyString(_taxCodeTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Code_tax_Invoice", nil);
    } else if (isEmptyString(_taxesTexField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Code_taxes_Invoice", nil);
    } else if (isEmptyString(_amountNotTaxTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Amount_NOT_Tax_Invoice", nil);
    } else if (isEmptyString(_amountTaxTextfield.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Amount_Tax_Invoice", nil);
    } else if (isEmptyString(_amountTotalTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Amount_Total_Invoice", nil);
    } else if (isEmptyString(_descriptionTextView.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_INVOICE_CONTENT", nil);
    } else if (!imageData) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_IMAGE", nil);
    } else {
        NSInteger amountNotTax = [_amountNotTaxTextField.text integerValue];
        NSInteger amountTax = [_amountTaxTextfield.text integerValue];
        NSInteger amountTotal = [_amountTotalTextField.text integerValue];
        
        if (amountNotTax + amountTax != amountTotal) {
            message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_Amount_Total_Invoice_INVALID", nil);
        }
    }
    
    if (message) {
        showAleartWithMessage(message);
        return;
    }
    
    HTInvoiceModel *invoiceInfo = [[HTInvoiceModel alloc] init];
    invoiceInfo.invoiceId   = (_invoiceModel?_invoiceModel.invoiceId:-1);
    invoiceInfo.invoiceDate = trimSpaceWhiteString(_invoiceDateTextField.text);
    invoiceInfo.invoiceSign = trimSpaceWhiteString(_invoiceSymbolTextField.text);
    invoiceInfo.invoiceSerial = trimSpaceWhiteString(_invoiceSerriTextField.text);
    invoiceInfo.unitIssue     = trimSpaceWhiteString(_providerTextfield.text);
    invoiceInfo.address     = trimSpaceWhiteString(_addressTextField.text);
    invoiceInfo.taxCode     = trimSpaceWhiteString(_taxCodeTextField.text);
    invoiceInfo.vat         = [NSNumber numberWithDouble:[trimSpaceWhiteString(_taxesTexField.text) doubleValue]];
    invoiceInfo.amountNotTax = [trimSpaceWhiteString(_amountNotTaxTextField.text) integerValue];
    invoiceInfo.amountTax   = [trimSpaceWhiteString(_amountTaxTextfield.text) integerValue];
    invoiceInfo.amountTotal = [trimSpaceWhiteString(_amountTotalTextField.text) integerValue];
    invoiceInfo.attachFileName = (_firtImageView.tag == FILE_TYPE_IMAGE ? fileNameImage:_invoiceModel.attachFileName);
    invoiceInfo.content     = trimSpaceWhiteString(_descriptionTextView.text);
    
    
    NSString *executableFileMD5Hash = (imageData == nil?@"":[imageData MD5]);
    NSString *base64String = (imageData == nil?@"":[self imageToBase64String:imageData]);
    
    NSString *title = _invoiceModel ? @"LOADDING_LOGGING_UPDATE" : @"LOADDING_LOGGING_ADD";
    showMBProgressHUD(self.view, title, YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    
    NSData *params = [DictionaryBuilder dictionaryForAddOrUpdateReportInvoice:invoiceInfo andDocumentModel:_doccumentModel andFileContent:base64String andMd5File:executableFileMD5Hash];
    [self executeWithWithParams:params withTag:REQUEST_FOR_ADD_INVOICE];
    
}

- (NSString*)imageToBase64String:(NSData*) imageData {
    
    
    NSString *imageDataEncodedString = [imageData base64EncodedString];
    return imageDataEncodedString;
}

- (void) imageViewFirtTap {
    if (_firtImageView.tag == FILE_TYPE_IMAGE) {
        ImageDetailView *imageDetailView = [ImageDetailView getViewPopUP];
        [imageDetailView.imageView setImage:_firtImageView.image];
        imageDetailView.delegate = self;
        [imageDetailView setFrame:[UIScreen mainScreen].bounds];
        self.navigationController.navigationBarHidden = YES;
        showPopUp(imageDetailView, nil, self);
    } else {
        NSString * strPath = [self getFilePathFromDownloadToUpdate];
        if ([[strPath lowercaseString] containsString:@".pdf"] ) {
            [self showFileWithPath:strPath andShowSingButton:NO andDocument:_doccumentModel];
        } else {
            ImageDetailView *imageDetailView = [ImageDetailView getViewPopUP];
            [imageDetailView.imageView setImage:_firtImageView.image];
            imageDetailView.delegate = self;
            [imageDetailView setFrame:[UIScreen mainScreen].bounds];
            self.navigationController.navigationBarHidden = YES;
            showPopUp(imageDetailView, nil, self);

        }
    }
}

- (IBAction)selectFilebuttonPressed:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"TITLE_SELECT_FILE", nil)  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"LABLE_BUTTON_CAMERA", nil),NSLocalizedString(@"LABLE_BUTTON_SELECT_FROM_LIBRARY_PHOTOS", nil), nil];
    [actionSheet showInView:self.view];
    
}

- (IBAction)previewButtonPressed:(id)sender {
    
    if (self.previewButton.tag == FILE_TYPE_IMAGE) {
        HTInvoiceModel *invoiceInfo = [[HTInvoiceModel alloc] init];
        invoiceInfo.invoiceId   = (_invoiceModel?_invoiceModel.invoiceId:-1);
        invoiceInfo.invoiceDate = trimSpaceWhiteString(_invoiceDateTextField.text);
        invoiceInfo.invoiceSign = trimSpaceWhiteString(_invoiceSymbolTextField.text);
        invoiceInfo.invoiceSerial = trimSpaceWhiteString(_invoiceSerriTextField.text);
        invoiceInfo.unitIssue     = trimSpaceWhiteString(_providerTextfield.text);
        invoiceInfo.address     = trimSpaceWhiteString(_addressTextField.text);
        invoiceInfo.taxCode     = trimSpaceWhiteString(_taxCodeTextField.text);
        invoiceInfo.vat         = [NSNumber numberWithDouble:[trimSpaceWhiteString(_taxesTexField.text) doubleValue]];
        invoiceInfo.amountNotTax = [trimSpaceWhiteString(_amountNotTaxTextField.text) integerValue];
        invoiceInfo.amountTax   = [trimSpaceWhiteString(_amountTaxTextfield.text) integerValue];
        invoiceInfo.amountTotal = [trimSpaceWhiteString(_amountTotalTextField.text) integerValue];
        invoiceInfo.content     = trimSpaceWhiteString(_descriptionTextView.text);
        HTPreviewViewController *previewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([HTPreviewViewController class])];
        previewController.shouldDisplayBackButton = YES;
        previewController.image = self.firtImageView.image;
        previewController.invoiceModel = invoiceInfo;
        [self.navigationController pushViewController:previewController animated:YES];
    } else {
        if (_invoiceModel.attachFileName) {
            NSString * strPath = [self getFilePathFromDownloadToUpdate];
            [self showFileWithPath:strPath andShowSingButton:NO andDocument:_doccumentModel];
        } else {
            NSString *message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_IMAGE", nil);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            showAleartWithMessage(message);
        }
    }
}

#pragma mark - delegate

- (void) dissmissImageDetail:(id)sender {
    self.navigationController.navigationBarHidden = NO;
    hidePopUpView(sender, nil, self);
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.allowsEditing = YES;
        imagePicker.delegate = self;
        imagePicker.sourceType = 1 - buttonIndex;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *avatarImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [_firtImageView setImage:avatarImage];
    [self setActionImageWithTag:FILE_TYPE_IMAGE];
    NSURL *imagePath = [info objectForKey:UIImagePickerControllerReferenceURL];
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    fileNameImage = [NSString stringWithFormat:@"iOS_Invoice_%ld_%ld_%@_%lf.%@",[_doccumentModel icReportId], [account partnerId], [account userName],[[NSDate date] timeIntervalSince1970], [imagePath pathExtension]];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self setNextForcusTextfield];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textFieldDidBeginEditing(textField, _scrollView);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    textFieldDidEndEditing(textField, _scrollView);
}

#pragma mark - textView delegate
- (void) textViewDidBeginEditing:(UITextView *)textView {
    scrollDownTexView(textView, _scrollView);
}

- (void) textViewDidEndEditing:(UITextView *)textView {
    scrollUpTexView(textView, _scrollView);
}

#pragma mark - touch view delegate
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void) hidenKeybroad {
    [self.view endEditing:YES];
}

#pragma mark - action

- (void) setNextForcusTextfield {
    
    CGRect  frame;
    UIView *textfield = nil;
    if ([self.invoiceDateTextField resignFirstResponder]) {
        [self.invoiceSymbolTextField becomeFirstResponder];
        textfield = _invoiceSymbolTextField;
    } else if ([self.invoiceSymbolTextField resignFirstResponder]) {
        [self.invoiceSerriTextField becomeFirstResponder];
        textfield = _invoiceSerriTextField;
    } else if ([self.invoiceSerriTextField resignFirstResponder]) {
        [self.providerTextfield becomeFirstResponder];
        textfield = self.providerTextfield;
    } else if ([self.providerTextfield resignFirstResponder]) {
        [self.addressTextField becomeFirstResponder];
        textfield = self.addressTextField;
    }else if ([self.addressTextField resignFirstResponder]) {
        [self.taxCodeTextField becomeFirstResponder];
        textfield = self.taxCodeTextField;
    } else if ([self.taxCodeTextField resignFirstResponder]) {
        [self.taxesTexField becomeFirstResponder];
        textfield = self.taxesTexField;
    } else if ([self.taxesTexField resignFirstResponder]) {
        [self.amountTaxTextfield becomeFirstResponder];
        textfield = self.amountTaxTextfield;
    } else if ([self.amountNotTaxTextField resignFirstResponder]) {
        [self.amountNotTaxTextField becomeFirstResponder];
        textfield = self.amountTotalTextField;
    } else if ([self.amountTaxTextfield resignFirstResponder]) {
        [self.amountTotalTextField becomeFirstResponder];
        textfield = self.amountTotalTextField;
    } else if ([self.amountTotalTextField resignFirstResponder]) {
        [self.descriptionTextView becomeFirstResponder];
        textfield = self.descriptionTextView;
    }
    if (textfield)
    {
        frame = textfield.frame;
        frame.origin.y += 44;
        [self.scrollView scrollRectToVisible:frame animated:YES];
    }
}

#pragma mark - restpont server

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    if (tag == REQUEST_FOR_DOWNLOAD_FILE_INVOICE) {
        if (responseObject != nil && ![responseObject isEqual:[NSNull class]] && ((NSString*) responseObject).length > 0) {
            NSString * strPath = [self getFilePathFromDownloadToUpdate];
            NSData *decodeData = [[NSData alloc] initWithBase64EncodedString:responseObject options:NSDataBase64DecodingIgnoreUnknownCharacters];
            [FileUtil deleteFileAtPath:strPath];
            [decodeData writeToFile:strPath atomically:YES];
            [self setActionImageWithTag:FILE_TYPE_PDF];
        } else {
            _firtImageView.hidden = NO;
            [_firtIndicator stopAnimating];
        }
    } if (tag == REQUEST_FOR_ADD_INVOICE) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kPLAddInvoiceNotification object:nil];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        showAleartWithMessage(message);
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    if (tag == REQUEST_FOR_DOWNLOAD_FILE_INVOICE) {
        if (errorCode == FILE_DUPLICATE) {
            [self setActionImageWithTag:FILE_TYPE_PDF];
        } else {
            _firtImageView.hidden = NO;
            [_firtIndicator stopAnimating];
        }
    } else {
        showAleartWithMessage(message);
        
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) setActionImageWithTag:(NSInteger) tag {
    self.previewButton.tag = tag;
    self.previewButton.enabled = YES;
    [_firtIndicator stopAnimating];
    _firtImageView.hidden = NO;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewFirtTap)];
    singleTap.numberOfTapsRequired = 1;
    _firtImageView.tag = tag;
    [_firtImageView setUserInteractionEnabled:YES];
    [_firtImageView addGestureRecognizer:singleTap];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self setupUI];
}


@end












