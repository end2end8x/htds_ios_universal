//
//  HTLoginViewController.h
//  HTDS
//
//  Created by Anhpt67 on 7/7/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "InputViewController.h"
#import "HTTextFieldLoggin.h"
#import "PLRoundedButton.h"

@interface HTLoginViewController : InputViewController


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet PLRoundedButton *forgotPassWordButton;


@end
