//
//  HTLoginViewController.m
//  HTDS
//
//  Created by Anhpt67 on 7/7/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTLoginViewController.h"
#import "MBProgressHUD.h"
#import "HTAccount.h"

#import "DictionaryBuilder.h"
#import "Defines.h"
#import "PLConstants.h"
#import "PLFunctions.h"
#import "HTPreferences.h"
#import "HTHomeViewController.h"
#import "HTManagerViewController.h"
#import "HTAppDelegate.h"
#import <Parse/Parse.h>



@interface HTLoginViewController ()

@end

@implementation HTLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void) setupUI {
    [super setupUI];
    self.scrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.forgotPassWordButton.frame) + 40);
    [self.emailTextField setRightViewTextFieldWithImageView:@"img_ico_email" andFrameRightView:CGRectMake(0, 0, 49, 49)];
    [self.passwordTextField setRightViewTextFieldWithImageView:@"img_ico_password" andFrameRightView:CGRectMake(0, 0, 49, 49)];
    
#if TARGET_IPHONE_SIMULATOR
    self.emailTextField.text = @"test_trang1";
    //    self.passwordTextField.text = @"123456a@";
    //        self.emailTextField.text = @"dt_nhanvien";
    //    self.emailTextField.text = @"dt_lanhdao";
    self.passwordTextField.text = @"123456a@";
//            self.emailTextField.text = @"truonggiang";
    //    self.passwordTextField.text = @"123456a@";
    NSLog(@"Running in Simulator - no app store or giro");
#endif
}

- (void) addSearchButtonItem {
    
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self loginButtonPressed:nil];
    return YES;
}

#pragma mark - action

- (IBAction)loginButtonPressed:(id)sender {
    
    [self.view endEditing:YES];
    NSString *message = nil;
    if (isEmptyString(self.emailTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_USER_NAME", nil);
    } else if (isEmptyString(self.passwordTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_PASSWORD", nil);
    }
    
    if (message) {
        showAleartWithMessage(message);
    } else {
        NSString *username = [trimSpaceWhiteString(self.emailTextField.text) lowercaseString];
        
        showMBProgressHUD(self.view, @"LOADDING_LOGGING", YES, MBProgressHUDModeDeterminate, HTRequestSignTimeOut);
        [HTAppDelegate sharedDelegate].timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                         target:self
                                       selector:@selector(onTick)
                                       userInfo:nil
                                        repeats:YES];
        
        NSData *params = [DictionaryBuilder dictionaryForLogin:username andPassWord:self.passwordTextField.text];
        [self executeWithWithParams:params withTag:REQUEST_FOR_LOGIN];
    }
}

- (IBAction)forgotPassWordButtonPressed:(id)sender {
    if (isEmptyString(self.emailTextField.text)) {
        showAleartWithMessage(NSLocalizedString(@"MESSAGE_INSERT_EMAIL_GET_PASSWORD", nil));
        return;
    }
}

#pragma mark - request respont delegate

- (void) home:(HTAccount *) account {
    [HTPreferences saveAccountToPreferences:account];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPLUserDidLoginNotification object:nil];
    
    // When users indicate they are Giants fans, we subscribe them to that channel.
    parseSubscribe([NSArray arrayWithObjects:[account userName], [NSString stringWithFormat:@"partnerId_%ld",[account partnerId]], nil]);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    if (tag == REQUEST_FOR_LOGIN) {
        HTAccount *account = [[HTAccount alloc] initWithAttributes:[((NSArray*)responseObject) firstObject]];
        
        [self home:account];
    }
    [super stopTimer];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    NSLog(@"HTLoginViewController requestFail errorCode: %ld message: %@ tag: %ld responseObject: %@ ", errorCode, message, tag , responseObject);
    [super stopTimer];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    showAleartWithMessage(message);
}

#pragma mark - rotate

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.scrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.forgotPassWordButton.frame) + 40);
}

@end
