//
//  PLMenuViewController.h
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/9/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTMenuViewController : UIViewController<UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (nonatomic,assign) NSInteger rowMenuItemSelected;
@property (nonatomic,readwrite) BOOL shouldRetrymenuItem;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLable;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLable;

+ (HTMenuViewController *) sharedInstance;

- (void) loadMenuItems:(NSNotification*) notify;
- (void) loadMenuItems;


@end
