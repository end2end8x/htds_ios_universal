//
//  PLMenuViewController.m
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/9/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "HTMenuViewController.h"
#import "HTMenuItem.h"
#import "MBProgressHUD.h"
#import "PLMenuItemTableViewCell.h"


#import "BaseViewController.h"
#import "HTHomeViewController.h"
#import "HTViettelSignViewController.h"
#import "HTReportViewController.h"
#import "HTReportCDAViewController.h"
#import "HTEmployViewController.h"
#import "HTManagerViewController.h"

#import "PLConstants.h"
#import "HTPreferences.h"
#import "PLFunctions.h"
#import <Parse/Parse.h>


@interface HTMenuViewController ()
@property (nonatomic, strong) NSMutableArray *menuItems;
@property (nonatomic, strong) NSMutableArray *categories;

@end

@implementation HTMenuViewController

static HTMenuViewController *singletonInstance;

#pragma mark - Initialization -

+ (HTMenuViewController *) sharedInstance
{
    if (!singletonInstance)
        NSLog(@"SlideNavigationController has not been initialized. Either place one in your storyboard or initialize one in code");
    
    return singletonInstance;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMenuItems) name:kPLUserDidLoginNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.rowMenuItemSelected = 0;
    [self loadMenuItems];
}

- (void) setHighlightMenuItem {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.rowMenuItemSelected inSection:0];
    [self.menuTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void) loadMenuItems:(NSNotification*) notify {
    NSLog(@"loadMenuItems");
}

- (void) loadMenuItems { 
    self.menuItems = [NSMutableArray array];
    [HTMenuItem getCategories:^(NSInteger errorCode, NSString *message, id data, id categories) {
        HTAccount *account = [HTPreferences sharedPreferences].account;
        if (account) {
            self.fullNameLable.text = [account.fullName uppercaseString];
            self.phoneNumberLable.text = [NSString stringWithFormat:@"SĐT: %@",account.tellNumber];
        }
        [self.menuItems addObjectsFromArray:(NSArray*)data];
        [self.menuTableView reloadData];
        [self setHighlightMenuItem];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }]; 
}

#pragma mark - UITableView delegate - datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.menuItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ROW_HEIGHT_MENU_ITEM;
} 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *LeftMenuCell = @"PLMenuItemTableViewCell";
    
    PLMenuItemTableViewCell *cell = (PLMenuItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:LeftMenuCell];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:LeftMenuCell owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    HTMenuItem *menuItem = self.menuItems[indexPath.row];
    [cell setCellData:menuItem];
    cell.titleLabel.highlightedTextColor = [UIColor whiteColor];
    return cell;
} 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HTMenuItem *menuItem = self.menuItems[indexPath.row];
    
    if ([menuItem.holderControllerClassName isEqualToString:@""]) {
        if (!menuItem.clickable) {
            return;
        } else { // Đăng xuất
            UIAlertView *allertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"MESSAGE_ALLEART_LOGOUT", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_ALLEART_LOGOUT_LEFT", nil) otherButtonTitles:NSLocalizedString(@"BUTTON_ALLEART_LOGOUT_RIGHT", nil), nil];
            [allertView show];
        }
        return;
    }
    self.rowMenuItemSelected = indexPath.row;
    @try {
        NSLog(@"name: %@",menuItem.holderControllerClassName);
        
        BaseViewController *viewController = [self getViewControllerWithMenuItem:menuItem];
        viewController.title                 = menuItem.titleDisplay.capitalizedString;
        if ([viewController isKindOfClass:[HTHomeViewController class]]) {
            [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:NO];
        } else {
            [[SlideNavigationController sharedInstance] switchToViewController:viewController withCompletion:nil];
        } 
    }
    @catch (NSException *exception) {
        NSLog(@"error: %@",exception);
    } 
}

- (BaseViewController*) getViewControllerWithMenuItem:(HTMenuItem*) menuItem {
    BaseViewController *viewController = nil;
    if ([menuItem.holderControllerClassName isEqualToString:NSStringFromClass([HTViettelSignViewController class])]) {
        viewController = [[HTViettelSignViewController alloc] initWithNibName:NSStringFromClass([HTBaseTabViewController class]) bundle:nil];
    } else if ([menuItem.holderControllerClassName isEqualToString:NSStringFromClass([HTReportViewController class])]) {
        viewController = [[HTReportViewController alloc] initWithNibName:NSStringFromClass([HTBaseReportViewController class]) bundle:nil];
    } else if ([menuItem.holderControllerClassName isEqualToString:NSStringFromClass([HTReportCDAViewController class])]) {
        viewController = [[HTReportCDAViewController alloc] initWithNibName:NSStringFromClass([HTBaseReportViewController class]) bundle:nil];
    } else if ([menuItem.holderControllerClassName isEqualToString:NSStringFromClass([HTEmployViewController class])]) {
        viewController = [[HTEmployViewController alloc] initWithNibName:NSStringFromClass([HTBaseTabViewController class]) bundle:nil];
    } else if ([menuItem.holderControllerClassName isEqualToString:NSStringFromClass([HTManagerViewController class])]) {
        viewController = [[HTManagerViewController alloc] initWithNibName:NSStringFromClass([HTBaseTabViewController class]) bundle:nil];
    } else {
        viewController = [self.storyboard instantiateViewControllerWithIdentifier:menuItem.holderControllerClassName];
    }
    return viewController;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [HTPreferences removeAccountFromPreferences];
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:NO];
        parseUnSubscribe();
        presentLoginViewController([SlideNavigationController sharedInstance].topViewController);
        
        self.rowMenuItemSelected = 0;
        [self loadMenuItems];
    }
}

@end











