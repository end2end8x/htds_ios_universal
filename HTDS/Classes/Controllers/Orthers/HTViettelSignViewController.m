//
//  HTRecentViewController.m
//  HTDS
//
//  Created by Anhpt67 on 7/9/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTViettelSignViewController.h"
#import "HTDocumentModel.h"
#import "SVPullToRefresh.h"
#import "MBProgressHUD.h"
#import "DictionaryBuilder.h"
#import "PLConstants.h"
#import "HTSearchViewController.h"

#import "DictionaryBuilder.h"
#import "PLConstants.h"
#import "Defines.h"
#import "HTAccount.h"
#import "HTPreferences.h"
#import "PLFunctions.h"

@interface HTViettelSignViewController ()

@end

@implementation HTViettelSignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//- (void) loadMoreData {
//    [super loadMoreData];
//    
//   NSString *status = [NSString stringWithFormat:@"%@,%@",[NSNumber numberWithInteger:STATUS_VIETTELL],[NSNumber numberWithInteger:STATUS_ACCEPTED]]; 
//    NSData *params = [DictionaryBuilder dictionarySearchReportWithStatus:self.tableView.listData.count andLimit:LIMIT andStatus:status];
//    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_LIST_REPORT_VIETEL_SIGN];
//}
//
//- (void) searchButtonPressed {
//    [super searchButtonPressed];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    HTSearchViewController *searchViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([HTSearchViewController class])];
//    searchViewController.shouldDisplayBackButton = YES;
//    searchViewController.delegate = self;
//    [self.navigationController pushViewController:searchViewController animated:YES];
//}

- (void) getListReports {
//    HTAccount *account = [HTPreferences sharedPreferences].account;
//    NSString *status = @"";
//    if (account.partnerUserType == PARTNER_TYPE_ANHGIANG) {
//        status = convertIntegerToString(STATUS_MANAGER_SIGN);
//    } else {
//        status = convertIntegerToString(STATUS_WAITING_SIGNATURE);
//    }
//    NSData *params = [DictionaryBuilder dictionarySearchAllReport:self.reportTableView.listData.count andLimit:LIMIT andCycleDate:@"" andStatus:status];
//    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_LIST_REPORT];
    
    NSString *status = [NSString stringWithFormat:@"%@,%@",[NSNumber numberWithInteger:STATUS_VIETTEL],[NSNumber numberWithInteger:STATUS_ACCEPTED]];
    NSData *params = [DictionaryBuilder dictionarySearchReportViettelSign:self.reportTableView.listData.count andLimit:LIMIT andStatus:status hasInvoice:0];
    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_LIST_REPORT];
}

- (void) getListReportHistory { 
//    HTAccount *account = [HTPreferences sharedPreferences].account;
//    NSString *status = @"";
//    if (account.partnerUserType == PARTNER_TYPE_ANHGIANG) {
//        status = convertIntegerToString(STATUS_VIETTELL);
//    } else {
//        status = convertIntegerToString(STATUS_MANAGER_SIGN);
//    }
//    
//    NSData *params = [DictionaryBuilder dictionarySearchUserHistory:self.reportHistoryTableView.listData.count andLimit:LIMIT  andCycleDate:@"" andStatus:status];
//    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_REPORT_HISTORY];
    
    NSString *status = [NSString stringWithFormat:@"%@,%@",[NSNumber numberWithInteger:STATUS_VIETTEL],[NSNumber numberWithInteger:STATUS_ACCEPTED]];
    NSData *params = [DictionaryBuilder dictionarySearchReportViettelSign:self.reportHistoryTableView.listData.count andLimit:LIMIT andStatus:status hasInvoice:1];
    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_REPORT_HISTORY];
}


@end
