//
//  HTSearchViewController.h
//  HTDS
//
//  Created by Anhpt67 on 7/8/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseListDocumentViewController.h"
#import "BaseTextField.h"
#import "HTDoccumentTableView.h"
#import "HTCategoriesViewController.h"


@interface HTSearchViewController : HTBaseListDocumentViewController<HTCategoriesDelegate> {
    NSInteger indexSelectionContract;
    NSInteger indexSelectionService;
    NSInteger indexSelectionStatus;
    NSInteger indexSelectionPartners;
    NSInteger indexSelectionStaff;
}

@property (weak, nonatomic) IBOutlet BaseTextField *contractTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *serviceTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *timeControlTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *statusTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *employTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *partnerTextField;
@property (nonatomic,assign) id delegate;


@property (weak, nonatomic) IBOutlet UIButton *contractButton;
@property (weak, nonatomic) IBOutlet UIButton *serviceButton;
@property (weak, nonatomic) IBOutlet UIButton *dateChargButton;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;




- (IBAction)employButtonPressed:(id)sender;
- (IBAction)partnerButtonPressed:(id)sender;
- (IBAction)contractButtonPressed:(id)sender; 
- (IBAction)serviceButtonPressed:(id)sender;
- (IBAction)dateChargeButtonPressed:(id)sender;
- (IBAction)statusButtonPressed:(id)sender;



@end
