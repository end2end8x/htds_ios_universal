//
//  HTSearchViewController.m
//  HTDS
//
//  Created by Anhpt67 on 7/8/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTSearchViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "MBProgressHUD.h"
#import "HTAppDelegate.h"
#import "DictionaryBuilder.h"
#import "PLFunctions.h"
#import "Defines.h"
#import "PLConstants.h"
#import "LGActionSheet.h"
#import "HTAccount.h"
#import "HTPreferences.h"

#import "HTContractModel.h"
#import "HTServiceModel.h"
#import "HTStatusModel.h"
#import "HTPartnerModel.h"
#import "HTStaffModel.h"
#import "HTHomeViewController.h"
#import "HTEmployViewController.h"
#import "HTManagerViewController.h"
#import "HTBaseTabViewController.h"
#import "HTViettelSignViewController.h"

@interface HTSearchViewController ()

@end

@implementation HTSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) setupUI {
    [super setupUI];
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        CGRect frame = self.tableView.frame;
        frame.origin.y += 39;
        frame.size.height -= 39;
        self.tableView.frame = frame;
    }
    [self.contractTextField setRightViewTextFieldWithImageView:@"img_row" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.serviceTextField setRightViewTextFieldWithImageView:@"img_row" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.timeControlTextField setRightViewTextFieldWithImageView:@"ico_calendar" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.statusTextField setRightViewTextFieldWithImageView:@"img_row" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.employTextField setRightViewTextFieldWithImageView:@"img_row" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.partnerTextField setRightViewTextFieldWithImageView:@"img_row" andFrameRightView:CGRectMake(0, 0, 40,30)];
}

- (void) registerNotification {
    [super registerNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:kPLSignatureReportNotification object:nil];
}

- (void) unregisterNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) refreshData { // không super hàm này tránh request ban đầu vào
    //    [super refreshData];
    [self unregisterNotification];
    [self registerNotification];
}

- (void) loadMoreData {
    [super loadMoreData];
    HTBaseTabViewController *basetabViewControler = (HTBaseTabViewController*) self.delegate;
    NSInteger tag = basetabViewControler.segmentView.tag;
    NSLog(@"loadMoreData %@ tag %ld", NSStringFromClass(basetabViewControler.class), tag);
    
    HTContractModel *contract = [HTAppDelegate sharedDelegate].listContract[indexSelectionContract];
    
    HTServiceModel *service = [HTAppDelegate sharedDelegate].listService[indexSelectionService];
    
    HTStatusModel *status = [HTAppDelegate sharedDelegate].listStatus[indexSelectionStatus];
    
    HTStaffModel *staff = [HTAppDelegate sharedDelegate].listStaff[indexSelectionStaff];
    
    HTPartnerModel *partner = [HTAppDelegate sharedDelegate].listPartners[indexSelectionPartners];
    
    NSString *icCycleDate = trimSpaceWhiteString(_timeControlTextField.text);
    
    NSArray *listCategories = [NSArray arrayWithObjects:contract,service,status,partner,staff, nil];
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    NSString *contractId = @"";
    NSString *serviceId  = @"";
    NSString *statusId   = @"";
    
    NSString *partnerId  = @"";
    NSString *staffId   = @"";
    
    if([[contract objectId] integerValue] != -1) {
        contractId = [[contract objectId] stringValue];
    }
    if([[service objectId] integerValue] != -1) {
        serviceId = [[service objectId] stringValue];
    }
    
    if([[partner objectId] integerValue] != -1) {
        partnerId = [[partner objectId] stringValue];
    }
    if([[staff objectId] integerValue] != -1) {
        staffId = [[staff objectId] stringValue];
    }
    
    if (isEmptyString(icCycleDate)) {
        NSString *message = NSLocalizedString(@"MESSAGE_PLEASE_IC_CYCLE_DATE", nil);
        
        [super hidenloadingAndRefressh];
        
        showAleartWithMessage(message);
        return;
    }
    
    if([basetabViewControler isKindOfClass:[HTViettelSignViewController class]]) {
        if([[status objectId] integerValue] != -1) {
            statusId = [[status objectId] stringValue];
        } else {
            statusId = @"8,9";
        }
        if (tag == TAB_REPORT) {
            NSData *params = [DictionaryBuilder dictionarySearchReportViettelSignStatus:statusId andContract:contractId andService:serviceId andIcCycleDate:icCycleDate andPartner:[NSString stringWithFormat:@"%ld", [account partnerId]] andUsername:[account userName] hasInvoice:0 andOffset:self.tableView.listData.count andLimit:LIMIT];
            [self executeWithWithParams:params withTag:REQUEST_FOR_SERCH_REPORT];
        } else if(tag == TAB_REPORT_HISTORY){
            NSData *params = [DictionaryBuilder dictionarySearchReportViettelSignStatus:statusId andContract:contractId andService:serviceId andIcCycleDate:icCycleDate andPartner:[NSString stringWithFormat:@"%ld", [account partnerId]] andUsername:[account userName] hasInvoice:1 andOffset:self.tableView.listData.count andLimit:LIMIT];
            [self executeWithWithParams:params withTag:REQUEST_FOR_SERCH_REPORT];
        }
    } else {
        if (tag == TAB_REPORT) {
            if([[status objectId] integerValue] != -1) {
                statusId = [[status objectId] stringValue];
            } else {
                if ([basetabViewControler isKindOfClass:[HTHomeViewController class]]) {
                    if(account.partnerUserType == PARTNER_TYPE_ADMIN || account.partnerUserType == PARTNER_TYPE_MANAGER ) {
                        statusId = @"5,7";
                    } else {
                        statusId = @"2,5";
                    }
                } else if ([basetabViewControler isKindOfClass:[HTEmployViewController class]]) {
                    statusId = @"2,5";
                } else if ([basetabViewControler isKindOfClass:[HTManagerViewController class]]) {
                    statusId = @"5,7";
                }
            }
            
            NSData *params = nil;
            if ([account partnerUserType] == PARTNER_TYPE_VIETTEL_VIP) {
                params = [DictionaryBuilder dictionarySearchReportStatus:statusId andContract:contractId andService:serviceId andIcCycleDate:icCycleDate andPartner:partnerId andStaff:staffId andOffset:self.tableView.listData.count andLimit:LIMIT];
            } else {
                params = [DictionaryBuilder dictionarySearchReportStatus:statusId andContract:contractId andService:serviceId andIcCycleDate:icCycleDate andPartner:[NSString stringWithFormat:@"%ld", [account partnerId]] andStaff:staffId andOffset:self.tableView.listData.count andLimit:LIMIT];
            }
            
            [self executeWithWithParams:params withTag:REQUEST_FOR_SERCH_REPORT];
            
        } else if(tag == TAB_REPORT_HISTORY){
            NSData *params = [DictionaryBuilder dictionarySearchReportUserHistoryWithKeyWord:@"" andCategoriesFilter:listCategories andDateCharg:icCycleDate andOffset:self.tableView.listData.count andLimit:LIMIT];
            [self executeWithWithParams:params withTag:REQUEST_FOR_SERCH_REPORT];
        }
    }
    
}

#pragma mark - action

- (void) searchButtonPressed {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    [self.tableView.listData removeAllObjects];
    [self.tableView reloadData];
    [self loadMoreData];
}

- (IBAction)employButtonPressed:(id)sender {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    if ([HTAppDelegate sharedDelegate].listStaff.count == 1) {
        [self loadListStaff];
        return;
    }
    
    [self showStaff];
}

- (void) loadListStaff {
    
    NSInteger offset = [HTAppDelegate sharedDelegate].listEmployer.count - 1;
    [self executeWithWithParams:[DictionaryBuilder dictionarySearchStaff:@"" andOffset:offset andLimit:LIMIT_LOAD_CATEGORY] withTag:REQUEST_FOR_GET_ALL_STAFF];
    
}

- (void) showStaff {
    [self showListCategories:[HTAppDelegate sharedDelegate].listStaff andIndexSelection:indexSelectionStaff andTitleCategorie:NSLocalizedString(@"TITLE_EMPLOYER", nil)];
}


- (IBAction)partnerButtonPressed:(id)sender {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    if ([HTAppDelegate sharedDelegate].listPartners.count == 1) {
        [self loadListPartners];
        return;
    }
    [self showListCategories:[HTAppDelegate sharedDelegate].listPartners andIndexSelection:indexSelectionPartners andTitleCategorie:NSLocalizedString(@"TITLE_PARNER_SEARCH", nil)];
    
}

- (void) loadListPartners {
    
    NSInteger offset = [HTAppDelegate sharedDelegate].listPartners.count - 1;
    [self executeWithWithParams:[DictionaryBuilder dictionarySearchPartners:offset andLimit:LIMIT_LOAD_CATEGORY] withTag:REQUEST_FOR_GET_ALL_PARTNERS];
}

- (IBAction)contractButtonPressed:(id)sender {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    if ([HTAppDelegate sharedDelegate].listContract.count == 1) {
        [self loadListContract];
        return;
    }
    [self showListCategories:[HTAppDelegate sharedDelegate].listContract andIndexSelection:indexSelectionContract andTitleCategorie:NSLocalizedString(@"TITLE_CONTRACT", nil)];
}

- (void) loadListContract {
    NSInteger offset = [HTAppDelegate sharedDelegate].listContract.count - 1;
    [self executeWithWithParams:[DictionaryBuilder dictionarySearchContract:offset andLimit:LIMIT_LOAD_CATEGORY] withTag:REQUEST_FOR_GET_ALL_CONTRACT];
}

- (IBAction)serviceButtonPressed:(id)sender {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    if ([HTAppDelegate sharedDelegate].listService.count == 1) {
        [self loadListService];
        return;
    }
    [self showListCategories:[HTAppDelegate sharedDelegate].listService andIndexSelection:indexSelectionService andTitleCategorie:NSLocalizedString(@"TITLE_SERVICE", nil)];
}

- (void) loadListService {
    NSInteger offset = [HTAppDelegate sharedDelegate].listService.count - 1;
    [self executeWithWithParams:[DictionaryBuilder dictionarySearchService:offset andLimit:LIMIT_LOAD_CATEGORY] withTag:REQUEST_FOR_GET_ALL_SERVICE];
}

- (IBAction)dateChargeButtonPressed:(id)sender {
    UIDatePicker *datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 100.f);
    
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:today];
    components.month--;
    components.day = 1;
    NSDate *dayOneInCurrentMonth = [gregorian dateFromComponents:components];
    
    [datePicker setDate:dayOneInCurrentMonth];
    
    [[[LGActionSheet alloc] initWithTitle:nil view:datePicker buttonTitles:@[@"Tất cả",@"Chọn"] cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil actionHandler:^(LGActionSheet *actionSheet, NSString *title, NSUInteger index) {
        
        if (index == 0) {
            _timeControlTextField.text  = @"";
            _timeControlTextField.placeholder = NSLocalizedString(@"TITLE_DATE_CHARG", nil);
        } else {
            _timeControlTextField.text = convertDateToString(datePicker.date,kPLFormatDateDD_MM_YY);
        }
        [self searchButtonPressed];
    } cancelHandler:^(LGActionSheet *actionSheet, BOOL onButton) {
        
    } destructiveHandler:nil] showAnimated:YES completionHandler:nil];
}

- (IBAction)statusButtonPressed:(id)sender {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    
    HTAccount *account = [[HTPreferences sharedPreferences] account];
    
    HTBaseTabViewController *basetabViewControler = (HTBaseTabViewController*) self.delegate;
    NSInteger tag = basetabViewControler.segmentView.tag;
    NSLog(@"statusButtonPressed %@ tag %ld", NSStringFromClass(basetabViewControler.class), tag);
    
    NSInteger typeStatus = 0;
    if([basetabViewControler isKindOfClass:[HTViettelSignViewController class]]) {
        if (tag == TAB_REPORT) {
            typeStatus = TYPE_STATUS_VIETTEL_SIGN;
        } else if(tag == TAB_REPORT_HISTORY){
            typeStatus = TYPE_STATUS_VIETTEL_SIGN;
        }
    } else {
        if (tag == TAB_REPORT) {
            if ([account partnerUserType] == PARTNER_TYPE_VIETTEL_VIP) {
                typeStatus = TYPE_STATUS_ALL;
            } else {
                if ([basetabViewControler isKindOfClass:[HTHomeViewController class]]) {
                    typeStatus = TYPE_STATUS_ADMIN;
                } else if ([basetabViewControler isKindOfClass:[HTEmployViewController class]]) {
                    typeStatus = TYPE_STATUS_EMPLOYER;
                } else if ([basetabViewControler isKindOfClass:[HTManagerViewController class]]) {
                    typeStatus = TYPE_STATUS_MANAGER;
                }
            }
        } else if(tag == TAB_REPORT_HISTORY){
            typeStatus = TYPE_STATUS_ALL;
        }
    }
    
    NSLog(@"statusButtonPressed typeStatus %ld", typeStatus);
    
    [HTStatusModel getListStatus:[HTAppDelegate sharedDelegate].listStatus andBlock:^{
        [self showListCategories:[HTAppDelegate sharedDelegate].listStatus andIndexSelection:indexSelectionStatus andTitleCategorie:NSLocalizedString(@"TITLE_STATUS", nil)];
    } andTypeStatus:typeStatus];
    
}

- (void) showListCategories:(NSMutableArray*) data
          andIndexSelection:(NSInteger)tag
          andTitleCategorie:(NSString*) strCategorie {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    HTCategoriesViewController *controller = [[HTCategoriesViewController alloc] initWithNibName:NSStringFromClass([HTCategoriesViewController class]) bundle:nil];
    controller.categories = data;
    controller.delegate = self;
    controller.indexSelected = tag;
    controller.strTitle = strCategorie;
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationSlideBottomBottom];
}

#pragma mark - delegate
- (void) didSelectCategoriesDelegate:(HTBaseCategories *)categorie
                            andIndex:(NSInteger)index {
    
    if ([categorie isKindOfClass:[HTContractModel class]]) {
        _contractTextField.text = categorie.titleDisplay;
        indexSelectionContract = index;
    } else if ([categorie isKindOfClass:[HTServiceModel class]]) {
        _serviceTextField.text = categorie.titleDisplay;
        indexSelectionService = index;
    } else if ([categorie isKindOfClass:[HTPartnerModel class]]) {
        _partnerTextField.text = categorie.titleDisplay;
        indexSelectionPartners = index;
    } else if ([categorie isKindOfClass:[HTStaffModel class]]) {
        _employTextField.text = categorie.titleDisplay;
        indexSelectionStaff = index;
    } else {
        _statusTextField.text = categorie.titleDisplay;
        indexSelectionStatus = index;
    }
    [self searchButtonPressed];
    [self dismissCategoriesPopupDelegate:nil];
}

- (void) dismissCategoriesPopupDelegate:(id)sender {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

#pragma mark - respont from server

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (tag == REQUEST_FOR_GET_ALL_CONTRACT) {
        NSMutableArray *contracs = [HTContractModel arrayOfModelsFromDictionaries:responseObject];
        [[HTAppDelegate sharedDelegate].listContract addObjectsFromArray:contracs];
        if (contracs.count == LIMIT_LOAD_CATEGORY) {
            [self loadListContract];
            return;
        }
        [self showListCategories:[HTAppDelegate sharedDelegate].listContract andIndexSelection:indexSelectionContract andTitleCategorie:NSLocalizedString(@"TITLE_CONTRACT", nil)];
        
        
    } else if (tag == REQUEST_FOR_GET_ALL_SERVICE) {
        NSMutableArray *listService = [HTServiceModel arrayOfModelsFromDictionaries:responseObject];
        [[HTAppDelegate sharedDelegate].listService addObjectsFromArray:listService];
        
        if (listService.count == LIMIT_LOAD_CATEGORY) {
            [self loadListService];
            return;
        }
        
        [self showListCategories:[HTAppDelegate sharedDelegate].listService andIndexSelection:indexSelectionService andTitleCategorie:NSLocalizedString(@"TITLE_SERVICE", nil)];
        
    } else if (tag == REQUEST_FOR_GET_ALL_STAFF) {
        NSMutableArray *staff = [HTStaffModel arrayOfModelsFromDictionaries:responseObject];
        [[HTAppDelegate sharedDelegate].listStaff addObjectsFromArray:staff];
        if (staff.count == LIMIT_LOAD_CATEGORY) {
            [self loadListStaff];
            return;
        }
        
        [self showStaff];
        
    } else if (tag == REQUEST_FOR_GET_ALL_PARTNERS) {
        NSMutableArray *partners = [HTPartnerModel arrayOfModelsFromDictionaries:responseObject];
        [[HTAppDelegate sharedDelegate].listPartners addObjectsFromArray:partners];
        if (partners.count == LIMIT_LOAD_CATEGORY) {
            [self loadListPartners];
            return;
        }
        
        [self showListCategories:[HTAppDelegate sharedDelegate].listPartners andIndexSelection:indexSelectionPartners andTitleCategorie:NSLocalizedString(@"TITLE_PARNER_SEARCH", nil)];
        
    } else if (tag == REQUEST_FOR_SERCH_REPORT) {
        [super requestSuccess:errorCode andMessage:message andResponseObject:responseObject andTagRequest:tag];
    }
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    if (tag == REQUEST_FOR_SERCH_REPORT) {
        [super requestFail:errorCode andMessage:message andResponseObject:responseObject andTagRequest:tag];
    } else {
        showAleartWithMessage(message);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}

@end
