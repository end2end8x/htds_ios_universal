//
//  HTCategoriesViewController.m
//  HTDS
//
//  Created by Anhpt67 on 7/8/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTCategoriesViewController.h"
#import "HTCategoriesTableViewCell.h"
#import "UIViewController+MJPopupViewController.h"
#import "PLConstants.h"
#import "PLFunctions.h"
#import "HTAppDelegate.h"

@interface HTCategoriesViewController ()
@property (nonatomic,strong) NSMutableArray *listData;
@end

@implementation HTCategoriesViewController

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        self = [super initWithNibName:@"HTCategoriesLandCapeViewController" bundle:nil];
    } else {
        self = [super initWithNibName:@"HTCategoriesViewController" bundle:nil];
    }
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
}

- (void) refreshData {
    self.listData = [NSMutableArray array];
    [self.listData addObjectsFromArray:self.categories];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.indexSelected inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void) setupUI {
    [super setupUI];
    self.searchTextField.placeholder = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"PLACE_HODER_SEARCH", nil),(!_strTitle?@"":[_strTitle lowercaseString])];
    [self.searchTextField setRightViewTextFieldWithImageView:@"img_btn_seach" andFrameRightView:CGRectMake(0, 0, 40,30)];
}

#pragma mark - delegate tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    HTBaseCategories *categorie = self.listData[indexPath.row];
    return [HTCategoriesTableViewCell heightForCellWithObject:categorie];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"HTCategoriesTableViewCell";
    HTCategoriesTableViewCell *cell = (HTCategoriesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.titleLabel.highlightedTextColor = bgColorApp;
    }
    HTBaseCategories *categorie = self.listData[indexPath.row];
    [cell setCellData:categorie];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HTBaseCategories *categorie = _listData[indexPath.row];
    NSLog(@"HTCategoriesViewController didSelectRowAtIndexPath %ld", indexPath.row);
    if (self.delegate) {
        [self.delegate didSelectCategoriesDelegate:categorie andIndex:indexPath.row];
    }
}

- (IBAction)dismissButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate dismissCategoriesPopupDelegate:sender];
    } 
}

#pragma mark - textfield delegate

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [self refreshData];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _searchTextField) {
        [textField resignFirstResponder];
        [self searchCategoriesLocalWithKey:trimSpaceWhiteString(textField.text)];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.searchTextField) {
        NSString *title = [NSString stringWithFormat:@"%@%@",trimSpaceWhiteString(textField.text),trimSpaceWhiteString(string)];
        NSUInteger newLength = [trimSpaceWhiteString(textField.text) length] + [trimSpaceWhiteString(string) length] - range.length;
        
        if (newLength < [trimSpaceWhiteString(textField.text) length]) {
            // Characters deleted
            title = [title substringToIndex:newLength];
        }
        
        [self searchCategoriesLocalWithKey:title];
    }
    return YES;
}

- (void) searchCategoriesLocalWithKey:(NSString*) keyWord {
    if (keyWord.length > 0) {
        NSString *nameformatString = [NSString stringWithFormat:@"titleDisplay contains[c] '%@'", keyWord];
        NSPredicate *namePredicate = [NSPredicate predicateWithFormat:nameformatString];
        _listData = (NSMutableArray*)[self.categories filteredArrayUsingPredicate:namePredicate];
        [self.tableView reloadData];
    } else  {
        [self refreshData];
        
    }
}

@end
