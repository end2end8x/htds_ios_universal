//
//  HTCategoriesViewController.h
//  HTDS
//
//  Created by Anhpt67 on 7/8/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseViewController.h"
#import "HTBaseCategories.h"
#import "BaseTextField.h"

@protocol HTCategoriesDelegate <NSObject>

- (void) didSelectCategoriesDelegate:(HTBaseCategories*) categorie
                            andIndex:(NSInteger) index;

- (void) dismissCategoriesPopupDelegate:(id) sender;

@end

@interface HTCategoriesViewController : BaseViewController


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewHeadder;
@property (weak, nonatomic) IBOutlet BaseTextField *searchTextField;


@property (nonatomic,assign) id <HTCategoriesDelegate> delegate;
@property (nonatomic,strong) NSString* strTitle;
@property (nonatomic,assign) NSInteger indexSelected;
@property (nonatomic,strong) NSMutableArray *categories;
@property (nonatomic,assign) BOOL unDisplaysSearch;




- (IBAction)dismissButtonPressed:(id)sender;

@end
