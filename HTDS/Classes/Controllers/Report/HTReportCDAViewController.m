//
//  HTReportCDAViewController.m
//  HTDS
//
//  Created by Aloha Cool on 7/15/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTReportCDAViewController.h"
#import "FileUtil.h"
#import "MBProgressHUD.h"
#import "HTServiceModel.h"
#import "HTReportTypeModel.h"

#import "PLFunctions.h"
#import "DictionaryBuilder.h"
#import "Defines.h"
#import "PLConstants.h"
#import "HTAppDelegate.h"

@interface HTReportCDAViewController ()

@end

@implementation HTReportCDAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) showReportWithType {
    NSMutableArray *listReport = [NSMutableArray array];
    for (HTReportTypeModel *reportTypeObject in [HTAppDelegate sharedDelegate].listReportType) {
        
        if (reportTypeObject.reportType.integerValue == REPORT_TYPE_CDR) {
            [listReport addObject:reportTypeObject];
        }
    }
    
    [self showListCategories:listReport andIndexSelection:indexSelectionReportType andTitleCategorie:NSLocalizedString(@"TITLE_REPORT_TYPE", nil)];
}

//- (NSString*) getFilePathToShowFile { // create foder and get file path
//    NSString *fileName = [NSString stringWithFormat:@"%@-%@-%@-%@.pdf",trimSpaceWhiteString(self.reportTextField.text),trimSpaceWhiteString(self.serviceTextField.text),trimSpaceWhiteString(self.startDatetextField.text),trimSpaceWhiteString(self.endDatetextField.text)];
//    
//    NSString *path = [[FileUtil createDirectoryWithName:kFolderExportReportCDR] stringByAppendingPathComponent:fileName];
//    return path;
//}


@end
