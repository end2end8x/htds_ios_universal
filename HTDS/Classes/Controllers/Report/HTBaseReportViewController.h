//
//  HTBaseReportViewController.h
//  HTDS
//
//  Created by Aloha Cool on 7/15/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseTextField.h"
#import "HTServiceModel.h"
#import "HTReportTypeModel.h"

@interface HTBaseReportViewController : BaseViewController {
    NSInteger typeDate;
    
    
    NSInteger indexSelectionService;
    NSInteger indexSelectionReportType;
    HTServiceModel *serviceModel;
    HTReportTypeModel *reportTypeModel;
    
}

@property (weak, nonatomic) IBOutlet BaseTextField *reportTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *serviceTextField;
@property (weak, nonatomic) IBOutlet BaseTextField *startDatetextField;
@property (weak, nonatomic) IBOutlet BaseTextField *endDatetextField; 
@property (weak, nonatomic) IBOutlet UIButton *watchButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;



- (IBAction)serviceButtonpressed:(id)sender;

- (IBAction)reportButtonPressed:(id)sender;

- (IBAction)fromDateButtonPressed:(id)sender;

- (IBAction)dueDateButtonPressed:(id)sender;

- (IBAction)watchReportButtonPressed:(id)sender;

- (void) loadListReportType;

- (void) showReportWithType;


- (NSString*) getFilePathToShowFile;

- (void) showListCategories:(NSArray*) data
          andIndexSelection:(NSInteger)tag
          andTitleCategorie:(NSString*) strCategorie;


@end
