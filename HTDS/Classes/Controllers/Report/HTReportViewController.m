//
//  HTReportViewController.m
//  HTDS
//
//  Created by TuanTD on 7/13/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTReportViewController.h"
#import "FileUtil.h"
#import "MBProgressHUD.h"
#import "HTServiceModel.h"
#import "HTReportTypeModel.h"

#import "HTAppDelegate.h"
#import "PLConstants.h"
#import "PLFunctions.h"

@interface HTReportViewController ()

@end

@implementation HTReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) showReportWithType {
    NSMutableArray *listReport = [NSMutableArray array];
    for (HTReportTypeModel *reportTypeObject in [HTAppDelegate sharedDelegate].listReportType) {
        
        if (reportTypeObject.reportType.integerValue == REPORT_TYPE_SERVICE) {
            [listReport addObject:reportTypeObject];
        }
    }
    
    [self showListCategories:listReport andIndexSelection:indexSelectionReportType andTitleCategorie:NSLocalizedString(@"TITLE_REPORT_TYPE", nil)];
}

//- (NSString*) getFilePathToShowFile { // create foder and get file path
//    NSString *fileName = [NSString stringWithFormat:@"%@-%@-%@-%@.pdf",trimSpaceWhiteString(self.reportTextField.text),trimSpaceWhiteString(self.serviceTextField.text),trimSpaceWhiteString(self.startDatetextField.text),trimSpaceWhiteString(self.endDatetextField.text)];
//    NSString *path = [[FileUtil createDirectoryWithName:kFolderExportReport] stringByAppendingPathComponent:fileName];
//    return path;
//}


@end







