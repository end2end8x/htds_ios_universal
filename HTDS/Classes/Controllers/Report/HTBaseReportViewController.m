//
//  HTBaseReportViewController.m
//  HTDS
//
//  Created by Aloha Cool on 7/15/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseReportViewController.h"
#import "ReaderViewController.h"
#import "FileUtil.h"
#import "MBProgressHUD.h"
#import "HTCategoriesViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "HTServiceModel.h"
#import "PLFunctions.h"
#import "HTAppDelegate.h"
#import "DictionaryBuilder.h"
#import "PLConstants.h"
#import "LGActionSheet.h"
#import "HTReportViewController.h"
#import "HTReportCDAViewController.h"



@interface HTBaseReportViewController ()<HTCategoriesDelegate>
@property (nonatomic,strong) NSString *formatDate;
@end

@implementation HTBaseReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.formatDate = ([self isKindOfClass:[HTReportViewController class]]?kPLFormatDate_MM_YY:kPLFormatDateDD_MM_YY);
    NSDate *now = [NSDate date];
    _endDatetextField.text = convertDateToString(now, self.formatDate);
    

    int daysToAdd = -31;
    NSDate *startDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
    
    _startDatetextField.text = convertDateToString(startDate, self.formatDate);
    
    if(reportTypeModel == nil) {
        [self loadListReportType];
    } else {
//        indexSelectionReportType = 0;
        _reportTextField.text = reportTypeModel.titleDisplay;
    }
    
    if ([self isKindOfClass:[HTReportCDAViewController class]]) {
        if(!serviceModel) {
            [self loadListService];
        } else {
        //  indexSelectionReportType = 0;
            _serviceTextField.text = serviceModel.titleDisplay;
        }
    }
}

- (void) setupUI {
    [super setupUI];
    
    textFieldPaddingWithFrame(self.reportTextField,CGRectMake(0, 0, 10, 10));
    textFieldPaddingWithFrame(self.serviceTextField,CGRectMake(0, 0, 10, 10));
    textFieldPaddingWithFrame(self.startDatetextField,CGRectMake(0, 0, 10, 10));
    textFieldPaddingWithFrame(self.endDatetextField,CGRectMake(0, 0, 10, 10));
    
    [self.reportTextField setLeftTextFieldWithImageView:@"img_row" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.serviceTextField setLeftTextFieldWithImageView:@"img_row" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.startDatetextField setLeftTextFieldWithImageView:@"ico_calendar" andFrameRightView:CGRectMake(0, 0, 40,30)];
    [self.endDatetextField setLeftTextFieldWithImageView:@"ico_calendar" andFrameRightView:CGRectMake(0, 0, 40,30)];
    self.scrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.watchButton.frame) + 40);
}

- (void) setupData {
    indexSelectionReportType = -1;
    indexSelectionService = -1;
}

#pragma mark - action

- (IBAction)serviceButtonpressed:(id)sender {
    if ([HTAppDelegate sharedDelegate].listService.count == 1) {
        [self loadListService];
        return;
    }
    NSMutableArray *listService = [NSMutableArray array];
    for (int i = 1; i < [HTAppDelegate sharedDelegate].listService.count; i++) {
        [listService addObject:[HTAppDelegate sharedDelegate].listService[i]];
    }
    [self showListCategories:listService andIndexSelection:indexSelectionService andTitleCategorie:NSLocalizedString(@"TITLE_SERVICE", nil)];
}

- (void) loadListService {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    
    
    NSInteger offset = [HTAppDelegate sharedDelegate].listService.count - 1;
    [self executeWithWithParams:[DictionaryBuilder dictionarySearchService:offset andLimit:LIMIT_LOAD_CATEGORY] withTag:REQUEST_FOR_GET_ALL_SERVICE];
}

- (IBAction)reportButtonPressed:(id)sender {
    if ([HTAppDelegate sharedDelegate].listReportType.count == 0) {
        [HTAppDelegate sharedDelegate].listReportType = [NSMutableArray array];
        [self loadListReportType];
        return;
    }
    [self showReportWithType];
}

- (void) showReportWithType {
    
}

- (void) loadListReportType {
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    NSInteger offset = [HTAppDelegate sharedDelegate].listReportType.count;
    [self executeWithWithParams:[DictionaryBuilder dictionarySearchReportType:offset andLimit:LIMIT_LOAD_CATEGORY] withTag:REQUEST_FOR_GET_ALL_REPORTTYPE];
}

- (IBAction)fromDateButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    UIDatePicker *datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 100.f);
    
    [[[LGActionSheet alloc] initWithTitle:nil view:datePicker buttonTitles:@[@"Chọn"] cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil actionHandler:^(LGActionSheet *actionSheet, NSString *title, NSUInteger index) {
        _startDatetextField.text = convertDateToString(datePicker.date,self.formatDate);
    } cancelHandler:^(LGActionSheet *actionSheet, BOOL onButton) {
        
    } destructiveHandler:nil] showAnimated:YES completionHandler:nil];
}

- (IBAction)dueDateButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    UIDatePicker *datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 100.f);
    
    [[[LGActionSheet alloc] initWithTitle:nil view:datePicker buttonTitles:@[@"Chọn"] cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil actionHandler:^(LGActionSheet *actionSheet, NSString *title, NSUInteger index) {
        _endDatetextField.text = convertDateToString(datePicker.date,self.formatDate);
    } cancelHandler:^(LGActionSheet *actionSheet, BOOL onButton) {
        
    } destructiveHandler:nil] showAnimated:YES completionHandler:nil];
}

- (IBAction)watchReportButtonPressed:(id)sender {
    [self.view endEditing:YES];
    NSString *message = nil;
    if (isEmptyString(_reportTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_REPORT", nil);
    } else if (isEmptyString(_serviceTextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_SERVICE", nil);
    } else if (isEmptyString(_startDatetextField.text)) {
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_START_MONTH", nil);
    } else if (isEmptyString(_endDatetextField.text)){
        message = NSLocalizedString(@"MESSAGE_PLEASE_INSERT_DUE_MONTH", nil);
    } else {
        NSDate* startDate = convertToDate(_startDatetextField.text, self.formatDate);
        NSDate* endDate = convertToDate(_endDatetextField.text, self.formatDate);
        if([startDate timeIntervalSince1970] > [endDate timeIntervalSince1970]) {
            message = NSLocalizedString(@"MESSAGE_PLEASE_MODIFY_START_MONTH", nil);
        }
    }
    
    if (message) {
        showAleartWithMessage(message);
        return;
    }
    
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    NSString *executableFileMD5Hash = [FileUtil getMd5FileWithPath:[self getFilePathToShowFile]];
    
    NSString *fromDate = [NSString stringWithFormat:@"01-%@",trimSpaceWhiteString(self.startDatetextField.text)];
    NSString *dueDate = [NSString stringWithFormat:@"01-%@",trimSpaceWhiteString(self.endDatetextField.text)];
    if (![self isKindOfClass:[HTReportViewController class]]) {
        fromDate = trimSpaceWhiteString(self.startDatetextField.text);
        dueDate  = trimSpaceWhiteString(self.endDatetextField.text);
    }
    
    NSData *params = [DictionaryBuilder dictionaryForExportReport:serviceModel andReportType:reportTypeModel andFromDate:fromDate endDate:dueDate andMd5File:executableFileMD5Hash];
    [self executeWithWithParams:params withTag:REQUEST_FOR_EXPORT_REQUEST];
}

- (void) showListCategories:(NSMutableArray*) data
          andIndexSelection:(NSInteger)tag
          andTitleCategorie:(NSString*) strCategorie {
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    HTCategoriesViewController *controller = [[HTCategoriesViewController alloc] initWithNibName:NSStringFromClass([HTCategoriesViewController class]) bundle:nil];
    controller.categories = data;
    controller.delegate = self;
    controller.indexSelected = tag;
    controller.strTitle = strCategorie;
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationSlideBottomBottom];
}

#pragma mark - delegate

- (void) didSelectCategoriesDelegate:(HTBaseCategories *) categorie
                            andIndex:(NSInteger)index {
    
    if ([categorie isKindOfClass:[HTServiceModel class]]) {
        indexSelectionService = index;
        _serviceTextField.text = categorie.titleDisplay;
        serviceModel = (HTServiceModel*) categorie;
    } else if ([categorie isKindOfClass:[HTReportTypeModel class]]) {
        indexSelectionReportType = index;
        _reportTextField.text = categorie.titleDisplay;
        reportTypeModel = (HTReportTypeModel*)categorie;
    }
    [self dismissCategoriesPopupDelegate:nil];
}

- (void) dismissCategoriesPopupDelegate:(id)sender {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
}

#pragma mark - respont from server

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    
    if (tag == REQUEST_FOR_EXPORT_REQUEST) {
        if (responseObject != nil && ![responseObject isEqual:[NSNull class]] && ((NSString*) responseObject).length > 0) {
            NSString *strPath = [self getFilePathToShowFile];
            NSData *decodeData = [[NSData alloc] initWithBase64EncodedString:responseObject options:NSDataBase64DecodingIgnoreUnknownCharacters];
            if ([FileUtil fileExistsAtPath:strPath]) {
                [FileUtil deleteFileAtPath:strPath];
            } 
            [decodeData writeToFile:strPath atomically:NO];
            [self showFileWithPath:strPath andShowSingButton:NO andDocument:nil];
        } else {
            showAleartWithMessage(NSLocalizedString(@"MESSAGE_DOWNLOAD_FAIL", nil));
        }
    } else if (tag == REQUEST_FOR_GET_ALL_SERVICE) {
        NSMutableArray *listService = [HTServiceModel arrayOfModelsFromDictionaries:responseObject];
        [[HTAppDelegate sharedDelegate].listService addObjectsFromArray:listService];
        if (listService.count == LIMIT_LOAD_CATEGORY) {
            [self loadListService];
        } else {
            
            NSMutableArray *listService = [NSMutableArray array];
            for (int i = 1; i < [HTAppDelegate sharedDelegate].listService.count; i++) {
                [listService addObject:[HTAppDelegate sharedDelegate].listService[i]];
            }
            
            // MARK: bo sung service mac dinh
            if ([self isKindOfClass:[HTReportCDAViewController class]]) {
                if(!serviceModel) {
                    if(listService && listService.count > 0 ) {
                        serviceModel = (HTServiceModel*) [listService objectAtIndex:0];
                        indexSelectionReportType = 0;
                        _serviceTextField.text = serviceModel.titleDisplay;
                    }
                } else {
                    [self showListCategories:listService andIndexSelection:indexSelectionService andTitleCategorie:NSLocalizedString(@"TITLE_SERVICE", nil)];
                }
            } else {
                [self showListCategories:listService andIndexSelection:indexSelectionService andTitleCategorie:NSLocalizedString(@"TITLE_SERVICE", nil)];
            }
        }
    } else if (tag == REQUEST_FOR_GET_ALL_REPORTTYPE) {
        NSMutableArray *listReportType = [HTReportTypeModel arrayOfModelsFromDictionaries:responseObject];
        [[HTAppDelegate sharedDelegate].listReportType addObjectsFromArray:listReportType];
        
        if (listReportType.count == LIMIT_LOAD_CATEGORY) {
            [self loadListReportType];
        } else {
            if(reportTypeModel == nil) {
                if ([self isKindOfClass:[HTReportViewController class]]) {
                    NSLog(@"listReportType.count %ld [HTAppDelegate sharedDelegate].listReportType %ld", listReportType.count, [HTAppDelegate sharedDelegate].listReportType.count);

                    if(listReportType == nil || listReportType.count == 0 ) {
                        listReportType = [HTAppDelegate sharedDelegate].listReportType;
                    }
                    for (int i = 0; i < listReportType.count; i++) {
                        HTReportTypeModel *reportTypeObject = [listReportType objectAtIndex:i];
                        if (reportTypeObject.reportType.integerValue == REPORT_TYPE_SERVICE) {
                            reportTypeModel = (HTReportTypeModel*) reportTypeObject;
                            indexSelectionReportType = 0;
                            _reportTextField.text = reportTypeObject.titleDisplay;
                            break;
                        }
                    }
                }
                if ([self isKindOfClass:[HTReportCDAViewController class]]) {
                    if(listReportType == nil || listReportType.count == 0 ) {
                        listReportType = [HTAppDelegate sharedDelegate].listReportType;
                    }
                    for (int i = 0; i < listReportType.count; i++) {
                        HTReportTypeModel *reportTypeObject = [listReportType objectAtIndex:i];
                        if (reportTypeObject.reportType.integerValue == REPORT_TYPE_CDR) {
                            indexSelectionReportType = 0;
                            _reportTextField.text = reportTypeObject.titleDisplay;
                            reportTypeModel = (HTReportTypeModel*) reportTypeObject;
                            break;
                        }
                    }
                }
            } else {
                [self showReportWithType];
            }
        }
    } 
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    if (tag == REQUEST_FOR_EXPORT_REQUEST && errorCode == FILE_DUPLICATE) {
        NSString *strPath = [self getFilePathToShowFile];
        [self showFileWithPath:strPath andShowSingButton:NO andDocument:nil];
    } else {
        showAleartWithMessage(message);
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (NSString*) getFilePathToShowFile {
    
    NSString *fileName = [NSString stringWithFormat:@"report.pdf"];
    NSString *path = [[FileUtil createDirectoryWithName:kFolderExportReport] stringByAppendingPathComponent:fileName];
    return path;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self setupUI];
}

@end











