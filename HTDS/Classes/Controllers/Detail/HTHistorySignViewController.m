//
//  HTHistorySignViewController.m
//  HTDS
//
//  Created by TuanTD on 7/16/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTHistorySignViewController.h"
#import "HTHistoryTableViewCell.h"
#import "MBProgressHUD.h"
#import "HTHistoryModel.h"
#import "SVPullToRefresh.h"

#import "DictionaryBuilder.h"
#import "Defines.h"
#import "PLConstants.h"
#import "PLFunctions.h"

@interface HTHistorySignViewController ()
@property (nonatomic,strong) NSMutableArray *listHistory;
@end

@implementation HTHistorySignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) setupUI {
    [super setupUI];
    self.listHistory = [NSMutableArray array];
    
    __weak HTHistorySignViewController *weakSelf = self;
    [weakSelf.tableView addInfiniteScrollingWithActionHandler:^{ // load more news
        [weakSelf loadMoreData];
        weakSelf.tableView.showsInfiniteScrolling = YES;
    }];
    
    // refres datata
    self.refreshHistoryControl = [[UIRefreshControl alloc] init];
    [self.refreshHistoryControl addTarget:self action:@selector(pullToRefressData) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshHistoryControl];
    
    self.tableView.showsInfiniteScrolling = NO;
}

- (void) pullToRefressData {
    [super pullToRefressData];
    [self.listHistory removeAllObjects];
    [self.tableView reloadData];
    [self loadMoreData];
}

- (void) refreshData {
    [super refreshData];
    showMBProgressHUD(self.tableView, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    [self loadMoreData];
}

- (void) loadMoreData {
    [super loadMoreData];
    NSData *params = [DictionaryBuilder dictionarySearchHistoryReport:self.listHistory.count andLimit:LIMIT andReport:self.doccumentModel];
    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_HISTORY_SIGN];
}


#pragma mark- delegate,datasource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 38;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.listHistory.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"HTHistoryTableViewCell";
    HTHistoryTableViewCell *cell = (HTHistoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.controller = self;
    HTHistoryModel *history = self.listHistory[indexPath.row];
    [cell setCellData:history];
    
    return cell;
}

- (IBAction)dismisButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate dismissHistoryPopupDelegate:sender];
    }
    
}

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    if (tag == REQUEST_FOR_GET_HISTORY_SIGN) {
        NSMutableArray *list = [HTHistoryModel parserListHistory:responseObject];
        [self.listHistory addObjectsFromArray:list];
        [self.tableView reloadData];
    }
    [self hidenloadingAndRefressh];
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    showAleartWithMessage(message);
    [self hidenloadingAndRefressh];
}

- (void) hidenloadingAndRefressh {
    [MBProgressHUD hideHUDForView:self.tableView animated:YES];
    [self.refreshHistoryControl endRefreshing];
    [self.tableView.infiniteScrollingView stopAnimating];
    self.tableView.showsInfiniteScrolling = !(self.listHistory.count%LIMIT > 0);
}

-(void) removeFromParentViewController {
    [super removeFromParentViewController];
    if(self.tableView!=nil) {
        self.tableView.delegate = nil;
        self.tableView.dataSource = nil;
        self.tableView = nil;
    }
}


@end








