//
//  HTDoccumentDetailViewController.h
//  HTDS
//
//  Created by Anhpt67 on 7/7/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseViewController.h"
#import "HTDocumentModel.h"

enum {
    MENU_ITEM_EMPLOYER = 1,
    MENU_ITEM_MANAGER
};

enum {
    TAG_ALLEART_CANCEL = 19,
    TAG_ALLEART_ADMIN = 20,
    TAG_ALLEART_COMMENT = 21,
    TAG_ACTION_SHEET_SHOW_FILE_OFTION = 22
};


@interface HTDoccumentDetailViewController : BaseViewController <UIActionSheetDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate> {
    BOOL showHistory;
}

@property (nonatomic,strong) NSMutableArray *listInfor;
@property (weak, nonatomic) IBOutlet UITableView *infoTableView;
@property (nonatomic,strong) HTDocumentModel *documentModel;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *titleDoccument;
@property (weak, nonatomic) IBOutlet UIButton *bookmarkbutton;
@property (nonatomic,assign) NSInteger typeStaff;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;



- (IBAction)bookMarkButtonPressed:(id)sender;
- (void) downLoadFileDelegate:(NSString*)jasperId;
- (void) historyButtonPressedDelegate:(id) data;

@end
