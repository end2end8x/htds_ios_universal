//
//  ReasonsViewController.m
//  HTDS
//
//  Created by TuanTD on 8/3/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTReasonsViewController.h"
#import "PLFunctions.h"
#import "DictionaryBuilder.h"
#import "PLConstants.h"
#import "HTReasonModel.h"
#import "MBProgressHUD.h"
#import "HTAppDelegate.h"
#import "HTPreferences.h"

@interface HTReasonsViewController () {
    HTReasonModel *reasonModel;
}

@property (nonatomic,strong) NSMutableArray *listData;
@property (nonatomic,strong) UIImageView *leftImage;

@end

@implementation HTReasonsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.leftImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40,30)];
    [self.leftImage setContentMode:UIViewContentModeCenter];
    [self.leftImage setImage:[UIImage imageNamed:@"img_row"]];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [self.leftImage setUserInteractionEnabled:YES];
    [self.leftImage addGestureRecognizer:singleTap];
    
//    [self.reasonsTextfield addGestureRecognizer:singleTap];
    self.reasonsTextfield.leftView = self.leftImage;
    self.reasonsTextfield.leftViewMode = UITextFieldViewModeAlways;
    
    self.descriptiontextfield.placeholder = NSLocalizedString(@"PLACE_HODER_DESCRIPTION", nil);
    self.descriptiontextfield.layer.borderWidth = 1.0f;
    self.descriptiontextfield.layer.borderColor = [UIColor colorWithRed:226.0f/255.0f green:228.0f/255.0f blue:229.0f/255.0f alpha:1].CGColor;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidenKeyBoard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) hidenKeyBoard {
    [self.view endEditing:YES];
}

-(void) tapDetected {
    [self searchCategoriesLocalWithKey:@""];
    [self rotateMoreInfoImageViewWithRadian:M_PI andImageView:self.leftImage];
//    [self rotateMoreInfoImageViewWithRadian:0 andImageView:self.leftImage];
    
//    if ([HTAppDelegate sharedDelegate].listReason.count == 0) {
//        [self refreshData];
//    } else {
//        [self.reasonsTextfield becomeFirstResponder];
//        if (![self.sugestionTableView isHidden]) {
//            [self rotateMoreInfoImageViewWithRadian:M_PI andImageView:self.leftImage];
//            [self.listData addObjectsFromArray:[HTAppDelegate sharedDelegate].listReason];
//            [self resizeTableSuggestion];
//        } else {
//            [self rotateMoreInfoImageViewWithRadian:0 andImageView:self.leftImage];
//            [self resetAllData];
//            self.sugestionTableView.hidden = YES;
//        }
//    }
}

- (void)rotateMoreInfoImageViewWithRadian:(float)radian
                             andImageView:(UIImageView*) imageView {
    
    CGAffineTransform tranform = CGAffineTransformMakeRotation(radian);
    [UIView animateWithDuration:0.3 animations:^{
        imageView.transform = tranform;
    } completion:^(BOOL finished) {
    }];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addCloseButtonItem];
}

- (void) setupData {
    [super refreshData];
    self.listData = [NSMutableArray array];
}

- (void) refreshData {
    [super refreshData];
    
    if([HTAppDelegate sharedDelegate].listReason.count == 0) {
        showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
        [self loadListReason];
    }
}

- (void) loadListReason {
    NSData *params = [DictionaryBuilder dictionaryReason:LIMIT_LOAD_CATEGORY andOffset:[HTAppDelegate sharedDelegate].listReason.count];
    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_LIST_REASON];
}

- (void) setupUI {
    self.scrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.sendCommentButton.frame) + 40);
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


#pragma mark - Request
- (void) unReviewReport:(HTReasonModel*)reason andNote:(NSString*)note {
    showMBProgressHUD(self.view, @"LOADDING_UNREVIEW", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    
    NSData *params = [DictionaryBuilder dictionarySignatureReport:self.documentModel andPath:kPathUnReviewReport andReason:reason andNote:note];
    [self executeWithWithParams:params withTag:REQUEST_FOR_UNREVIEW_REPORT];
}

- (void) unSignatureReport:(HTReasonModel*)reason andNote:(NSString*)note{ // huỷ ký
    showMBProgressHUD(self.view, @"LOADDING_LOGGING_CANCEL_SIGNATURE", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    NSData *params = [DictionaryBuilder dictionarySignatureReport:self.documentModel andPath:kPathCancelSignatureReport andReason:reason andNote:note];
    [self executeWithWithParams:params withTag:REQUEST_FOR_UNSIGNATURE_REPORT];
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.reasonsTextfield) {
        [self hidenSuggesstion];
    }
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [textField becomeFirstResponder];
    [self resetAllData];
    [self.listData addObjectsFromArray:[HTAppDelegate sharedDelegate].listReason];
    [self resizeTableSuggestion];
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self searchCategoriesLocalWithKey:@""];
    [self rotateMoreInfoImageViewWithRadian:M_PI andImageView:self.leftImage];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *title = @"";
    if (textField == self.reasonsTextfield) {
        title = [NSString stringWithFormat:@"%@%@",trimSpaceWhiteString(textField.text),trimSpaceWhiteString(string)];
        NSUInteger newLength = [trimSpaceWhiteString(textField.text) length] + [trimSpaceWhiteString(string) length] - range.length;
    
        
        if (newLength < [trimSpaceWhiteString(textField.text) length]) {
            // Characters deleted
            title = [title substringToIndex:newLength];
        }
        
        [self searchCategoriesLocalWithKey:title];
    }
    [self searchCategoriesLocalWithKey:title];
    return NO;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    BOOL returnKey = [text rangeOfString: @"\n"].location != NSNotFound;
//    return (trimSpaceWhiteString(textView.text).length + range.length < 5) || returnKey;
    if(textView == self.descriptiontextfield) {
        return [[textView text] length] - range.length + text.length  <= TEXT_VIEW_REASON_LENGTH;
    }
    return YES;
}


- (void) searchCategoriesLocalWithKey:(NSString*) keyWord {
    if (keyWord.length > 0) {
        [self resetAllData];
        NSString *nameformatString = [NSString stringWithFormat:@"titleDisplay contains[c] '%@'", keyWord];
        NSPredicate *namePredicate = [NSPredicate predicateWithFormat:nameformatString];
        NSArray *list = (NSMutableArray*)[[HTAppDelegate sharedDelegate].listReason filteredArrayUsingPredicate:namePredicate];
        if (list.count == 0) {
            [self.listData addObjectsFromArray:[HTAppDelegate sharedDelegate].listReason];
        } else {
            [self.listData addObjectsFromArray:list];
        }
        NSLog(@"count data reasoon reload: %@",[NSNumber numberWithInteger:self.listData.count]);
        NSLog(@"count data List reasoon: %@",[NSNumber numberWithInteger:list.count]);
        NSLog(@"count data List get reasoon: %@",[NSNumber numberWithInteger:[HTAppDelegate sharedDelegate].listReason.count]);
        [self resizeTableSuggestion];
    } else  {
        [self resetAllData];
        [self.listData addObjectsFromArray:[HTAppDelegate sharedDelegate].listReason];
        [self resizeTableSuggestion];
    }
}

- (void) hidenSuggesstion {
    [self.reasonsTextfield resignFirstResponder];
    self.sugestionTableView.hidden = YES;
}

- (void) resetAllData {
    self.listData = [NSMutableArray array];
    [self.sugestionTableView reloadData];
}

- (void) resizeTableSuggestion {
    self.sugestionTableView.hidden = NO;
    CGFloat heightMax = self.view.frame.size.height - 216 - CGRectGetMinY(self.sugestionTableView.frame);
    CGRect frame = self.sugestionTableView.frame;
    if (self.sugestionTableView.frame.size.height < heightMax) {
        float height = self.listData.count*45;
        frame.size.height = (height<heightMax?height:heightMax);
    } else {
        frame.size.height = heightMax;
    }
    frame.size.width = self.view.frame.size.width;
    frame.origin.y   = CGRectGetMaxY(self.reasonsTextfield.frame);
    self.sugestionTableView.frame = frame;
    [self.sugestionTableView reloadData];
}

#pragma mark - tableview


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    HTReasonModel *reason = self.listData[indexPath.row];
    cell.textLabel.text = reason.titleDisplay;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    reasonModel = self.listData[indexPath.row];
    self.reasonsTextfield.text = reasonModel.titleDisplay;
    [self hidenSuggesstion];
    [self rotateMoreInfoImageViewWithRadian:0 andImageView:self.leftImage];
}

#pragma mark - service

- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    if (tag ==  REQUEST_FOR_GET_LIST_REASON) {
        NSMutableArray *list =   [HTReasonModel arrayOfModelsFromDictionaries:responseObject];
        if (![HTAppDelegate sharedDelegate].listReason) {
            [HTAppDelegate sharedDelegate].listReason = [NSMutableArray array];
        }
        [[HTAppDelegate sharedDelegate].listReason addObjectsFromArray:list];
        if (list.count == LIMIT_LOAD_CATEGORY) {
            [self loadListReason];
            return;
        }
    } else if (tag == REQUEST_FOR_UNREVIEW_REPORT || tag == REQUEST_FOR_UNSIGNATURE_REPORT) {
  //MARK: HUY KI HUY DIUYET o day
        HTAccount *account = [HTPreferences sharedPreferences].account;
        NSString *notify = nil;
        
        NSDate* date = convertToDate(_documentModel.icCycleDate, kPLFormatDate_MMDD_YY);
        NSString* dateStr = convertDateToString(date, kPLFormatDateDD_MM_YY);
        
        if (tag == REQUEST_FOR_UNREVIEW_REPORT) {
            notify = [NSString stringWithFormat:@"Đ/c %@ vừa TỪ CHỐI DUYỆT biên bản: %@ - %@ - Kỳ đối soát: %@", [account fullName], _documentModel.contractNo, _documentModel.serviceName, dateStr];
        } else if (tag == REQUEST_FOR_UNSIGNATURE_REPORT) {
            notify = [NSString stringWithFormat:@"Đ/c %@ vừa TỪ CHỐI KÝ biên bản: %@ - %@ - Kỳ đối soát: %@", [account fullName], _documentModel.contractNo, _documentModel.serviceName, dateStr];
        }
        if (notify) {
            parsePush(account, [NSArray arrayWithObjects:[NSString stringWithFormat:@"partnerId_%ld", account.partnerId], nil] , notify);
        }
        
        self.documentModel.status = convertIntegerToString(STATUS_REPORT_NEWS);
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kPLSignatureReportNotification object:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
        
        showAleartWithMessage(message);
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    showAleartWithMessage(message);
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (IBAction)sendCommentButtonPressed:(id)sender {
    if (isEmptyString(self.descriptiontextfield.text)) {
        showAleartWithMessage(NSLocalizedString(@"MESSAGE_COMMENT_CANCEL", nil));
        return;
    } else if (reasonModel == nil) {
        showAleartWithMessage(NSLocalizedString(@"MESSAGE_SELLECT_REASOON", nil));
        return;
    }
    
    BOOL ischeckReasoon = NO;
    NSString *title = trimSpaceWhiteString(self.reasonsTextfield.text);
    for (HTReasonModel *resasoon in [HTAppDelegate sharedDelegate].listReason) {
        if ([title isEqualToString:resasoon.titleDisplay]) {
            ischeckReasoon = YES;
            break;
        }
    }
    
    if (!ischeckReasoon) {
        showAleartWithMessage(NSLocalizedString(@"MESSAGE_SELLECT_ERROR_REASOON", nil));
        return;
    }
    //MARK: Button huy ki huy duyet
    if (self.type == TYPE_UN_APPROVE) {
        [self unReviewReport:reasonModel andNote:_descriptiontextfield.text];
    } else if(self.type == TYPE_UN_SIGNATURE){
        [self unSignatureReport:reasonModel andNote:_descriptiontextfield.text];
    }
}
@end
