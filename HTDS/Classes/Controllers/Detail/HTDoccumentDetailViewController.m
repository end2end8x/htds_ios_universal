//
//  HTDoccumentDetailViewController.m
//  HTDS
//
//  Created by Anhpt67 on 7/7/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTDoccumentDetailViewController.h"
#import "HTDoccumentItemTableViewCell.h"
#import "HTAddInvoiceViewController.h"
#import "HTHistorySignViewController.h"
#import "HTInfoDocumentModel.h"
#import "HTFileTableViewCell.h"
#import "HTReportBookMarkManaged.h"
#import "HTReportRecentManaged.h"
#import "FileUtil.h"
#import "MBProgressHUD.h"
#import "UIViewController+MJPopupViewController.h"
#import "HTPreferences.h"
#import "PLConstants.h"
#import "PLFunctions.h"
#import "DictionaryBuilder.h"
#import "Defines.h"
#import "HTAppDelegate.h"
#import "HTHistoryModel.h"
#import "HTHistoryTableViewCell.h"
#import "HTReasonsViewController.h"
#import "LGActionSheet.h"
#import "HTListInvoiceViewController.h"
#import "Defines.h"

@interface HTDoccumentDetailViewController ()<HTHistoryDelegate,UIDocumentInteractionControllerDelegate>

@end

@implementation HTDoccumentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    recognizer.delegate = self;
    
    parseBadgeDecrease(0);
}

-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gestureRecognizer{
    NSLog(@"UISwipeGestureRecognizerDirectionRight");
    showAleartWithMessage(@"UISwipeGestureRecognizerDirectionRight");
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self checkShowSignButton];
    
}

- (void) checkShowSignButton {
    HTAccount *account = [HTPreferences sharedPreferences].account;
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) { // tài khoản anh giang chỉ có quyền ký ko huỷ biên bản viettel đã ký
        if (_documentModel.status.integerValue == STATUS_MANAGER_SIGN) {
            [self addPenSignButtonItem];
        } else {
            self.navigationItem.rightBarButtonItem = nil;
        }
    } else {
        
        if (account.partnerUserType == PARTNER_TYPE_EMPLOY) {
            if ([self checkPermisonReviewOrUnreview]) {
                [self addPenSignButtonItem];
            } else {
                self.navigationItem.rightBarButtonItem = nil;
            }
        } else if (account.partnerUserType == PARTNER_TYPE_ADMIN) {
            if ([self checkPermisonReviewOrUnreview] || [self checkPermisionSignatureOrUnSignature]) {
                [self addPenSignButtonItem];
            } else {
                self.navigationItem.rightBarButtonItem = nil;
            }
        } else {
            if ([self checkPermisionSignatureOrUnSignature]) {
                [self addPenSignButtonItem];
            } else {
                self.navigationItem.rightBarButtonItem = nil;
            }
        }
    }
}

- (BOOL) checkPermisionSignatureOrUnSignature {
    if (_documentModel.status.integerValue == STATUS_WAITING_SIGNATURE ||
        _documentModel.status.intValue == STATUS_MANAGER_SIGN ) {
        return YES;
    }
    
    return NO;
}

- (BOOL) checkPermisonReviewOrUnreview {
    if (_documentModel.status.integerValue == STATUS_WAITING_REVIEW ||
        _documentModel.status.intValue == STATUS_WAITING_SIGNATURE ) {
        return YES;
    }
    
    return NO;
}

- (void) setupUI {
    [super setupUI];
    
    //    [_bookmarkbutton setSelected:_doccumentModel.isBookmark];
    //    [_doccumentModel saveReportToRecent];
}

- (void) refreshData {
    [super refreshData];
    _titleDoccument.text = _documentModel.contractName;
    self.listInfor = [HTInfoDocumentModel getListInfoDocument:_documentModel];
    [self.infoTableView reloadData];
}

#pragma mark - action

- (UIActionSheet*) showActionSheetSignature {
    UIActionSheet *actionSheet  = [[UIActionSheet alloc] initWithTitle:nil  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BUTTON_TITLE_SIGN", nil),NSLocalizedString(@"BUTTON_TITLE_REFUSE_SIGNATURE", nil), nil];
    return actionSheet;
}

- (UIActionSheet*) showActionSheetUnSignature {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BUTTON_TITLE_UNSIGN", nil),nil];
    return actionSheet;
}

- (UIActionSheet*) showActionSheetReview {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BUTTON_TITLE_REVIEW", nil),NSLocalizedString(@"BUTTON_TITLE_REFUSE_APPROVE", nil), nil];
    return actionSheet;
}

- (UIActionSheet*) showActionSheetUnReview {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BUTTON_TITLE_UNREVIEW", nil), nil];
    return actionSheet;
}


- (void) penSignButtonPressed {
    [super penSignButtonPressed];
    
    UIActionSheet *actionSheet = nil;
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) { // tài khoản anh giang chỉ có quyền ký
        actionSheet = [self showActionSheetSignature];
    } else if (account.partnerUserType == PARTNER_TYPE_EMPLOY) {
        if (_documentModel.status.integerValue == STATUS_WAITING_REVIEW) { // biên bản Chưa duyệt
            actionSheet =  [self showActionSheetReview];
        } else { // huỷ duyệt biên bản
            actionSheet =  [self showActionSheetUnReview];
            actionSheet.tag = TAG_ALLEART_CANCEL;
        }
    } else if (account.partnerUserType == PARTNER_TYPE_MANAGER)  {
        if (_documentModel.status.integerValue == STATUS_WAITING_SIGNATURE) { // biên bản đợi ký
            actionSheet =  [self showActionSheetSignature];
        } else { // biên bản huỷ ký
            actionSheet =  [self showActionSheetUnSignature];
            actionSheet.tag = TAG_ALLEART_CANCEL;
        }
    } else {
        if (_documentModel.status.integerValue == STATUS_WAITING_REVIEW ) {
            actionSheet =  [self showActionSheetReview];
        } else if (_documentModel.status.integerValue == STATUS_WAITING_SIGNATURE) {
            if (self.typeStaff == MENU_ITEM_EMPLOYER) {
                actionSheet =   [self showActionSheetUnReview];
                actionSheet.tag = TAG_ALLEART_CANCEL;
            } else if (self.typeStaff == MENU_ITEM_MANAGER) {
                actionSheet =   [self showActionSheetSignature];
            } else {
                actionSheet = [[UIActionSheet alloc] initWithTitle:nil  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BUTTON_TITLE_SIGN", nil),NSLocalizedString(@"BUTTON_TITLE_REFUSE_SIGNATURE", nil), NSLocalizedString(@"BUTTON_TITLE_UNREVIEW", nil),nil];
                actionSheet.tag = TAG_ALLEART_ADMIN;
            }
        } else if (_documentModel.status.integerValue == STATUS_MANAGER_SIGN) {
            actionSheet = [self showActionSheetUnSignature];
            actionSheet.tag = TAG_ALLEART_CANCEL;
        }
    }
    
    [actionSheet showInView:self.view];
}

- (IBAction)bookMarkButtonPressed:(id)sender {
    [_documentModel bookMarkOrUnBookMark];
    [_bookmarkbutton setSelected:_documentModel.isBookmark];
}

#pragma mark - Actonsheetdelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        
        if (actionSheet.tag == TAG_ALLEART_CANCEL || (actionSheet.tag == TAG_ALLEART_ADMIN && buttonIndex == BUTTON_FORSIGNED)) {
            
            // MARK: Huy ki duyet chua can alert truoc
            //            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"MESSAGE_CONFIRM_WORK", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_ALLEART_LOGOUT_LEFT", nil) otherButtonTitles:NSLocalizedString(@"BUTTON_ALLEART_LOGOUT_RIGHT", nil), nil];
            //            alertView.tag = TAG_ALLEART_COMMENT;
            //            [alertView show];
            HTAccount *account = [HTPreferences sharedPreferences].account;
            if (account.partnerUserType == PARTNER_TYPE_EMPLOY ||
                account.partnerUserType == PARTNER_TYPE_ADMIN) {
                if (_documentModel.status.integerValue == STATUS_MANAGER_SIGN) {
                    [self showReasoonWithType:TYPE_UN_SIGNATURE];
                } else {
                    [self showReasoonWithType:TYPE_UN_APPROVE];
                }
            } else if (account.partnerUserType == PARTNER_TYPE_MANAGER) {
                [self showReasoonWithType:TYPE_UN_SIGNATURE];
            }
        } else if (actionSheet.tag == TAG_ACTION_SHEET_SHOW_FILE_OFTION) {
            NSString *strPath = [self getFilePathToShowFile];
            if (buttonIndex == 0) {
                [self showOpenInViewWithPath:strPath];
            } else if (buttonIndex == 1){
                BOOL isShow = (_documentModel.status.integerValue != STATUS_VIETTEL ||
                               _documentModel.status.integerValue != STATUS_ACCEPTED);
                [self showFileWithPath:strPath andShowSingButton:isShow andDocument:_documentModel];
            }
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"MESSAGE_CONFIRM_WORK", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_ALLEART_LOGOUT_LEFT", nil) otherButtonTitles:NSLocalizedString(@"BUTTON_ALLEART_LOGOUT_RIGHT", nil), nil];
            alertView.tag = buttonIndex;
            [alertView show];
        }
    }
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger newLength = [trimSpaceWhiteString(textField.text) length] + [trimSpaceWhiteString(string) length] - range.length;
    return newLength <= 50;
}

#pragma mark - delegate

- (void) showReasoonWithType:(NSInteger) type {
    [self dismissViewControllerAnimated:YES completion:nil];
    HTReasonsViewController *reasonViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([HTReasonsViewController class])];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:reasonViewController];
    customizeAppearance(navigationController);
    reasonViewController.documentModel = self.documentModel;
    reasonViewController.type = type;
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        HTAccount *account = [HTPreferences sharedPreferences].account;
        if (alertView.tag == TAG_ALLEART_COMMENT) {
            if (account.partnerUserType == PARTNER_TYPE_EMPLOY ||
                account.partnerUserType == PARTNER_TYPE_ADMIN) {
                if (_documentModel.status.integerValue == STATUS_MANAGER_SIGN) { // tu choi ki
                    [self showReasoonWithType:TYPE_UN_SIGNATURE];
                } else {
                    [self showReasoonWithType:TYPE_UN_APPROVE]; // tu choi duyet
                }
            } else if (account.partnerUserType == PARTNER_TYPE_MANAGER) {  // tu choi ki
                [self showReasoonWithType:TYPE_UN_SIGNATURE];
            }
        } else {
            buttonIndex = alertView.tag;
            if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) { // tk anh giang
                if (buttonIndex == BUTTON_SIGN) { // ký biên bản
                    [self signatureReport];
                } else if (buttonIndex == BUTTON_REFUSE) { // từ chối kí biên bản
                    //                    [self refuseSignatureReport];
                    [self showReasoonWithType:TYPE_UN_SIGNATURE];
                }
            } else if (account.partnerUserType == PARTNER_TYPE_EMPLOY) { // TK nhân viên
                if (buttonIndex == BUTTON_SIGN) { // duyệt
                    [self reviewReport];
                } else {
                    //                    [self refuseApproveReport]; // từ chối duyệt
                    [self showReasoonWithType:TYPE_UN_APPROVE];
                }
            }  else if (account.partnerUserType == PARTNER_TYPE_ADMIN) { // TK addmin
                if (_documentModel.status.integerValue == STATUS_WAITING_REVIEW ) { // văn bản đợi duyệt
                    if (buttonIndex == BUTTON_SIGN) { // duyệt văn bản
                        [self reviewReport];
                    } else { // từ chối duyệt
                        [self showReasoonWithType:TYPE_UN_APPROVE];
                    }
                } else if (_documentModel.status.integerValue == STATUS_WAITING_SIGNATURE) { // văn bản đợi ký
                    if (buttonIndex == BUTTON_SIGN) { // ký văn bản
                        [self signatureReport];
                    } else if (buttonIndex == BUTTON_REFUSE) { // từ chối ký
                        [self showReasoonWithType:TYPE_UN_SIGNATURE];
                    }                 }
            } else { // lãnh đạo
                if (buttonIndex == BUTTON_SIGN) { // ký biên bản
                    [self signatureReport];
                } else if (buttonIndex == BUTTON_REFUSE) { // từ chối biên bản ký
                    //                    [self refuseSignatureReport];
                    [self showReasoonWithType:TYPE_UN_SIGNATURE];
                }
            }
        }
    }
}

- (void) historyButtonPressedDelegate:(id) data {
    HTInfoDocumentModel *infoDocument = (HTInfoDocumentModel*) data;
    if ([infoDocument.holderClass isEqualToString:NSStringFromClass([HTAddInvoiceViewController class])]) {
        HTAddInvoiceViewController *addInvoiceViewController = [self.storyboard instantiateViewControllerWithIdentifier:infoDocument.holderClass];
        addInvoiceViewController.doccumentModel = _documentModel;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addInvoiceViewController];
        customizeAppearance(navigationController);
        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    } else if ([infoDocument.holderClass isEqualToString:NSStringFromClass([HTHistorySignViewController class])]) {
        
        if (showHistory == NO) {
            if (_documentModel.listHistory.count > 0) {
                [self showHistoryReport];
            } else {
                showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
                [self loadListHistory];
            }
        } else {
            [self hidenHistoryReport];
        }
    } else if ([infoDocument.holderClass isEqualToString:NSStringFromClass([HTListInvoiceViewController class])]) {
        HTListInvoiceViewController *addInvoiceViewController = [self.storyboard instantiateViewControllerWithIdentifier:infoDocument.holderClass];
        addInvoiceViewController.doccumentModel = _documentModel;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addInvoiceViewController];
        customizeAppearance(navigationController);
        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    }
}

- (void) loadListHistory {
    NSData *params = [DictionaryBuilder dictionarySearchHistoryReport:self.documentModel.listHistory.count andLimit:LIMIT andReport:self.documentModel];
    [self executeWithWithParams:params withTag:REQUEST_FOR_GET_HISTORY_SIGN];
}

- (void) showHistoryReport {
    [self.listInfor addObjectsFromArray:self.documentModel.listHistory];
    [self.listInfor enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if ([obj isKindOfClass:[HTInfoDocumentModel class]]) {
            HTInfoDocumentModel *infoDocument = (HTInfoDocumentModel*) obj;
            if ([infoDocument.holderClass isEqualToString:NSStringFromClass([HTHistorySignViewController class])]) {
                infoDocument.selected = YES;
                infoDocument.thumbImage = @"img_icon_sub";
            }
        }
    }];
    
    [self.infoTableView reloadData];
    showHistory = YES;
}

- (void) hidenHistoryReport {
    [self.listInfor enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[HTHistoryModel class]]) {
            [self.listInfor removeObject:obj];
        } else if ([obj isKindOfClass:[HTInfoDocumentModel class]]) {
            HTInfoDocumentModel *infoDocument = (HTInfoDocumentModel*) obj;
            if ([infoDocument.holderClass isEqualToString:NSStringFromClass([HTHistorySignViewController class])]) {
                infoDocument.selected = NO;
                infoDocument.thumbImage = @"img_icon_add";
            }
        }
    }];
    [self.infoTableView reloadData];
    showHistory = NO;
}



- (void) dismissHistoryPopupDelegate:(id)sender {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
}

- (void) downLoadFileDelegate:(NSString*)jasperId {
    //    NSLog(@"downLoadFileDelegate jasperId %ld", jasperId);
    
    NSString *executableFileMD5Hash = [FileUtil getMd5FileWithPath:[self getFilePathToShowFile]];
    showMBProgressHUD(self.view, @"LOADDING", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    NSData *parmas = [DictionaryBuilder dictionaryForDownloadFileReport:_documentModel andMd5File:executableFileMD5Hash andJasperId:jasperId];
    [self executeWithWithParams:parmas withTag:REQUEST_FOR_DOWNLOAD_FILE];
}

// Chức năng dành cho nhân viên

- (void) reviewReport {
    showMBProgressHUD(self.view, @"LOADDING_REVIEW", YES, MBProgressHUDModeIndeterminate, HTRequestTimeOut);
    NSData *params = [DictionaryBuilder dictionarySignatureReport:_documentModel andPath:kPathReviewReport andReason:nil andNote:nil];
    [self executeWithWithParams:params withTag:REQUEST_FOR_APPROVE_REPORT];
}

// Chức năng dành cho lãnh đạo

- (void) signatureReport { // ký văn bản
    showMBProgressHUD(self.view , @"LOADDING_LOGGING_SIGNATURING", YES, MBProgressHUDModeDeterminate, HTRequestSignTimeOut);
    [HTAppDelegate sharedDelegate].timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                                            target:self
                                                                          selector:@selector(onTick)
                                                                          userInfo:nil
                                                                           repeats:YES];
    
    NSData *params = [DictionaryBuilder dictionarySignatureReport:_documentModel andPath:kPathSignatureReport andReason:nil andNote:nil];
    [self executeWithWithParams:params withTag:REQUEST_FOR_SIGNATURE_REPORT];
}

#pragma mark - tableview delegate and datasource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    self.titleDoccument.text = self.documentModel.contractName;
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 56;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listInfor.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HTBaseModel *baseModel = self.listInfor[indexPath.row];
    if ([baseModel isKindOfClass:[HTHistoryModel class]]) {
        return ROW_HEIGHT_DOCUMENT_HISTORY_117;
    }
    HTInfoDocumentModel *infoDocumentModel = (HTInfoDocumentModel*) baseModel;
    if ([infoDocumentModel.cellDisplay isEqualToString:@"HTDoccumentItemTableViewCell"]) {
        return  [HTDoccumentItemTableViewCell heightForCellWithObject:infoDocumentModel];
    }
    return ROW_HEIGHT_INFOR_52;
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HTBaseModel *baseModel = self.listInfor[indexPath.row];
    UITableViewCell *cell = nil;
    if ([baseModel isKindOfClass:[HTHistoryModel class]]) {
        cell = [self historyTableViewCell:tableView celforRowAtIndex:indexPath];
    } else {
        cell = [self infoDocumentTableViewCell:tableView cellforRowAtIndex:indexPath];
    }
    return cell;
}

- (UITableViewCell*) infoDocumentTableViewCell:(UITableView*) tableView
                             cellforRowAtIndex:(NSIndexPath*) indexPath {
    HTInfoDocumentModel *inforDocumentModel = self.listInfor[indexPath.row];
    static NSString *newsCellIdentifier = nil;
    if ([inforDocumentModel.cellDisplay isEqualToString:@"HTDoccumentItemTableViewCell"]) {
        newsCellIdentifier = @"HTDoccumentItemTableViewCell";
    } else if ([inforDocumentModel.cellDisplay isEqualToString:@"HTFileTableViewCell"]){
        newsCellIdentifier = @"HTFileTableViewCell";
    } else {
        newsCellIdentifier = @"HTDocumentHistoryTableViewCell";
    }
    BaseTableViewCell *cell = (BaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:newsCellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        [cell setUserInteractionEnabled:inforDocumentModel.clickable];
    }
    cell.controller = self;
    [cell setCellData:inforDocumentModel];
    
    return cell;
}

- (UITableViewCell*) historyTableViewCell:(UITableView*) tableView
                         celforRowAtIndex:(NSIndexPath*) indexPath {
    
    static NSString *cellIdentifier = @"HTHistoryTableViewCell";
    HTHistoryTableViewCell *cell = (HTHistoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.controller = self;
    
    HTHistoryModel *historyModel = self.listInfor[indexPath.row];
    [cell setCellData:historyModel];
    
    return cell;
}

#pragma mark - restpont server
- (void) requestSuccess:(NSInteger)errorCode
             andMessage:(NSString *)message
      andResponseObject:(id)responseObject
          andTagRequest:(NSInteger)tag {
    
    if (tag == REQUEST_FOR_SIGNATURE_REPORT) {
        [super stopTimer];
    }
    
    if (tag == REQUEST_FOR_DOWNLOAD_FILE) {
        if (responseObject != nil && ![responseObject isEqual:[NSNull class]] && ((NSString*) responseObject).length > 0) {
            NSString *strPath = [self getFilePathToShowFile];
            NSData *decodeData = [[NSData alloc] initWithBase64EncodedString:responseObject options:NSDataBase64DecodingIgnoreUnknownCharacters];
            
            if ([FileUtil fileExistsAtPath:strPath]) {
                [FileUtil deleteFileAtPath:strPath];
            }
            
            [decodeData writeToFile:strPath atomically:YES];
            
            UIActionSheet    *actionSheet = [[UIActionSheet alloc] initWithTitle:nil  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BUTTON_OPEN_FILE_WITH_APP", nil),NSLocalizedString(@"BUTTON_OPEN_FILE_PREVIEW", nil),nil];
            actionSheet.tag = TAG_ACTION_SHEET_SHOW_FILE_OFTION;
            [actionSheet showInView:self.view];
            
        } else {
            showAleartWithMessage(NSLocalizedString(@"MESSAGE_DOWNLOAD_FAIL", nil));
        }
    } else if (tag == REQUEST_FOR_APPROVE_REPORT || tag == REQUEST_FOR_SIGNATURE_REPORT) {
    
        HTAccount *account = [HTPreferences sharedPreferences].account;
        NSString *notify = nil;
        
        NSDate* date = convertToDate(_documentModel.icCycleDate, kPLFormatDate_MMDD_YY);
        NSString* dateStr = convertDateToString(date, kPLFormatDateDD_MM_YY);
        
        if (tag == REQUEST_FOR_APPROVE_REPORT) {
            notify = [NSString stringWithFormat:@"Đ/c %@ vừa DUYỆT biên bản: %@ - %@ - Kỳ đối soát: %@", [account fullName], _documentModel.contractNo, _documentModel.serviceName, dateStr];
            
            parsePush(account, [NSArray arrayWithObjects:[NSString stringWithFormat:@"partnerId_%ld", account.partnerId], VIP, nil] , notify);
        } else if (tag == REQUEST_FOR_SIGNATURE_REPORT) {
            notify = [NSString stringWithFormat:@"Đ/c %@ vừa KÝ biên bản: %@ - %@ - Kỳ đối soát: %@", [account fullName], _documentModel.contractNo, _documentModel.serviceName, dateStr];
            
            parsePush(account, [NSArray arrayWithObjects:[NSString stringWithFormat:@"partnerId_%ld", account.partnerId], nil] , notify);
        }
        
        if (_documentModel.status.integerValue == STATUS_WAITING_REVIEW) {
            _documentModel.status = convertIntegerToString(STATUS_WAITING_SIGNATURE);
        } else if (_documentModel.status.integerValue == STATUS_WAITING_SIGNATURE) {
            _documentModel.status = convertIntegerToString(STATUS_MANAGER_SIGN);
        } else if (_documentModel.status.integerValue == STATUS_MANAGER_SIGN) {
            _documentModel.status = convertIntegerToString(STATUS_VIETTEL);
        }
        [self refreshData];
        [self checkShowSignButton];
       
        [[NSNotificationCenter defaultCenter] postNotificationName:kPLSignatureReportNotification object:nil];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        //MARK: TUANTD7 neu dismiss o day thi khi dung pdf de ki khong back lai duoc can xem lai
//        [self.navigationController popViewControllerAnimated:YES];
        
         showAleartWithMessage(message);
    } else if (tag == REQUEST_FOR_UNSIGNATURE_REPORT || tag == REQUEST_FOR_UNREVIEW_REPORT) {
        //MARK: TUANTD7 Từ chối kí duyệt không xảy ra ở đây do có màn hình chọn Reason
    } else if (tag == REQUEST_FOR_GET_HISTORY_SIGN) {
        NSMutableArray *list = [HTHistoryModel parserListHistory:responseObject];
        [self.documentModel.listHistory addObjectsFromArray:list];
        
        if(self.documentModel.listHistory) {
            NSLog(@"self.doccumentModel.listHistory %ld", self.documentModel.listHistory.count);
        }
        
        if (list.count == LIMIT) {
            [self loadListHistory];
            return;
        }
        self.documentModel.listHistory = self.documentModel.listHistory;
        [self showHistoryReport];
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) requestFail:(NSInteger)errorCode
          andMessage:(NSString *)message
   andResponseObject:(id)responseObject
       andTagRequest:(NSInteger)tag {
    
    if (tag == REQUEST_FOR_SIGNATURE_REPORT || tag == REQUEST_FOR_DISCONNECT) {
        [super stopTimer];
    }
    
    if (tag == REQUEST_FOR_DOWNLOAD_FILE && errorCode == FILE_DUPLICATE) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil  delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BUTTON_OPEN_FILE_WITH_APP", nil),NSLocalizedString(@"BUTTON_OPEN_FILE_PREVIEW", nil),nil];
        actionSheet.tag = TAG_ACTION_SHEET_SHOW_FILE_OFTION;
        [actionSheet showInView:self.view];
        //        [[[LGActionSheet alloc] initWithTitle:nil view:nil buttonTitles:@[NSLocalizedString(@"BUTTON_OPEN_FILE_WITH_APP", nil),NSLocalizedString(@"BUTTON_OPEN_FILE_PREVIEW", nil)] cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", nil) destructiveButtonTitle:nil actionHandler:^(LGActionSheet *actionSheet, NSString *title, NSUInteger index) {
        //            NSString *strPath = [self getFilePathToShowFile];
        //            if (index == 0) {
        //                [self showOpenInViewWithPath:strPath];
        //            } else if (index == 1){
        //                [self showFileWithPath:strPath andShowSingButton:(_doccumentModel.status.integerValue != STATUS_VIETTELL) andDocument:_doccumentModel];
        //            }
        //
        //        } cancelHandler:^(LGActionSheet *actionSheet, BOOL onButton) {
        //
        //        } destructiveHandler:^(LGActionSheet *actionSheet) {
        //
        //        }] showAnimated:YES completionHandler:nil];
        //
        //        NSLog(@"FILE NOT EXIT PATH: %@",strPath);
        //        NSLog(@"FILE NOT EXIT: %d",[FileUtil fileExistsAtPath:strPath]);
    } else {
        showAleartWithMessage(message);
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (NSString*) getFilePathToShowFile { // create foder and get file path
    NSString *fileName = [NSString stringWithFormat:@"%@.pdf",[NSNumber numberWithInteger:_documentModel.icReportId]];
    NSString *path = [[FileUtil createDirectoryWithName:kFolderReport] stringByAppendingPathComponent:fileName];
    return path;
}

- (void)showOpenInViewWithPath:(NSString *)filePath
{
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    if (fileURL) {
        self.docInteractionController = nil;
        // Initialize Document Interaction Controller
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
        
        // Configure Document Interaction Controller
        self.docInteractionController.delegate = self;
        
        // Present Open In Menu
        [self.docInteractionController presentOptionsMenuFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    }
}

@end






