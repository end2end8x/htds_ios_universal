//
//  HTHistorySignViewController.h
//  HTDS
//
//  Created by TuanTD on 7/16/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseViewController.h"
#import "HTDocumentModel.h"

@protocol HTHistoryDelegate <NSObject>

- (void) dismissHistoryPopupDelegate:(id)sender;

@end

@interface HTHistorySignViewController : BaseViewController <UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic,strong) HTDocumentModel *doccumentModel;
@property (nonatomic,assign) id <HTHistoryDelegate> delegate;
@property (nonatomic,strong) UIRefreshControl *refreshHistoryControl;




- (IBAction)dismisButtonPressed:(id)sender;


@end
