//
//  ReasonsViewController.h
//  HTDS
//
//  Created by TuanTD on 8/3/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "InputViewController.h"
#import "BaseTextField.h"
#import "SZTextView.h"
#import "PLRoundedButton.h"
#import "HTDocumentModel.h"

enum {
    TYPE_UN_APPROVE = 1,
    TYPE_UN_SIGNATURE,
};

@interface HTReasonsViewController : InputViewController

@property (nonatomic,strong) HTDocumentModel *documentModel;
@property (weak, nonatomic) IBOutlet BaseTextField *reasonsTextfield;
@property (weak, nonatomic) IBOutlet SZTextView *descriptiontextfield;
@property (weak, nonatomic) IBOutlet UITableView *sugestionTableView;
@property (weak, nonatomic) IBOutlet PLRoundedButton *sendCommentButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic,assign) NSInteger type;

- (IBAction)sendCommentButtonPressed:(id)sender;


@end
