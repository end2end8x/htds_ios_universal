//
//  PLHomeViewController.m
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/15/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "HTHomeViewController.h"
#import "SVPullToRefresh.h"
#import "MBProgressHUD.h"
// controller
#import "HTLoginViewController.h"
#import "HTDocumentModel.h"
#import "HTServiceModel.h"
#import "HTContractModel.h"
#import "DictionaryBuilder.h"


#import "PLFunctions.h"
#import "DictionaryBuilder.h"
#import "Defines.h"
#import "PLConstants.h"
#import "HTAccount.h"
#import "HTPreferences.h"



@interface HTHomeViewController ()
@end

@implementation HTHomeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    HTAccount *account = [HTPreferences loadAccountfromUserDefault];
    if (account) {
        NSDate *current = [NSDate date];
        if ([current timeIntervalSince1970] - [account.date timeIntervalSince1970] < HTSessionTimeOut) {
            
            account.date = current;
            
            [HTPreferences saveAccountToPreferences:account];
            [[NSNotificationCenter defaultCenter] postNotificationName:kPLUserDidLoginNotification object:nil];
            
            // When users indicate they are Giants fans, we subscribe them to that channel.
            parseSubscribe([NSArray arrayWithObjects:[account userName], [NSString stringWithFormat:@"partnerId_%ld",[account partnerId]], nil]);
            
            return;
        } else {
            [HTPreferences removeAccountFromPreferences];
            parseUnSubscribe();
        }
    }
    presentLoginViewController([SlideNavigationController sharedInstance].topViewController);
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view setUserInteractionEnabled:YES];
    
}

- (void) getListReports {
    [super getListReports];
}

- (void) getListReportHistory {
    [super getListReportHistory];
}



@end









