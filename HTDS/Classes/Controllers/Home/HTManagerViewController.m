//
//  HTDepartMentViewController.m
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTManagerViewController.h"

#import "DictionaryBuilder.h"
#import "PLConstants.h"
#import "Defines.h"
#import "HTAccount.h"
#import "HTPreferences.h"
#import "PLFunctions.h"


@interface HTManagerViewController ()

@end

@implementation HTManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) getListReports {
    [super getListReports];
}

- (void) getListReportHistory {
    [super getListReportHistory];
}


@end
