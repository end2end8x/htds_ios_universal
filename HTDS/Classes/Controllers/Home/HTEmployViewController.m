//
//  HTEmployViewController.m
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTEmployViewController.h"
#import "DictionaryBuilder.h"
#import "Defines.h"
#import "PLConstants.h"
#import "PLFunctions.h"
#import "HTAccount.h"
#import "HTPreferences.h"

@interface HTEmployViewController ()

@end

@implementation HTEmployViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) getListReports {
    [super getListReports];
}

- (void) getListReportHistory {
    [super getListReportHistory];
}


@end
