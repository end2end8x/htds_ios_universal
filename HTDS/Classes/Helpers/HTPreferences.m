//
//  PLPreferences.m
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "HTPreferences.h"
#import "HTAppDelegate.h"
#import "Defines.h"
#import "PLConstants.h"


@implementation HTPreferences

+ (HTPreferences *)sharedPreferences {
    static HTPreferences *_sharedPreferences;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedPreferences = [[HTPreferences alloc] init];
    });
    return _sharedPreferences;
}

#pragma mark - save object account to user default

+ (void) saveAccountToPreferences:(HTAccount*) account {
    [HTPreferences sharedPreferences].loggedIn = YES;
    [HTPreferences sharedPreferences].account = account;
    
    [self saveAccounttoUserdefault:account];
}

+ (void) removeAccountFromPreferences {
    [HTPreferences sharedPreferences].loggedIn = NO;
    [HTPreferences sharedPreferences].account = nil;
    
    [[HTAppDelegate sharedDelegate].listContract removeObjectsInRange:NSMakeRange(1, [HTAppDelegate sharedDelegate].listContract.count - 1)];
    [[HTAppDelegate sharedDelegate].listService removeObjectsInRange:NSMakeRange(1, [HTAppDelegate sharedDelegate].listService.count - 1)];
    
    [self removeAccountFromeUserdefault];
}

+ (void) resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

+ (void) saveAccounttoUserdefault:(HTAccount*) account {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:account];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:KSaveAccount];
    [defaults setBool:YES forKey:KCheckAccount]; 
    [defaults synchronize];
}

+ (HTAccount*)loadAccountfromUserDefault {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:KSaveAccount];
    HTAccount *account = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return account;
}

- (BOOL)hasAccountInfo {
    return [[NSUserDefaults standardUserDefaults] boolForKey:KCheckAccount];
}

+ (void) removeAccountFromeUserdefault {
    [HTPreferences sharedPreferences].loggedIn = NO;
    [HTPreferences sharedPreferences].account = nil;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KSaveAccount];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:KCheckAccount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
