//
//  PLFunctions.h
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "HTReportManagedModel.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "HTAccount.h"

#ifndef PhapLuat_PLFunctions_h
#define PhapLuat_PLFunctions_h


extern NSString *const kPLFormatDateDD_MM_YY;

extern NSString *const kPLFormatDate_MM_YY;

extern NSString *const kPLFormatDate_MMDD_YY;



extern float _SystemVersion();
extern BOOL  _IsOS7OrNewer();
extern BOOL  _IsIphone5();

extern BOOL IS_IPAD();

extern UIImage* imageWithPath(NSString* imageName);



extern void textFieldPaddingWithFrame(UITextField* textfield,CGRect frame);

extern BOOL PL_IsNullOrEmptyString(NSString *string);

extern BOOL PL_IsValidPhoneNumber(NSString *phoneNumber);

extern void showAleartWithMessage(NSString *message);

extern void showLocalNotification(NSString *body, NSCalendarUnit repeatInterval, NSDate *fireDate);

extern MBProgressHUD* showMBProgressHUD(UIView *view, NSString *title, BOOL userInteractionEnabled, NSInteger mode, NSTimeInterval time);

extern void showAlertDefaultViewWithMessage(NSString *message);

extern NSString* intervalToString(NSTimeInterval interval);

extern NSString* decodeImageUrl(NSString *urlImageString);

extern void textFieldDidBeginEditing(UITextField* textField,UIView *controllerView);

extern void textFieldDidEndEditing(UITextField* textField,UIView *controllerView);

extern void scrollDownTexView (UITextView* textField,UIView *controllerView);

extern void scrollUpTexView (UITextView* textField,UIView *controllerView);

extern void customizeAppearance (UINavigationController* navigationController);

extern void presentLoginViewController (UIViewController *viewcontroller);

extern NSString *trimSpaceWhiteString (NSString* strTrim);

extern BOOL isEmptyString (NSString* content);

extern void showPopUp (UIView *Popupshowview,UIView *viewBlur,UIViewController *controller);

extern void hidePopUpView (UIView *popupView,UIView *viewBlur,UIViewController *controller);

extern NSString *convertListParams (NSMutableArray* params);

extern NSDictionary *convertParam (NSString *key,id value);

extern HTReportManagedModel *getReportBookMarkModelWithDocument(id document, NSString *infoReport);

extern NSString* convertIntegerToString (NSInteger number);

UIKIT_EXTERN NSString *convertDateToString (NSDate *date,NSString *fomateDate);
UIKIT_EXTERN NSDate *convertToDate (NSString *dateStr, NSString *formate);

extern void animationShakeTextField(UIView *textField, BOOL isNextRemoveAnimation);

extern NSString* convertVietnamCurrency(NSString* number);

extern void parseBadgeDecrease(NSInteger number);
extern void parseSubscribe(NSArray*);
extern void parseUnSubscribe();
extern void parsePush(HTAccount* account, NSArray* channels, NSString* message);

#endif
