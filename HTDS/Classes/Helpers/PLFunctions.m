//
//  PLFunctions.m
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//


#import "PLFunctions.h"
#import <CommonCrypto/CommonDigest.h>
#import "HTAppDelegate.h"
#import "HTLoginViewController.h"
#import "HTDocumentModel.h"
#import "HTAccount.h"
#import "SlideNavigationController.h"

#import "PLConstants.h"
#import "HTPreferences.h"
#import "BaseViewController.h"
#import <Parse/Parse.h>

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;


CGFloat const kFadeInAnimationDuration = 0.3;
CGFloat const kTransformPart1AnimationDuration = 0.3;
CGFloat const kTransformPart2AnimationDuration = 0.2;

NSString *const kPLFormatDateDD_MM_YY = @"dd-MM-yyyy";

NSString *const kPLFormatDate_MM_YY = @"MM-yyyy";

NSString *const kPLFormatDate_MMDD_YY = @"MM dd,yyyy";


UIKIT_EXTERN BOOL IS_IPAD() {
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
}

UIKIT_EXTERN float _SystemVersion() {
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

UIKIT_EXTERN BOOL _IsOS7OrNewer() {
    return _SystemVersion() >= 7.0;
}

UIKIT_EXTERN BOOL _IsIphone5() {
    float screenHeight = [[UIScreen mainScreen] bounds].size.height;
    return (screenHeight == 568.0f);
}

UIKIT_EXTERN UIImage* imageWithPath(NSString* imageName) {
    NSString *path = nil;
    path = [[NSBundle mainBundle] pathForResource:imageName ofType:@"png"];
    UIImage *img1 = [UIImage imageWithContentsOfFile:path];
    return img1;
}

UIKIT_EXTERN void textFieldPaddingWithFrame(UITextField* textfield,CGRect frame) {
    UIView *paddingView = [[UIView alloc] initWithFrame:frame];
    [paddingView setBackgroundColor:[UIColor clearColor]];
    textfield.leftView = paddingView;
    textfield.leftViewMode = UITextFieldViewModeAlways;
}

UIKIT_EXTERN BOOL PL_IsValidPhoneNumber(NSString *phoneNumber) {
    NSString *phoneRegex=@"([+][0-9]{2})?[0-9]{9,11}";
    NSPredicate *no=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [no evaluateWithObject:phoneNumber];
}

UIKIT_EXTERN BOOL PL_IsNullOrEmptyString(NSString *string) {
    if (string == nil) {
        return YES;
    }
    
    NSString *trim = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([trim length] == 0) {
        return YES;
    }
    
    if ([trim isEqualToString:@"(null)"]) {
        return YES;
    }
    
    return NO;
}

UIKIT_EXTERN void showAleartWithMessage(NSString*message) {
    
    UIAlertView* uialert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [uialert show];
}

UIKIT_EXTERN void showLocalNotification(NSString *body, NSCalendarUnit repeatInterval, NSDate *fireDate) {
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    [notification setRepeatInterval:repeatInterval];
    [notification setAlertBody:body];
    [notification setFireDate:fireDate];
    [notification setTimeZone:[NSTimeZone  defaultTimeZone]];
    [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObject:notification]];
}

UIKIT_EXTERN MBProgressHUD* showMBProgressHUD(UIView *view, NSString *title, BOOL userInteractionEnabled, NSInteger mode, NSTimeInterval time) {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view andTitle:NSLocalizedString(title, nil) animated:YES];
    
    hud.userInteractionEnabled = userInteractionEnabled;
    hud.mode = mode;
    [hud hide:YES afterDelay:time];
    
    return hud;
}

UIKIT_EXTERN NSString* intervalToString(NSTimeInterval interval) {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (interval >= 3600) {
        [dateFormatter setDateFormat:@"HH:mm:ss"];
    } else {
        [dateFormatter setDateFormat:@"mm:ss"];
    }
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    return [dateFormatter stringFromDate:date];
}

UIKIT_EXTERN NSString* decodeImageUrl(NSString *urlImageString) {
    return (__bridge NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                         (__bridge CFStringRef) urlImageString,
                                                                                         CFSTR(""),
                                                                                         kCFStringEncodingUTF8);
}

UIKIT_EXTERN NSString *trimSpaceWhiteString (NSString* strTrim) {
    NSString *trimmedString = [strTrim stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    return trimmedString;
}

UIKIT_EXTERN BOOL isEmptyString (NSString* content) {
    if (trimSpaceWhiteString(content).length > 0) {
        return NO;
    }
    return YES;
}

// animation
UIKIT_EXTERN void animationShakeTextField(UIView *textField,BOOL isNextRemoveAnimation) {
    if (isNextRemoveAnimation) {
        [textField becomeFirstResponder];
        return;
    }
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values = @[ @0, @10, @-10, @10, @0 ];
    animation.keyTimes = @[ @0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1 ];
    animation.duration = 0.4;
    
    animation.additive = YES;
    [textField.layer addAnimation:animation forKey:@"shake"];
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [textField becomeFirstResponder];
    });
}

#pragma mark - scroll up - scoll down uitextfiel

UIKIT_EXTERN void textFieldDidBeginEditing(UITextField* textField,UIView *controllerView) {
    CGRect textFieldRect = [controllerView.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [controllerView.window convertRect:controllerView.bounds fromView:controllerView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0) {
        heightFraction = 0.0;
    }  else if (heightFraction > 1.0) {
        heightFraction = 1.0;
    }
    
    /*UIInterfaceOrientation orientation =
     [[UIApplication sharedApplication] statusBarOrientation];
     if (orientation == UIInterfaceOrientationPortrait ||
     orientation == UIInterfaceOrientationPortraitUpsideDown)
     {*/
    
    [HTAppDelegate sharedDelegate].animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    /*}
     else
     {
     animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
     }*/
    
    CGRect viewFrame = controllerView.frame;
    viewFrame.origin.y -=  [HTAppDelegate sharedDelegate].animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [controllerView setFrame:viewFrame];
    
    [UIView commitAnimations];
}

UIKIT_EXTERN void textFieldDidEndEditing(UITextField* textField,UIView *controllerView) {
    CGRect viewFrame = controllerView.frame;
    viewFrame.origin.y +=  [HTAppDelegate sharedDelegate].animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [controllerView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - scroll TextView

#pragma mark - scroll down , scroll up uitextview

UIKIT_EXTERN void scrollDownTexView (UITextView* textField,UIView *controllerView) {
    CGRect textFieldRect = [controllerView.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [controllerView.window convertRect:controllerView.bounds fromView:controllerView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0) {
        heightFraction = 0.0;
    }  else if (heightFraction > 1.0) {
        heightFraction = 1.0;
    }
    
    /*UIInterfaceOrientation orientation =
     [[UIApplication sharedApplication] statusBarOrientation];
     if (orientation == UIInterfaceOrientationPortrait ||
     orientation == UIInterfaceOrientationPortraitUpsideDown)
     {*/
    
    [HTAppDelegate sharedDelegate].animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    /*}
     else
     {
     animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
     }*/
    
    CGRect viewFrame = controllerView.frame;
    viewFrame.origin.y -=  [HTAppDelegate sharedDelegate].animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [controllerView setFrame:viewFrame];
    
    [UIView commitAnimations];
}

UIKIT_EXTERN void scrollUpTexView (UITextView* textField,UIView *controllerView) {
    CGRect viewFrame = controllerView.frame;
    viewFrame.origin.y +=  [HTAppDelegate sharedDelegate].animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [controllerView setFrame:viewFrame];
    [UIView commitAnimations];
}


UIKIT_EXTERN void customizeAppearance (UINavigationController* navigationController) {
    //MARK: uncomment để custom navigation bar
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bg_navigation"];
    [navigationController.navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor clearColor];
    shadow.shadowOffset = CGSizeMake(0, 0);
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                                          shadow, NSShadowAttributeName,
                                                          [UIFont fontWithName:kFontArial size:SIZE_15], NSFontAttributeName, nil]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
}

UIKIT_EXTERN void presentLoginViewController (UIViewController *viewcontroller) {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HTLoginViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([HTLoginViewController class])];
    UINavigationController *loginNavigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    customizeAppearance(loginNavigationController);
    [[SlideNavigationController sharedInstance] presentViewController:loginNavigationController animated:YES completion:nil];
}

#pragma mark - popup

UIKIT_EXTERN void showPopUp (UIView *Popupshowview,UIView *viewBlur,UIViewController *controller) {
    
    //    CGRect frame = Popupshowview.frame;
    //    frame = [UIScreen mainScreen].bounds;
    //    Popupshowview.frame = frame;
    viewBlur.hidden = NO;
    viewBlur.alpha = 0.8;
    Popupshowview.hidden = NO;
    [controller.view addSubview:Popupshowview];
    Popupshowview.alpha = 0;
    Popupshowview.layer.shouldRasterize = NO;
    Popupshowview.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.4, 0.4);
    [UIView animateWithDuration:0.15 animations:^{
        Popupshowview.alpha = 1;
        
        Popupshowview.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    } completion:^(BOOL finished) {  // UIViewAnimationOptionCurveEaseOut
        
        //        [UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        //            Popupshowview.alpha = 1;
        //            Popupshowview.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        //        } completion:^(BOOL finished2) {
        //            Popupshowview.layer.shouldRasterize = NO;
        //            //            [Popupshowview layoutIfNeeded];
        //        }];
    }];
}

UIKIT_EXTERN void hidePopUpView (UIView *popupView,UIView *viewBlur,UIViewController *controller) {
    viewBlur.alpha = 1;
    [UIView animateWithDuration:kFadeInAnimationDuration animations:^{
        viewBlur.alpha = 0;
    }];
    viewBlur.hidden = YES;
    popupView.layer.shouldRasterize = YES;
    [UIView animateWithDuration:kTransformPart2AnimationDuration animations:^{
        popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished){
        [UIView animateWithDuration:kTransformPart1AnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            popupView.alpha = 0;
            popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.4, 0.4);
        } completion:^(BOOL finished2){
            [popupView removeFromSuperview];
        }];
    }];
}

UIKIT_EXTERN NSDictionary *convertParam (NSString *key,id value) {
    
    return @{
             kName:key,
             kValue:value
             };
}

UIKIT_EXTERN HTReportManagedModel *getReportBookMarkModelWithDocument(id document, NSString *infoReport) {
    HTDocumentModel *documentModel = (HTDocumentModel*) document;
    HTAccount *accountModel = [HTPreferences sharedPreferences].account;
    HTReportManagedModel *reportBookMarkModel = [HTReportManagedModel new];
    reportBookMarkModel.accountName = accountModel.userName;
    reportBookMarkModel.idReport    = documentModel.icReportId;
    reportBookMarkModel.status      = [documentModel.status integerValue];
    reportBookMarkModel.icCycleDate = documentModel.icCycleDate;
    reportBookMarkModel.createDate  = [NSDate date];
    reportBookMarkModel.infoReport  = infoReport;
    
    return reportBookMarkModel;
}

UIKIT_EXTERN NSString* convertIntegerToString (NSInteger number) {
    return [NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:number]];
}

UIKIT_EXTERN NSString *convertDateToString (NSDate *date, NSString *fomateDate) {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:fomateDate];
    NSString *strDate = [format stringFromDate:date];
    return strDate;
}

UIKIT_EXTERN NSDate *convertToDate (NSString *dateStr, NSString *formate) {
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:formate];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return date;
}

UIKIT_EXTERN NSString* convertVietnamCurrency(NSString* number) {
    if(!number || number.length == 0)
        return @"";
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger: [number integerValue]]];
    
    NSString* vnCurrency = [numberAsString substringFromIndex:1];
    if ([vnCurrency containsString:@"."]) {
        vnCurrency = [vnCurrency substringToIndex: vnCurrency.length - 3];
    }
    //    vnCurrency = [vnCurrency substringToIndex: vnCurrency.length - 3];
    return vnCurrency;
}

UIKIT_EXTERN void parseBadgeDecrease(NSInteger number) {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    NSInteger badge = currentInstallation.badge;
    if (badge != 0) {
        if(number <= 0) {
            currentInstallation.badge = 0;
        } else {
            currentInstallation.badge = badge > number ? badge - number : 0;
        }
        [currentInstallation saveEventually];
    }
}

UIKIT_EXTERN void parseSubscribe(NSArray* channels) {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation removeObjectForKey:@"channels"];
    
    for (NSString *channel in channels) {
        [currentInstallation addUniqueObject:channel forKey:@"channels"];
    }
    [currentInstallation saveEventually];
}

UIKIT_EXTERN void parseUnSubscribe() {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation removeObjectForKey:@"channels"];
    currentInstallation.badge = 0;
    [currentInstallation saveEventually];
}

UIKIT_EXTERN void parsePush(HTAccount* account, NSArray* channels, NSString* message) {
    PFQuery *query = [PFInstallation query];
    for (NSString *channel in channels) {
        [query whereKey:@"channels" equalTo:channel];
    }
    [query whereKey:@"channels" notEqualTo:[account userName]];

//    // Notification for Android users
//    [query whereKey:@"deviceType" equalTo:@"android"];
//    PFPush *androidPush = [[PFPush alloc] init];
//    [androidPush setMessage:@"Your suitcase has been filled with tiny robots!"];
//    [androidPush setQuery:query];
//    [androidPush sendPushInBackground];

    NSDictionary *data = @{
                           @"alert" : message,
                           @"badge" : @"Increment",
//                           @"sounds" : @"cheering.caf",
                           kUsername: account.userName
                           };
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:query];
//    [push setChannels:channels];
    [push setData:data];
    [push sendPushInBackground];
}


