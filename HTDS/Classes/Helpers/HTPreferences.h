//
//  PLPreferences.h
//  PhapLuat
//
//  Created by Tuanpk Huy on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTAccount.h"

@interface HTPreferences : NSObject

+ (HTPreferences *)sharedPreferences;

@property (nonatomic,readwrite,assign) BOOL loggedIn;
@property(nonatomic,strong) HTAccount *account;


+ (void) saveAccountToPreferences:(HTAccount*) account;

+ (void) removeAccountFromPreferences;

+ (HTAccount*)loadAccountfromUserDefault;

//+ (void) saveAccounttoUserdefault:(HTAccount*) account;

//+ (void) removeAccountFromeUserdefault;

//+ (void) resetDefaults;

//- (BOOL) hasAccountInfo;

@end
