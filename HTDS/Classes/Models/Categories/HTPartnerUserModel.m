//
//  HTEmployerModel.m
//  HTDS
//
//  Created by TuanTD on 7/30/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTPartnerUserModel.h"

@implementation HTPartnerUserModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       kPartnerUserId: kObjectId,
                                                       kFullName: @"titleDisplay",
                                                       }];
}

@end
