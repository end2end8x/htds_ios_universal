//
//  HTServiceModel.m
//  HTDS
//
//  Created by TuanTD on 7/21/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTServiceModel.h"

@implementation HTServiceModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       kServiceId: kObjectId,
                                                       kName: @"titleDisplay",
                                                       kCode: kCode
                                                       }];
}

@end
