//
//  HTStaffModel.m
//  HTDS
//
//  Created by Aloha Cool on 9/4/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTStaffModel.h"

@implementation HTStaffModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       kStaffId: kObjectId,
                                                       kName: @"titleDisplay",
                                                       kCode: kCode
                                                       }];
}

@end
