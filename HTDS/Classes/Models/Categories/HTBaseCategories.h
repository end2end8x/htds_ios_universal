//
//  HTBaseCategories.h
//  HTDS
//
//  Created by TuanTD on 7/21/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseModel.h"

@interface HTBaseCategories : HTBaseModel

@property (nonatomic,strong) NSNumber<Optional> *statusSign; // 0 đã ký 1 đã từ chối 2 chờ ký duyệt
@property (nonatomic,strong) NSString<Optional> *code;

+ (NSMutableArray*) getListCategoriesSignedAndReview;
@end
