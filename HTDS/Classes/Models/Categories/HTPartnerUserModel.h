//
//  HTEmployerModel.h
//  HTDS
//
//  Created by TuanTD on 7/30/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTBaseCategories.h"

@interface HTPartnerUserModel : HTBaseCategories

@property (nonatomic,assign) NSInteger partnerId;
@property (nonatomic,strong) NSString *userName;

@end
