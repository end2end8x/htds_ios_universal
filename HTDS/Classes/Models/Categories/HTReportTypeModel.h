//
//  HTReportTypeModel.h
//  HTDS
//
//  Created by TuanTD on 7/25/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseCategories.h"

@interface HTReportTypeModel : HTBaseCategories

@property (nonatomic,strong) NSString  *reportType;


@end
