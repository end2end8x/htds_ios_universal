//
//  HTStatusModel.m
//  HTDS
//
//  Created by TuanTD on 7/21/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTStatusModel.h"
#import "HTAccount.h"
#import "HTPreferences.h"
#import "PLConstants.h"

@implementation HTStatusModel


+ (void) getListStatus:(NSMutableArray*) list
              andBlock:(CompletionDataBlock) block andTypeStatus:(NSInteger) typeStatus {
    
    [list removeAllObjects];
    HTStatusModel *statusAll = [HTStatusModel new];
    statusAll.objectId         = [NSNumber numberWithInteger:-1];
    statusAll.titleDisplay     = NSLocalizedString(@"TITLE_ALL", nil);
    [list addObject:statusAll];
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    if (account.partnerUserType == PARTNER_TYPE_EMPLOY) { // nhan vien doi tac
        HTStatusModel *status2 = [HTStatusModel new];
        status2.titleDisplay   = NSLocalizedString(@"status2", nil);
        status2.objectId       = [NSNumber numberWithInteger:STATUS_WAITING_REVIEW];
        [list addObject:status2];
        
        HTStatusModel *status5 = [HTStatusModel new];
        status5.titleDisplay   = NSLocalizedString(@"status5", nil);
        status5.objectId       = [NSNumber numberWithInteger:STATUS_WAITING_SIGNATURE];
        [list addObject:status5];
        
    }  else if (account.partnerUserType == PARTNER_TYPE_MANAGER) { // lanh dao doi tac
        HTStatusModel *status5 = [HTStatusModel new];
        status5.titleDisplay   = NSLocalizedString(@"status5", nil);
        status5.objectId       = [NSNumber numberWithInteger:STATUS_WAITING_SIGNATURE];
        [list addObject:status5];
        
        HTStatusModel *status7 = [HTStatusModel new];
        status7.titleDisplay   = NSLocalizedString(@"status7", nil);
        status7.objectId       = [NSNumber numberWithInteger:STATUS_MANAGER_SIGN];
        [list addObject:status7];
        
//    } else if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) { // truonggiang
//        HTStatusModel *status7 = [HTStatusModel new];
//        status7.titleDisplay   = NSLocalizedString(@"status7", nil);
//        status7.objectId       = [NSNumber numberWithInteger:STATUS_MANAGER_SIGN];
//        [list addObject:status7];
//        
//        HTStatusModel *status8 = [HTStatusModel new];
//        status8.titleDisplay   = NSLocalizedString(@"status8", nil);
//        status8.objectId       = [NSNumber numberWithInteger:STATUS_VIETTEL];
//        [list addObject:status8];
//        
//        HTStatusModel *status9 = [HTStatusModel new];
//        status9.titleDisplay   = NSLocalizedString(@"status9", nil);
//        status9.objectId       = [NSNumber numberWithInteger:STATUS_ACCEPTED];
//        [list addObject:status9];

        
    } else if (account.partnerUserType == PARTNER_TYPE_ADMIN) { // admin
        if (typeStatus == TYPE_STATUS_EMPLOYER ) {
            HTStatusModel *status2 = [HTStatusModel new];
            status2.titleDisplay   = NSLocalizedString(@"status2", nil);
            status2.objectId       = [NSNumber numberWithInteger:STATUS_WAITING_REVIEW];
            [list addObject:status2];
            
            HTStatusModel *status5 = [HTStatusModel new];
            status5.titleDisplay   = NSLocalizedString(@"status5", nil);
            status5.objectId       = [NSNumber numberWithInteger:STATUS_WAITING_SIGNATURE];
            [list addObject:status5];
        } else if (typeStatus == TYPE_STATUS_MANAGER || typeStatus == TYPE_STATUS_ADMIN) {
            HTStatusModel *status5 = [HTStatusModel new];
            status5.titleDisplay   = NSLocalizedString(@"status5", nil);
            status5.objectId       = [NSNumber numberWithInteger:STATUS_WAITING_SIGNATURE];
            [list addObject:status5];
            
            HTStatusModel *status7 = [HTStatusModel new];
            status7.titleDisplay   = NSLocalizedString(@"status7", nil);
            status7.objectId       = [NSNumber numberWithInteger:STATUS_MANAGER_SIGN];
            [list addObject:status7];
        }
    }
    
    if (typeStatus == TYPE_STATUS_ALL) {
        [list removeAllObjects];
        HTStatusModel *statusAll = [HTStatusModel new];
        statusAll.objectId         = [NSNumber numberWithInteger:-1];
        statusAll.titleDisplay     = NSLocalizedString(@"TITLE_ALL", nil);
        [list addObject:statusAll];
        
        
        HTStatusModel *status0 = [HTStatusModel new];
        status0.titleDisplay   = NSLocalizedString(@"status0", nil);
        status0.objectId       = [NSNumber numberWithInteger:0];
        [list addObject:status0];
        
        HTStatusModel *status1 = [HTStatusModel new];
        status1.titleDisplay   = NSLocalizedString(@"status1", nil);
        status1.objectId       = [NSNumber numberWithInteger:1];
        [list addObject:status1];
        
        HTStatusModel *status2 = [HTStatusModel new];
        status2.titleDisplay   = NSLocalizedString(@"status2", nil);
        status2.objectId       = [NSNumber numberWithInteger:STATUS_WAITING_REVIEW];
        [list addObject:status2];
        
        HTStatusModel *status3 = [HTStatusModel new];
        status3.titleDisplay   = NSLocalizedString(@"status3", nil);
        status3.objectId       = [NSNumber numberWithInteger:3];
        [list addObject:status3];
        
        HTStatusModel *status4 = [HTStatusModel new];
        status4.titleDisplay   = NSLocalizedString(@"status4", nil);
        status4.objectId       = [NSNumber numberWithInteger:4];
        [list addObject:status4];
        
        HTStatusModel *status5 = [HTStatusModel new];
        status5.titleDisplay   = NSLocalizedString(@"status5", nil);
        status5.objectId       = [NSNumber numberWithInteger:5];
        [list addObject:status5];
        
        HTStatusModel *status6 = [HTStatusModel new];
        status6.titleDisplay   = NSLocalizedString(@"status6", nil);
        status6.objectId       = [NSNumber numberWithInteger:6];
        [list addObject:status6];
        
        HTStatusModel *status7 = [HTStatusModel new];
        status7.titleDisplay   = NSLocalizedString(@"status7", nil);
        status7.objectId       = [NSNumber numberWithInteger:7];
        [list addObject:status7];
        
        HTStatusModel *status8 = [HTStatusModel new];
        status8.titleDisplay   = NSLocalizedString(@"status8", nil);
        status8.objectId       = [NSNumber numberWithInteger:8];
        [list addObject:status8];
        
        HTStatusModel *status9 = [HTStatusModel new];
        status9.titleDisplay   = NSLocalizedString(@"status9", nil);
        status9.objectId       = [NSNumber numberWithInteger:9];
        [list addObject:status9];
    }
    
    if (typeStatus == TYPE_STATUS_VIETTEL_SIGN) {
        [list removeAllObjects];
        
        HTStatusModel *status8 = [HTStatusModel new];
        status8.titleDisplay   = NSLocalizedString(@"status8", nil);
        status8.objectId       = [NSNumber numberWithInteger:8];
        [list addObject:status8];
        
        HTStatusModel *status9 = [HTStatusModel new];
        status9.titleDisplay   = NSLocalizedString(@"status9", nil);
        status9.objectId       = [NSNumber numberWithInteger:9];
        [list addObject:status9];
    }
    
    
//
//    HTStatusModel *status10 = [HTStatusModel new];
//    status10.titleDisplay   = NSLocalizedString(@"status10", nil);
//    status10.objectId       = [NSNumber numberWithInteger:10];
//    [list addObject:status10];
//    
//    HTStatusModel *status12 = [HTStatusModel new];
//    status12.titleDisplay   = NSLocalizedString(@"status12", nil);
//    status12.objectId       = [NSNumber numberWithInteger:12];
//    [list addObject:status12];
//    
//    HTStatusModel *status22 = [HTStatusModel new];
//    status22.titleDisplay   = NSLocalizedString(@"status22", nil);
//    status22.objectId       = [NSNumber numberWithInteger:22];
//    [list addObject:status22];
    
    if (block) {
        block();
    }
}

@end
