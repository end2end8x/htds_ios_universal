//
//  HTCategoriesModel.m
//  HTDS
//
//  Created by TuanTD on 7/17/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTContractModel.h"

@implementation HTContractModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       kContractId: kObjectId,
                                                       //kName: @"titleDisplay",
                                                       kCode: @"titleDisplay",
                                                       kCode: kCode
                                                       }];
}

@end
