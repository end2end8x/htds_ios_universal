//
//  HTReasonModel.h
//  HTDS
//
//  Created by TuanTD on 8/3/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTBaseModel.h"

@interface HTReasonModel : HTBaseModel

@property (nonatomic,strong) NSString *reasonCode;

@end
