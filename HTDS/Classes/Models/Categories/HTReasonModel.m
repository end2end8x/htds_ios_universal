//
//  HTReasonModel.m
//  HTDS
//
//  Created by TuanTD on 8/3/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTReasonModel.h"

@implementation HTReasonModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       kReasonId: kObjectId,
                                                       kName: @"titleDisplay",
                                                       kCode: kReasonCode
                                                       }];
}

@end
