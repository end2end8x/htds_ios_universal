//
//  HTReportTypeModel.m
//  HTDS
//
//  Created by TuanTD on 7/25/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTReportTypeModel.h"

@implementation HTReportTypeModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                      kReportId: kObjectId,
                                                      kReportName: @"titleDisplay"
                                                      }];
}



@end
