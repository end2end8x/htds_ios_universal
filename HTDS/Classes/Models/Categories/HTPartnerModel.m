//
//  HTPartnerModel.m
//  HTDS
//
//  Created by TuanTD on 7/30/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "HTPartnerModel.h"

@implementation HTPartnerModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       kPartnerId: kObjectId,
                                                       kName: @"titleDisplay",
                                                       kCode: kCode
                                                       }];
}

@end
