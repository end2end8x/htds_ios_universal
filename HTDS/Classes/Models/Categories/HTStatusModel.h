//
//  HTStatusModel.h
//  HTDS
//
//  Created by TuanTD on 7/21/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseCategories.h"

enum {
    TYPE_STATUS_EMPLOYER,
    TYPE_STATUS_MANAGER,
    TYPE_STATUS_ADMIN,
    TYPE_STATUS_VIETTEL_SIGN,
    TYPE_STATUS_VIETTEL_VIP,
    TYPE_STATUS_ALL
};

@interface HTStatusModel : HTBaseCategories


+ (void) getListStatus:(NSMutableArray*) list
              andBlock:(CompletionDataBlock) block
         andTypeStatus:(NSInteger) typeStatus;

@end
