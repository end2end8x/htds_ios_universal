//
//  HTBaseCategories.m
//  HTDS
//
//  Created by TuanTD on 7/21/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseCategories.h"
#import "HTAccount.h"
#import "HTPreferences.h"
#import "PLConstants.h"


@implementation HTBaseCategories


+ (NSMutableArray*) getListCategoriesSignedAndReview {
    HTAccount *accout  =  [HTPreferences sharedPreferences].account;
    NSMutableArray *listCategories = [NSMutableArray array];
    HTBaseCategories *categoriesSigned = [[HTBaseCategories alloc] init];
    categoriesSigned.titleDisplay = (accout.partnerUserType == PARTNER_TYPE_MANAGER?NSLocalizedString(@"TITLE_SIGNED", nil):NSLocalizedString(@"TITLE_REVIEWED", nil));
    categoriesSigned.statusSign   = [NSNumber numberWithInteger:BUTTON_SIGN];
    [listCategories addObject:categoriesSigned];
    
    HTBaseCategories *categoriesRefuse = [[HTBaseCategories alloc] init];
    categoriesRefuse.titleDisplay = (accout.partnerUserType == PARTNER_TYPE_MANAGER?NSLocalizedString(@"TITLE_REFUSED", nil):NSLocalizedString(@"TITLE_UNREVIEWED", nil));
    categoriesRefuse.statusSign   = [NSNumber numberWithInteger:BUTTON_REFUSE];
    [listCategories addObject:categoriesRefuse];
    
    HTBaseCategories *categoriesForsigned = [[HTBaseCategories alloc] init];
    categoriesForsigned.titleDisplay = (accout.partnerUserType == PARTNER_TYPE_MANAGER?NSLocalizedString(@"TITLE_WAIT_UP_SIGNED", nil):NSLocalizedString(@"TITLE_WAIT_UP_REVIEWED", nil));
    categoriesForsigned.statusSign   = [NSNumber numberWithInteger:BUTTON_FORSIGNED];
    [listCategories addObject:categoriesForsigned];
    return listCategories;
}

@end
