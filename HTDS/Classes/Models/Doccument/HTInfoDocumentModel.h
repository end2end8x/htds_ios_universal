//
//  HTInfoDocumentModel.h
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseModel.h"
#import "BaseTableViewCell.h"
#import "HTDocumentModel.h"



@interface HTInfoDocumentModel : HTBaseModel

@property (nonatomic,assign) NSInteger numberLine;
@property (nonatomic,strong) NSString *cellDisplay;
@property (nonatomic,strong) NSString *holderClass;
@property (nonatomic,assign) BOOL     clickable;
@property (nonatomic,strong) HTDocumentModel *documentModel;
@property (nonatomic,assign) BOOL selected;
@property (nonatomic,strong) NSString *thumbImage;

@property (nonatomic,strong) NSString  *jasperId;
@property (nonatomic,strong) NSString  *jasperCode;
@property (nonatomic,strong) NSString  *jasperName;


//@property (nonatomic,strong) NSString *actionCode;
//@property (nonatomic,strong) NSString *actionReasonCode;
//@property (nonatomic,strong) NSString *columnName;
//@property (nonatomic,strong) NSString *logDate;
//@property (nonatomic,strong) NSString *userUpdated;


+ (NSMutableArray*) getListInfoDocument:(HTDocumentModel*) document;

@end
