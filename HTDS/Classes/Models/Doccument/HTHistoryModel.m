//
//  HTHistoryModel.m
//  HTDS
//
//  Created by TuanTD on 7/27/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTHistoryModel.h"


@implementation HTHistoryModel


- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    if (self = [super init]) {
        self.actionCode  = [attributes stringForKey:kActionCode];
        self.userUpdated       = [attributes stringForKey:kUserUpdated];
        self.logDate = [attributes stringForKey:kLogDate];
        self.tellNumber = [attributes stringForKey:kTelNumber];
        self.note       = [attributes stringForKey:kNote];
    }
    return self;
}

+ (NSMutableArray*) parserListHistory:(NSArray*) data {
    NSMutableArray *listData = [NSMutableArray array];
    if (![data isKindOfClass:[NSNull class]]&& data.count > 0) {
        for (NSDictionary *dic in data) {
            if (![dic isKindOfClass:[NSNull class]]) {
                HTHistoryModel *history = [[HTHistoryModel alloc] initWithAttributes:dic];
                [listData addObject:history];
            }
            
        }
    }
    return listData;
}


@end
