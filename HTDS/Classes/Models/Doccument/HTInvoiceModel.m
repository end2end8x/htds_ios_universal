//
//  HTBillModel.m
//  HTDS
//
//  Created by TuanTD on 7/23/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTInvoiceModel.h"

@implementation HTInvoiceModel


- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    if (self = [super init]) {
        self.address = [attributes stringForKey:kAddress];
        self.amountNotTax = [attributes integerForKey:kAmountNotTax];
        self.amountTax    = [attributes integerForKey:kAmountTax];
        
        self.amountTotal = [attributes integerForKey:kAmountTotal];
        self.attachFileName = [attributes stringForKey:kAttachFileName];
        self.content    = [attributes stringForKey:kContent];
        
        self.icReportId = [attributes integerForKey:kICReportId];
        self.invoiceId = [attributes integerForKey:kIdentifier];
        self.invoiceDate    = [attributes stringForKey:kInvoiceDate];
        self.invoiceSerial = [attributes stringForKey:kInvoiceSerial];
        self.invoiceSign = [attributes stringForKey:kInvoiceSign];
        self.status = [attributes integerForKey:kStatus];
        self.taxCode = [attributes stringForKey:kTaxCode];
        self.vat     = [attributes numberForKey:kVat];
        self.unitIssue = [attributes stringForKey:kUnitIssue];
    }
    return self;
}


+ (NSMutableArray*) parserListObject:(NSArray*) responseObject {
    NSMutableArray *listData = [NSMutableArray array];
    if (![responseObject isKindOfClass:[NSNull class]]&& responseObject.count > 0) {
        for (NSDictionary *dic in responseObject) {
            if (![dic isKindOfClass:[NSNull class]]) {
                HTInvoiceModel *billModel = [[HTInvoiceModel alloc] initWithAttributes:dic];
                [listData addObject:billModel];
            }
            
        }
    }
    return listData;
}


@end
