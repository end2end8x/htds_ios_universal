//
//  HTDocumentModel.m
//  HTDS
//
//  Created by Anhpt67 on 7/10/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTDocumentModel.h"
#import "HTBaseModel.h"
#import "HTReportBookMarkManaged.h"
#import "NSString+SBJSON.h"
#import "HTReportRecentManaged.h"
#import "HTAccount.h"

#import "PLFunctions.h"
#import "PLConstants.h"

@implementation HTDocumentModel


- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    if (self = [super init]) {
        self.icReportId  = [attributes integerForKey:kICReportId];
        self.reportDateTime = [attributes stringForKey:kReportDateTime];
        self.icCycleDate    = [attributes stringForKey:kICCycleDate];
        self.templateId     = [attributes integerForKey:kTemplateId];
        self.status         = [attributes stringForKey:kStatus];
        self.deptId         = [attributes integerForKey:kDeptId];
        self.createDateTime = [attributes stringForKey:kCreateDateTime];
        
        // contract
        self.contractId       = [attributes integerForKey:kContractId];
        self.contractCode     = [attributes stringForKey:kContractCode];
        
        self.contractName       = [attributes stringForKey:kContractName];
        self.contractNo = [attributes stringForKey:kContractNo];
        // service
        self.serviceName = [attributes stringForKey:kServiceName];
        
        // parner
        self.partnerName = [attributes stringForKey:kPartnerName];
        
        // amount
        self.amountTotal = [attributes stringForKey:kAmountTotal];
        self.amountRevenue = [attributes stringForKey:kAmountRevenue];
        self.amountBonus = [attributes stringForKey:kAmountBonus];
        self.amountFee = [attributes stringForKey:kAmountFee];
        self.amountPayment = [attributes stringForKey:kAmountPayment];
        self.isBookmark    = [attributes boolForKey:kIsBookmark];
        
        // many file jasper
        self.numberFileJasper = [attributes integerForKey:kNumberFileJasper];
        self.listJasperId = [attributes stringForKey:kListJasperId];
        self.listJasperCode = [attributes stringForKey:kListJasperCode];
        self.listJasperName = [attributes stringForKey:kListJasperName];
        
        self.listHistory = [NSMutableArray new];
    }
    return self;
}

+ (NSMutableArray*) parserListObject:(NSArray*) responseObject {
    NSMutableArray *listData = [NSMutableArray array];
    if (![responseObject isKindOfClass:[NSNull class]]&& responseObject.count > 0) {
        for (NSDictionary *dic in responseObject) {
            if (![dic isKindOfClass:[NSNull class]]) {
                HTDocumentModel *document = [[HTDocumentModel alloc] initWithAttributes:dic];
                //MARK: TUANTD dang fix du lieu loi font unicode
//                if ([[document contractName] containsString:@"Hop dong dich vu"]) {
//                    document.contractName = [[document contractName] stringByReplacingOccurrencesOfString:@"Hop dong dich vu" withString:@"Hợp đồng dịch vụ"];
//                }
//                if ([[document contractName] containsString:@"bang"]) {
//                    document.contractName = [[document contractName] stringByReplacingOccurrencesOfString:@"bang" withString:@"bằng"];
//                }
//                if ([[document serviceName] containsString:@"Dich vu"]) {
//                    document.serviceName = [[document serviceName] stringByReplacingOccurrencesOfString:@"Dich vu" withString:@"Dịch vụ"];
//                }
//                if ([[document listJasperName] containsString:@"Bi�n ban doanh thu"]) {
//                    document.listJasperName = [[document listJasperName] stringByReplacingOccurrencesOfString:@"Bi�n ban doanh thu" withString:@"Biên bản doanh thu"];
//                }
//                if ([[document listJasperName] containsString:@"Bi�n ban x�c nh?n d? li?u"]) {
//                    document.listJasperName = [[document listJasperName] stringByReplacingOccurrencesOfString:@"Bi�n ban x�c nh?n d? li?u" withString:@"Biên bản xác nhận dữ liệu"];
//                }
                //MARK DELETE ###############################
                document.isBookmark =  [HTReportBookMarkManaged checkReportBookMark:document];
                [listData addObject:document];
            }
            
        }
    }
    return listData;
}

+ (NSMutableArray*) getListReportBookMark:(NSString*) strDate {
    NSMutableArray *reportsData = [NSMutableArray array];
    NSArray *listReport = [HTReportBookMarkManaged getListReportObjet];
    if (listReport && listReport.count > 0) {
        for (HTReportBookMarkManaged *reportModel in listReport) {
            NSDictionary *responseObject = [reportModel.infoReport JSONValue];
            HTDocumentModel *document = [[HTDocumentModel alloc] initWithAttributes:responseObject];
            [reportsData addObject:document];
        }
    }
    return reportsData;
}

#pragma mark - action bookmark
- (void) bookMarkOrUnBookMark {
    self.isBookmark = !self.isBookmark;
    if (!self.isBookmark) {
        [HTReportBookMarkManaged removeReportObject:getReportBookMarkModelWithDocument(self,nil)];
    } else {
        NSString* strDocument = [self toJSONString];
        [HTReportBookMarkManaged addReportObject:getReportBookMarkModelWithDocument(self,strDocument)];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kPLCheckBookMarksNotification object:self];
}

- (void) saveReportToRecent {
    NSString* strDocument = [self toJSONString];
    [HTReportRecentManaged addReportObject:getReportBookMarkModelWithDocument(self,strDocument)];
}

+ (NSMutableArray*) getListReportRecent {
    NSMutableArray *reportsData = [NSMutableArray array];
    NSArray *listReport = [HTReportRecentManaged getListReportObjet];
    if (listReport && listReport.count > 0) {
        for (HTReportRecentManaged *reportModel in listReport) {
            NSDictionary *responseObject = [reportModel.infoReport JSONValue];
            if (responseObject && ![responseObject isKindOfClass:[NSNull class]]) {
                HTDocumentModel *document = [[HTDocumentModel alloc] initWithAttributes:responseObject];
                document.isBookmark =  [HTReportBookMarkManaged checkReportBookMark:document];
                document.status = [NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:STATUS_VIETTEL]];
                [reportsData addObject:document];
            } 
        }
    }
    return reportsData;
}

@end
