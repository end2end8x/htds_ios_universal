//
//  HTInfoDocumentModel.m
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTInfoDocumentModel.h"
#import "HTDocumentModel.h"
#import "HTDoccumentItemTableViewCell.h"
#import "HTFileTableViewCell.h"
#import "HTDocumentHistoryTableViewCell.h"
#import "HTHistorySignViewController.h"
#import "HTAddInvoiceViewController.h"
#import "PLConstants.h"
#import "HTListInvoiceViewController.h"
#import "PLFunctions.h"

@implementation HTInfoDocumentModel 


+ (NSMutableArray*) getListInfoDocument:(HTDocumentModel*) document {
    NSMutableArray *listInfor = [NSMutableArray array];
    // kỳ đối soát
    HTInfoDocumentModel *dateChargInfo = [HTInfoDocumentModel new];
    dateChargInfo.titleDisplay = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"TITLE_DATE_CHARG", nil)];
    dateChargInfo.descriptionDisplay = document.icCycleDate;
    dateChargInfo.numberLine = 1;
    [listInfor addObject:dateChargInfo];
    
    // Dịch vụ
    HTInfoDocumentModel *serviceInfo = [HTInfoDocumentModel new];
    serviceInfo.titleDisplay = NSLocalizedString(@"TITLE_SERVICE", nil);
    serviceInfo.descriptionDisplay = document.serviceName;
    serviceInfo.numberLine = 1;
    [listInfor addObject:serviceInfo];
    
    // Hợp đồng
    HTInfoDocumentModel *contractInfo = [HTInfoDocumentModel new];
    contractInfo.titleDisplay = NSLocalizedString(@"TITLE_CONTRACT", nil);
    contractInfo.descriptionDisplay = document.contractNo;
    contractInfo.numberLine = 1;
    [listInfor addObject:contractInfo];
    
    // đối tác
    HTInfoDocumentModel *partnerInfo = [HTInfoDocumentModel new];
    partnerInfo.titleDisplay = NSLocalizedString(@"TITLE_PARNER", nil);
    partnerInfo.descriptionDisplay = document.partnerName;
    partnerInfo.numberLine = 1;
    [listInfor addObject:partnerInfo];
    
    // Trạng thái
    NSString *status = [NSString stringWithFormat:@"status%@",document.status];
    HTInfoDocumentModel *statusInfo = [HTInfoDocumentModel new];
    statusInfo.titleDisplay = NSLocalizedString(@"TITLE_STATUS", nil);
    statusInfo.descriptionDisplay = NSLocalizedString(status, nil);
    statusInfo.numberLine = 1;
    [listInfor addObject:statusInfo];
    
    // Doanh thu khách hàng
    HTInfoDocumentModel *customerRevenueInfo = [HTInfoDocumentModel new];
    customerRevenueInfo.titleDisplay = NSLocalizedString(@"TITLE_CUSTOMER_REVENUE", nil);
    customerRevenueInfo.descriptionDisplay = convertVietnamCurrency(document.amountTotal);
    customerRevenueInfo.numberLine = 2;
    [listInfor addObject:customerRevenueInfo];
    
    // Doanh thu Viettel
    HTInfoDocumentModel *viettelRevenueInfo = [HTInfoDocumentModel new];
    viettelRevenueInfo.titleDisplay = NSLocalizedString(@"TITLE_VIETTEL_REVENUE", nil);
    viettelRevenueInfo.descriptionDisplay = convertVietnamCurrency(document.amountRevenue);
    viettelRevenueInfo.numberLine = 1;
    [listInfor addObject:viettelRevenueInfo];

    // Doanh thu đối tác
    NSInteger parnerRevenue = [document.amountTotal integerValue] - [document.amountRevenue integerValue];
    HTInfoDocumentModel *parnerRevenueInfo = [HTInfoDocumentModel new];
    parnerRevenueInfo.titleDisplay = NSLocalizedString(@"TITLE_PARNER_REVENUE", nil);
    parnerRevenueInfo.descriptionDisplay = convertVietnamCurrency([NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:parnerRevenue]]);
    parnerRevenueInfo.numberLine = 1;
    [listInfor addObject:parnerRevenueInfo];
    
    // Chi phí
    HTInfoDocumentModel *costsInfo = [HTInfoDocumentModel new];
    costsInfo.titleDisplay = NSLocalizedString(@"TITLE_COSTS", nil);
    costsInfo.descriptionDisplay = convertVietnamCurrency(document.amountFee);
    costsInfo.numberLine = 1;
    [listInfor addObject:costsInfo];
    
    // Tổng só tiền thanh toán đối tác
    HTInfoDocumentModel *sumAmountPaymentInfo = [HTInfoDocumentModel new];
    sumAmountPaymentInfo.titleDisplay = NSLocalizedString(@"TITLE_PAYMENT_PARTNER", nil);
    sumAmountPaymentInfo.descriptionDisplay = convertVietnamCurrency(document.amountPayment);
    sumAmountPaymentInfo.numberLine = 2;
    [listInfor addObject:sumAmountPaymentInfo];
    
    for (HTInfoDocumentModel *infoDocument in listInfor) {
        infoDocument.cellDisplay = @"HTDoccumentItemTableViewCell";
        infoDocument.clickable = NO;
    }
    
    // file đính kèm
    NSArray *jasperIdArray = [document.listJasperId componentsSeparatedByString: @";"];
    NSArray *jasperCodeArray = [document.listJasperCode componentsSeparatedByString: @";"];
    NSArray *jasperNameArray = [document.listJasperName componentsSeparatedByString: @";"];
    for (int i = 0; i < document.numberFileJasper; i++) {
        HTInfoDocumentModel *fileInfo = [HTInfoDocumentModel new];
        fileInfo.titleDisplay = jasperNameArray[i];
        fileInfo.jasperCode = jasperCodeArray[i];
        fileInfo.jasperId = jasperIdArray[i];
        fileInfo.numberLine = 1;
        fileInfo.cellDisplay = @"HTFileTableViewCell";
        fileInfo.clickable = YES;
        fileInfo.thumbImage = @"file_extension_pdf";
        fileInfo.documentModel = document;
        
        [listInfor addObject:fileInfo];
    }

    
    // Danh sách hoá đơn
    if (document.status.integerValue == STATUS_VIETTEL || document.status.integerValue == STATUS_ACCEPTED) {
        HTInfoDocumentModel *billInfo = [HTInfoDocumentModel new];
        billInfo.titleDisplay = NSLocalizedString(@"TITLE_ADD_INVOICE", nil);
        billInfo.descriptionDisplay = document.amountPayment;
        billInfo.numberLine = 1;
        billInfo.holderClass = NSStringFromClass([HTAddInvoiceViewController class]);
        billInfo.cellDisplay = @"HTDocumentHistoryTableViewCell";
        billInfo.clickable = YES;
        [listInfor addObject:billInfo];
        
        HTInfoDocumentModel *listBillInfo = [HTInfoDocumentModel new];
        listBillInfo.titleDisplay = NSLocalizedString(@"TITLE_LIST_BILL", nil);
        listBillInfo.descriptionDisplay = document.amountPayment;
        listBillInfo.numberLine = 1;
        listBillInfo.holderClass = NSStringFromClass([HTListInvoiceViewController class]);
        listBillInfo.cellDisplay = @"HTDocumentHistoryTableViewCell";
        listBillInfo.clickable = YES;
        [listInfor addObject:listBillInfo];
    }
    
    // Lịch sử ký duyệt
    HTInfoDocumentModel *historySignInfo = [HTInfoDocumentModel new];
    historySignInfo.titleDisplay = NSLocalizedString(@"TITLE_HISTORY_SIGN", nil);
    historySignInfo.numberLine = 1;
    historySignInfo.cellDisplay = @"HTDocumentHistoryTableViewCell";
    historySignInfo.holderClass = NSStringFromClass([HTHistorySignViewController class]);
    historySignInfo.clickable = YES;
    historySignInfo.thumbImage = @"img_icon_add";
    [listInfor addObject:historySignInfo];

    return listInfor;
    
}

@end
