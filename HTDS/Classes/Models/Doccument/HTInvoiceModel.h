//
//  HTBillModel.h
//  HTDS
//
//  Created by TuanTD on 7/23/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseModel.h"
#import "HTDocumentModel.h"

@interface HTInvoiceModel : HTBaseModel

@property (nonatomic,strong) NSString *address;
@property (nonatomic,assign) NSInteger amountNotTax;
@property (nonatomic,assign) NSInteger amountTax;
@property (nonatomic,assign) NSInteger amountTotal;
@property (nonatomic,strong) NSString *attachFileName;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,assign) NSInteger icReportId;
@property (nonatomic,assign) NSInteger invoiceId;
@property (nonatomic,strong) NSString *invoiceDate;
@property (nonatomic,strong) NSString *invoiceSerial;
@property (nonatomic,strong) NSString *invoiceSign;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,strong) NSString *taxCode;
@property (nonatomic,strong) NSNumber *vat;
@property (nonatomic,strong) NSString *unitIssue;






+ (NSMutableArray*) parserListObject:(NSArray*) responseObject;

@end
