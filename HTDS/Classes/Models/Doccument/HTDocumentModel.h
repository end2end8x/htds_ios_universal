//
//  HTDocumentModel.h
//  HTDS
//
//  Created by Anhpt67 on 7/10/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseModel.h"

@interface HTDocumentModel : HTBaseModel


@property (nonatomic,assign) NSInteger icReportId;
@property (nonatomic,assign) NSInteger templateId;
@property (nonatomic,assign) NSInteger deptId;
@property (nonatomic,assign) NSInteger contractId;
@property (nonatomic,strong) NSString  *contractCode;
@property (nonatomic,strong) NSString  *contractName;
@property (nonatomic,strong) NSString  *contractNo;
@property (nonatomic,assign) NSInteger serviceId;
@property (nonatomic,strong) NSString  *serviceCode;
@property (nonatomic,strong) NSString  *serviceName;
@property (nonatomic,assign) NSInteger partnerId;
@property (nonatomic,strong) NSString  *partnerCode;
@property (nonatomic,strong) NSString  *partnerName;
@property (nonatomic,strong) NSString  *reportDateTime;
@property (nonatomic,assign) NSInteger staffId;
@property (nonatomic,strong) NSString  *staffCode;
@property (nonatomic,strong) NSString  *staffName;
@property (nonatomic,strong) NSString  *status;
@property (nonatomic,strong) NSString  *icCycleDate;
@property (nonatomic,strong) NSString  *amountTotal;
@property (nonatomic,strong) NSString  *amountRevenue;
@property (nonatomic,strong) NSString  *amountBonus;
@property (nonatomic,strong) NSString  *amountFee;
@property (nonatomic,strong) NSString  *amountPayment;
@property (nonatomic,strong) NSString  *icReportType;
@property (nonatomic,assign) NSInteger signedTimes;
@property (nonatomic,strong) NSString  *fromDate;
@property (nonatomic,strong) NSString  *toDate;
@property (nonatomic,assign) NSInteger printedTimes;
@property (nonatomic,strong) NSString  *createDateTime;
@property (nonatomic,strong) NSString  *amountTotalNotVAT;
@property (nonatomic,strong) NSString  *amountRevenueNotVAT;
@property (nonatomic,strong) NSString  *amountBonusNotVAT;
@property (nonatomic,strong) NSString  *amountFeeNotVAT;
@property (nonatomic,strong) NSString  *amountPaymentNotVAT;
@property (nonatomic,assign) NSInteger signedByPartner;
@property (nonatomic,strong) NSString  *creationType;
@property (nonatomic,strong) NSString  *hardInvoice;
@property (nonatomic,strong) NSString  *inStorage;

// thêm
@property (nonatomic,assign) BOOL isBookmark;
@property (nonatomic,strong) NSString *infoReport;
@property (nonatomic,strong) NSMutableArray *listHistory;

// them
@property (nonatomic,assign) NSInteger numberFileJasper;
@property (nonatomic,strong) NSString  *listJasperId;
@property (nonatomic,strong) NSString  *listJasperCode;
@property (nonatomic,strong) NSString  *listJasperName;

+ (NSMutableArray*) parserListObject:(NSArray*) responseObject;
+ (NSMutableArray*) getListReportBookMark:strDate;

- (void) bookMarkOrUnBookMark;

- (void) saveReportToRecent;
+ (NSMutableArray*) getListReportRecent;

@end
