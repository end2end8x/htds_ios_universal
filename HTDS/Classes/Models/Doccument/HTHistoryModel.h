//
//  HTHistoryModel.h
//  HTDS
//
//  Created by TuanTD on 7/27/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseModel.h"


@interface HTHistoryModel : HTBaseModel




@property (nonatomic,strong) NSString *actionCode;
@property (nonatomic,strong) NSString *actionReasonCode;
//@property (nonatomic,strong) NSString *columnName;
@property (nonatomic,strong) NSString *logDate;
@property (nonatomic,strong) NSString *userUpdated;
@property (nonatomic,strong) NSString *tellNumber;
@property (nonatomic,strong) NSString *note;


+ (NSMutableArray*) parserListHistory:(NSArray*) data;

@end
