//
//  HTAccount.m
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTAccount.h"

@implementation HTAccount

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    if (self = [super init]) {
        self.session       = [attributes stringForKey:kSession];
        self.address = [attributes stringForKey:kAddress];
        self.email = [attributes stringForKey:kEmail];
        self.partnerId = [attributes integerForKey:kPartnerId];
        self.partnerUserType = [attributes integerForKey:kPartnerUserType];
        self.fullName   = [attributes stringForKey:kFullName];
        self.tellNumber = [attributes stringForKey:kTelNumber];
        self.userName   = [attributes stringForKey:kUser];
        self.provider   = [attributes stringForKey:kProvider];
        self.taxCode    = [attributes stringForKey:kTaxCode];
        
        self.date = [NSDate date];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *) coder {
    //Encode properties, other class variables, etc
    [super encodeWithCoder:coder];
    
    [coder encodeObject:self.session forKey:kSession];
    [coder encodeObject:self.address forKey:kAddress];
    [coder encodeObject:self.email forKey:kEmail];
    [coder encodeInteger:self.partnerId forKey:kPartnerId];
    [coder encodeInteger:self.partnerUserType forKey:kPartnerUserType];
    [coder encodeObject:self.fullName   forKey:kFullName];
    [coder encodeObject:self.tellNumber forKey:kTelNumber];
    [coder encodeObject:self.userName   forKey:kUser];
    [coder encodeObject:self.provider   forKey:kProvider];
    [coder encodeObject:self.taxCode    forKey:kTaxCode];
    
    [coder encodeObject:self.date    forKey:kDate];
}

- (id)initWithCoder:(NSCoder *) coder {
    self = [super init];
    if (self) {
        self.session       = [coder decodeObjectForKey:kSession];
        self.address = [coder decodeObjectForKey:kAddress];
        self.email = [coder decodeObjectForKey:kEmail];
        self.partnerId = [coder decodeIntegerForKey:kPartnerId];
        self.partnerUserType = [coder decodeIntegerForKey:kPartnerUserType];
        self.fullName   = [coder decodeObjectForKey:kFullName];
        self.tellNumber = [coder decodeObjectForKey:kTelNumber];
        self.userName   = [coder decodeObjectForKey:kUser];
        self.provider   = [coder decodeObjectForKey:kProvider];
        self.taxCode    = [coder decodeObjectForKey:kTaxCode];
        
        self.date    = [coder decodeObjectForKey:kDate];
    }
    return self;
}

@end
