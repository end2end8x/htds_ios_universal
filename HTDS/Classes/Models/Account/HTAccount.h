//
//  HTAccount.h
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseModel.h"

@interface HTAccount : HTBaseModel

@property (nonatomic,strong) NSString *userName;
@property (nonatomic,strong) NSString *session;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,assign) NSInteger partnerId;
@property (nonatomic,assign) NSInteger partnerUserType;
@property (nonatomic,strong) NSString *fullName;
@property (nonatomic,strong) NSString *tellNumber;
@property (nonatomic,strong) NSString *roleCode;
@property (nonatomic,strong) NSString *provider;
@property (nonatomic,strong) NSString *taxCode;
@property (nonatomic,strong) NSDate *date;

- (instancetype)initWithAttributes:(NSDictionary *) attributes;
- (void) encodeWithCoder:(NSCoder *) coder;
- (id) initWithCoder:(NSCoder *) coder ;

@end
