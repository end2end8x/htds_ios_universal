//
//  PLMenuItem.m
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/9/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "HTMenuItem.h"
#import "HTHomeViewController.h"
#import "HTViettelSignViewController.h"
#import "HTReportViewController.h"
#import "HTReportCDAViewController.h"
#import "HTEmployViewController.h"
#import "HTManagerViewController.h"

#import "PLConstants.h"
#import "HTAccount.h"
#import "HTPreferences.h"


@implementation HTMenuItem

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    if (self = [super initWithAttributes:attributes]) {
        self.clickable                  = [attributes boolForKey:KMenuClickable];
        self.level                      = [attributes integerForKey:KMenuLevel];
        self.holderControllerClassName  = [attributes stringForKey:KMenuClassName];
        
    }
    return self;
}

+ (void) addItemHometoMenulist:(NSMutableArray*) meunuitems {
    [HTMenuItem loadMenuHome:^(NSInteger errorCode, NSString *message, id data) {
        [meunuitems addObjectsFromArray:(NSArray*)data];
    }];
}

+ (void)getCategories:(CompletionMenuBlock) block {
    NSMutableArray *listmenuItems = [NSMutableArray new];
    NSMutableArray *listCategories = [NSMutableArray new];
     [HTMenuItem addItemHometoMenulist:listmenuItems];
    if (block) {
        block (0,@"success",listmenuItems,listCategories);
    }
}

#pragma mark - loadMenu Login

+ (void) loadMenuHome:(CompletionMenuInfoBlock) block {
    NSMutableArray *menuItemHome = [NSMutableArray array];
    // TRANG CHỦ
    [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_HOME", nil),kThumbnail : @"img_ico_home",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTHomeViewController class])}]];
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    if (account) {
        if (account.partnerUserType == PARTNER_TYPE_EMPLOY) {
            [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_EMPLOYER", nil),kThumbnail : @"img_icon_staff",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTEmployViewController class])}]];
        } else if (account.partnerUserType == PARTNER_TYPE_ADMIN) {
            
            [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_MANAGER", nil),kThumbnail : @"img_ico_menuItem_account",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTManagerViewController class])}]];
            
            [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_EMPLOYER", nil),kThumbnail : @"img_icon_staff",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTEmployViewController class])}]];
        } else {
            [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_MANAGER", nil),kThumbnail : @"img_ico_menuItem_account",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTManagerViewController class])}]];
        }
    }
    
    if (account.partnerUserType != PARTNER_TYPE_VIETTEL_VIP) {
        [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_VIEW_DOCCUMENT_RECENT", nil),kThumbnail : @"icon_pen_sign",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTViettelSignViewController class])}]];
        
        [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_REPORT_SEVICE", nil),kThumbnail : @"img-icon_report",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTReportViewController class])}]];
        
        [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_REPORT_CDR_SEVICE", nil),kThumbnail : @"img-icon_report",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:NSStringFromClass([HTReportCDAViewController class])}]];
    }
    
    [menuItemHome addObject:[[HTMenuItem alloc] initWithAttributes:@{kTitle: NSLocalizedString(@"MENU_LOGOUT", nil),kThumbnail : @"ico_logout",KMenuClickable :@"YES",KMenuLevel : @"1",KMenuClassName:@""}]];
    
    
    if (block) {
        block (0,@"success",menuItemHome);
    }
}

















@end
