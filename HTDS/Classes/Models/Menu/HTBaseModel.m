//
//  PLBaseModel.m
//  PhapLuat
//
//  Created by tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "HTBaseModel.h"

@implementation HTBaseModel

//- (id) init {
//    if (self == [super init]) {
//        self.titleDisplay = @"Đang cập nhật";
//        self.type         = @"";
//        self.objectId     = 0;
//    }
//    return self;
//}

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    if (self = [super init]) {
        self.titleDisplay  = [attributes stringForKey:kTitle];
        self.type       = [attributes stringForKey:kObjectType];
        self.descriptionDisplay = [attributes stringForKey:kDescription];
        self.thumbUrlString = [attributes stringForKey:kThumbnail];
        
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"A {%@} object with title {%@}", NSStringFromClass([self class]), _titleDisplay];
}

//+ (NSMutableArray*) parserListSearchSuggestion:(NSArray*) json {
//    NSMutableArray *listSuggesstion = [NSMutableArray array];
//    if (![json isKindOfClass:[NSNull class]]) {
//        for (NSDictionary *dict in json) {
//            HTBaseModel *sugessModel = [[HTBaseModel alloc] initWithAttributes:dict];
//            [listSuggesstion addObject:sugessModel];
//        }
//    }
//    
//    return listSuggesstion;
//}

@end
