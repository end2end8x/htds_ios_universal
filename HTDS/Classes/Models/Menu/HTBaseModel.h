//
//  PLBaseModel.h
//  PhapLuat
//
//  Created by tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "NSDictionary+PL.h" 
#import "Defines.h"

typedef void(^CompletionDataBlock)(); 
@interface HTBaseModel : JSONModel



@property (nonatomic, strong) NSString<Optional>  *titleDisplay;
@property (nonatomic, strong) NSString<Optional>  *descriptionDisplay;
@property (nonatomic, strong) NSString<Optional>  *thumbUrlString;
@property (nonatomic,strong) NSString<Optional>   *type;
@property (nonatomic,strong) NSNumber <Optional>  *objectId;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

//+ (NSMutableArray*) parserListSearchSuggestion:(NSArray*) json;

@end
