//
//  PLMenuItem.h
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/9/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTBaseModel.h"

enum {
    TypeHeader,
    TypeMenuItem,
    TypeMenuFooter
};

typedef void (^CompletionMenuBlock)(NSInteger errorCode,NSString *message,id data,id categories);
typedef void (^CompletionMenuInfoBlock)(NSInteger errorCode,NSString *message,id data);

@interface HTMenuItem : HTBaseModel

@property (nonatomic,strong) NSString *iconName;
@property (nonatomic,assign) BOOL clickable;
@property (nonatomic,assign) NSInteger level; // 0 menu ko co icon , 1  có icon
@property (nonatomic,strong) NSString *holderControllerClassName;




- (instancetype)initWithAttributes:(NSDictionary *)attributes;

+ (void)getCategories:(CompletionMenuBlock) block;

+ (void) loadMenuHome:(CompletionMenuInfoBlock) block;

@end
