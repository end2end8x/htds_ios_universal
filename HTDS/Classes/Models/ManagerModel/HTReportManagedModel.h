//
//  HTReportBookMarkModel.h
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTBaseModel.h"



@interface HTReportManagedModel : HTBaseModel

@property (nonatomic,strong) NSString *accountName;
@property (nonatomic,assign) NSInteger idReport;
@property (nonatomic,strong) NSDate    *createDate; 
@property (nonatomic, strong) NSString * infoReport;
@property (nonatomic,assign)  NSInteger status;
@property (nonatomic, retain) NSString * icCycleDate;




@end
