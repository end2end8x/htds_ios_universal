//
//  HTReportBookMarkManaged.h
//  HTDS
//
//  Created by TuanTD on 7/22/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "HTReportManagedModel.h"
#import "HTDocumentModel.h"

@interface HTReportBookMarkManaged : NSManagedObject

@property (nonatomic, retain) NSString * accountName;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSNumber * idReport;
@property (nonatomic, retain) NSString * infoReport;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * icCycleDate;


+ (void) addReportObject:(HTReportManagedModel *) reportBookMarkModel;

+ (void) removeReportObject:(HTReportManagedModel *) reportBookMarkModel;

+ (NSArray*) getListReportObjet;

+ (NSMutableArray*) getListReportObjet:(NSInteger) statusId
                          andCycleDate:(NSString*) cycleDate;

+ (BOOL) checkReportBookMark:(HTDocumentModel*) document;

@end
