//
//  HTReportRecentManaged.h
//  HTDS
//
//  Created by TuanTD on 7/22/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "HTReportManagedModel.h"

@interface HTReportRecentManaged : NSManagedObject

@property (nonatomic, retain) NSString * accountName;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSString * infoReport;
@property (nonatomic, retain) NSNumber * idReport;


+ (void) addReportObject:(HTReportManagedModel *) reportModel;

+ (void) removeLastReportObject;

+ (NSArray*) getListReportObjet;

+ (BOOL) checkReportObject:(HTReportManagedModel*) reportModel;

+ (void) updateReportObject:(HTReportManagedModel*) reportModel;


@end
