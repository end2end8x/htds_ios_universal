//
//  HTReportBookMarkManaged.m
//  HTDS
//
//  Created by TuanTD on 7/22/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTReportBookMarkManaged.h"
#import "HTAccount.h"

#import "HTAppDelegate.h"
#import "PLFunctions.h"
#import "HTPreferences.h"



@implementation HTReportBookMarkManaged

@dynamic accountName;
@dynamic createDate;
@dynamic idReport;
@dynamic infoReport;
@dynamic status;
@dynamic icCycleDate;

+ (void) addReportObject:(HTReportManagedModel *) reportBookMarkModel {
    HTReportBookMarkManaged *reportManaged = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([HTReportBookMarkManaged class]) inManagedObjectContext:[HTAppDelegate sharedDelegate].managedObjectContext];
    
    [reportManaged setAccountName:reportBookMarkModel.accountName];
    [reportManaged setIdReport:[NSNumber numberWithInteger:reportBookMarkModel.idReport]];
    [reportManaged setCreateDate:reportBookMarkModel.createDate];
    [reportManaged setInfoReport:reportBookMarkModel.infoReport];
    [reportManaged setStatus:[NSNumber numberWithInteger:reportBookMarkModel.status]];
    [reportManaged setIcCycleDate:reportBookMarkModel.icCycleDate];
    
    NSError *error = nil;
    if (![[HTAppDelegate sharedDelegate].managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        showAleartWithMessage([error localizedDescription]);
    }
}

+ (void) removeReportObject:(HTReportManagedModel *) reportBookMarkModel {
    NSManagedObjectContext *context = [HTAppDelegate sharedDelegate].managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:NSStringFromClass([HTReportBookMarkManaged class]) inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(idReport = %@) AND (accountName = %@)",[NSNumber numberWithInteger:reportBookMarkModel.idReport],reportBookMarkModel.accountName];
    [fetch setPredicate:predicate];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    NSManagedObject *managerObject = [fetchedProducts lastObject];
    [context deleteObject:managerObject];
    [context save:&error];
}

+ (NSArray*) getListReportObjet {
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSMutableArray *listReport = [NSMutableArray array];
    NSEntityDescription *reportEnrtityDesc = [NSEntityDescription entityForName:NSStringFromClass([HTReportBookMarkManaged class]) inManagedObjectContext:[HTAppDelegate sharedDelegate].managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(accountName = %@)",account.userName];
    [request setPredicate:predicate];
    [request setEntity:reportEnrtityDesc];
    NSError *error;
    listReport = (NSMutableArray*)[[HTAppDelegate sharedDelegate].managedObjectContext executeFetchRequest:request error:&error];
    
    NSArray *sortedArray = [listReport sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSDate *first = [(HTReportBookMarkManaged*)a createDate];
        NSDate *second = [(HTReportBookMarkManaged*)b createDate];
        return [second compare:first];
    }];
    
    return sortedArray;
}

+ (NSMutableArray*) getListReportObjet:(NSInteger) statusId
                          andCycleDate:(NSString*) cycleDate {
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSMutableArray *listReport = [NSMutableArray array];
    NSEntityDescription *reportEnrtityDesc = [NSEntityDescription entityForName:NSStringFromClass([HTReportBookMarkManaged class]) inManagedObjectContext:[HTAppDelegate sharedDelegate].managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(accountName = %@) AND (statusId = %@) AND (icCycleDate = %@)",account.userName,[NSNumber numberWithInteger:statusId],cycleDate];
    [request setPredicate:predicate];
    [request setEntity:reportEnrtityDesc];
    NSError *error;
    listReport = (NSMutableArray*)[[HTAppDelegate sharedDelegate].managedObjectContext executeFetchRequest:request error:&error];
    return listReport;
}

+ (BOOL) checkReportBookMark:(HTDocumentModel*) document {
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSMutableArray *listReport = [NSMutableArray array];
    NSEntityDescription *reportEnrtityDesc = [NSEntityDescription entityForName:NSStringFromClass([HTReportBookMarkManaged class]) inManagedObjectContext:[HTAppDelegate sharedDelegate].managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(accountName = %@) AND (idReport = %@)",account.userName,[NSNumber numberWithInteger:document.icReportId]];
    [request setPredicate:predicate];
    [request setEntity:reportEnrtityDesc];
    NSError *error;
    listReport = (NSMutableArray*)[[HTAppDelegate sharedDelegate].managedObjectContext executeFetchRequest:request error:&error];
    return (listReport.count>0);
}

@end
