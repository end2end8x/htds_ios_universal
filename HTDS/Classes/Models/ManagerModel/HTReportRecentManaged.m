//
//  HTReportRecentManaged.m
//  HTDS
//
//  Created by TuanTD on 7/22/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTReportRecentManaged.h"
#import "HTAppDelegate.h"
#import "PLFunctions.h"
#import "HTPreferences.h"


@implementation HTReportRecentManaged

@dynamic accountName;
@dynamic createDate;
@dynamic infoReport;
@dynamic idReport;



+ (void) addReportObject:(HTReportManagedModel*) reportModel {
    if ([HTReportRecentManaged checkReportObject:reportModel]) {
        [HTReportRecentManaged updateReportObject:reportModel];
        return;
    }
    [self removeLastReportObject];
    HTReportRecentManaged *reportManaged = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([HTReportRecentManaged class]) inManagedObjectContext:[HTAppDelegate sharedDelegate].managedObjectContext];
    
    [reportManaged setAccountName:reportModel.accountName];
    [reportManaged setIdReport:[NSNumber numberWithInteger:reportModel.idReport]];
    [reportManaged setCreateDate:reportModel.createDate];
    [reportManaged setInfoReport:reportModel.infoReport];
    
    NSError *error = nil;
    if (![[HTAppDelegate sharedDelegate].managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        showAleartWithMessage([error localizedDescription]);
    }
}

+ (void) removeLastReportObject {
    NSArray *listReport = [HTReportRecentManaged getListReportObjet];
    if (listReport && listReport.count > 100) {
        HTReportRecentManaged *reportModel = [listReport lastObject];
        NSManagedObjectContext *context = [HTAppDelegate sharedDelegate].managedObjectContext;
        NSEntityDescription *entityDescription=[NSEntityDescription entityForName:NSStringFromClass([HTReportRecentManaged class]) inManagedObjectContext:context];
        NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
        [fetch setEntity:entityDescription];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(idReport = %@) AND (accountName = %@)",reportModel.idReport,reportModel.accountName];
        [fetch setPredicate:predicate];
        NSError *fetchError;
        NSError *error;
        NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
        NSManagedObject *managerObject = [fetchedProducts lastObject];
        [context deleteObject:managerObject];
        [context save:&error];
    } 
}

+ (NSArray*) getListReportObjet {
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSEntityDescription *reportEnrtityDesc = [NSEntityDescription entityForName:NSStringFromClass([HTReportRecentManaged class]) inManagedObjectContext:[HTAppDelegate sharedDelegate].managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(accountName = %@)",account.userName];
    [request setPredicate:predicate];
    [request setEntity:reportEnrtityDesc];
    NSError *error;
    NSArray *listReport = [[HTAppDelegate sharedDelegate].managedObjectContext executeFetchRequest:request error:&error];
    NSArray *sortedArray = nil;
    if (listReport.count > 0) {
        sortedArray = [listReport sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSDate *first = [(HTReportRecentManaged*)a createDate];
            NSDate *second = [(HTReportRecentManaged*)b createDate];
            return [second compare:first];
        }];
    }
    return sortedArray;
}

+ (BOOL) checkReportObject:(HTReportManagedModel*) reportModel {
    NSEntityDescription *reportEnrtityDesc = [NSEntityDescription entityForName:NSStringFromClass([HTReportRecentManaged class]) inManagedObjectContext:[HTAppDelegate sharedDelegate].managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(idReport = %@) AND (accountName = %@)",[NSNumber numberWithInteger:reportModel.idReport],reportModel.accountName];
    [request setPredicate:predicate];
    [request setEntity:reportEnrtityDesc];
    NSError *error;
    NSArray *listReport = [[HTAppDelegate sharedDelegate].managedObjectContext executeFetchRequest:request error:&error];
    if (listReport.count > 0) {
        return YES;
    }
    return NO;
}

+ (void) updateReportObject:(HTReportManagedModel*) reportModel {
    NSManagedObjectContext *context = [[HTAppDelegate sharedDelegate] managedObjectContext];
    NSEntityDescription *productEntity = [NSEntityDescription entityForName:NSStringFromClass([HTReportRecentManaged class]) inManagedObjectContext:context];
    NSFetchRequest *fetRequest = [[NSFetchRequest alloc] init];
    [fetRequest setEntity:productEntity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(idReport = %@) AND (accountName = %@)",[NSNumber numberWithInteger:reportModel.idReport],reportModel.accountName];
    [fetRequest setPredicate:predicate];
    
    
    NSError *fetchError;
    NSError *error;
    NSArray *objectsArray = [context executeFetchRequest:fetRequest error:&fetchError];
    
    HTReportRecentManaged *reportManaged = [objectsArray lastObject];
    [reportManaged setIdReport:[NSNumber numberWithInteger:reportModel.idReport]];
    [reportManaged setCreateDate:reportModel.createDate];
    [reportManaged setInfoReport:reportModel.infoReport];
    // And save values to core data
    [context save:&error];
}

@end
