//
//  PLBaseTableViewCell.h
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell
@property (nonatomic,weak) id controller;

- (void) setCellData:(id) data;
+ (CGFloat)heightForCellWithObject:(id) baseModel;

@end
