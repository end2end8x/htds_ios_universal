//
//  PLAlleartView.h
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/12/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PLAlleartView;

typedef void(^PLAleartViewBlock)(PLAlleartView *alleart,NSInteger buttonIndex);

@interface PLAlleartView : UIView

@property (nonatomic,strong) NSString *message;


  @property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIButton *leftButton;
@property (nonatomic, weak) IBOutlet UIButton *rightButton;
@property (nonatomic, copy) PLAleartViewBlock dismissBlock;

+ (PLAlleartView *)alertViewWithMessage:(NSString *)message leftButtonTitle:(NSString *)leftTitle rightButtonTitle:(NSString *)rightTitle dismissBlock:(PLAleartViewBlock)block;

+ (PLAlleartView *)alertViewWithMessage:(NSString *)message buttonTitle:(NSString *)title dismissBlock:(PLAleartViewBlock)block;



@end
