//
//  BaseTextField.h
//  MobilityDashBoard
//
//  Created by Tuan on 5/18/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import <UIKit/UIKit.h>


IB_DESIGNABLE

@interface BaseTextField : UITextField

@property (nonatomic,assign) id controller;

+ (BaseTextField*) getTextFieldView;

- (void) setRightViewTextFieldWithImageView:(NSString*) image andFrameRightView:(CGRect) frame;

- (void) setLeftTextFieldWithImageView:(NSString*) image andFrameRightView:(CGRect) frame;

@end
