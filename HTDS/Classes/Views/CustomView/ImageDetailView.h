//
//  ImageDetailView.h
//  HTDS
//
//  Created by TuanTD on 7/29/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageDetailDelegate <NSObject>

- (void) dissmissImageDetail:(id) sender;

@end

@interface ImageDetailView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic,assign) id<ImageDetailDelegate> delegate;


- (IBAction)dissmissButtonPressed:(id)sender;
+ (ImageDetailView*) getViewPopUP;
@end
