//
//  IZSegmentView.h
//  Imuzik3G
//
//  Created by MAC on 7/22/14.
//  Copyright (c) 2014 Tuanpk. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PLSegmentedViewDelegate <NSObject>

- (void)segmentedViewDidSelectItemAtIndex:(NSInteger)index;


@end


@interface PLSegmentView : UIView {
    UIView *_selectedIndicatorView;
}



@property (weak, nonatomic) IBOutlet id <PLSegmentedViewDelegate> delegate;

@property (nonatomic) NSMutableArray *buttons;
@property (nonatomic) NSMutableArray *labels;
@property (nonatomic) NSMutableArray *images;
@property (nonatomic) NSMutableArray *titles;


- (void)setButtonTitles:(NSArray *)titles;
- (void)setButtonHomeTitles:(NSArray *)titles;

- (void)setSelectButtonAtIndex:(NSInteger)index userPressed:(BOOL) isPress;

@end

