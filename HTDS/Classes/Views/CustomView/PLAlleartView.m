//
//  PLAlleartView.m
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/12/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "PLAlleartView.h"

@implementation PLAlleartView

+ (PLAlleartView*) alertViewWithMessage:(NSString *)message leftButtonTitle:(NSString *)leftTitle rightButtonTitle:(NSString *)rightTitle dismissBlock:(PLAleartViewBlock)block {
    PLAlleartView *alertView = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PLAlleartView" owner:self options:nil];
    alertView = [nibs objectAtIndex:0];
    alertView.messageLabel.text = message;
    [alertView.leftButton setTitle:leftTitle forState:UIControlStateNormal];
    [alertView.rightButton setTitle:rightTitle forState:UIControlStateNormal];
    alertView.dismissBlock = block;
    alertView.alpha = 0.98;
    return alertView;
}


+ (PLAlleartView *)alertViewWithMessage:(NSString *)message buttonTitle:(NSString *)title dismissBlock:(PLAleartViewBlock)block {
    PLAlleartView *alertView = nil;
    
    
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PLAlleartView2" owner:self options:nil];
    alertView = [nibs objectAtIndex:0];
    
    
    alertView.messageLabel.text = message;
    [alertView.leftButton setTitle:title forState:UIControlStateNormal];
    alertView.dismissBlock = block;
    alertView.alpha = 0.98;
    return alertView;
}

- (IBAction)buttonPressed:(UIButton *) sender {
    if (self.dismissBlock) {
        self.dismissBlock(self, sender.tag);
    }
}

@end
