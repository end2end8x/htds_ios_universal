///Users/anhpt67
//  BDTextFieldLoggin.m
//  BongDa
//
//  Created by Phung Khac Tuan on 2/27/15.
//  Copyright (c) 2015 VEGA Corp. All rights reserved.
//

#import "HTTextFieldLoggin.h"
#import "PLFunctions.h"

@implementation HTTextFieldLoggin

- (void) awakeFromNib {
    [super awakeFromNib];
    textFieldPaddingWithFrame(self,CGRectMake(0, 0, 10, 10));
}

@end
