//
//  VFRoundedButton.m
//  VegaFun
//
//  Created by Tuanpk on 5/20/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "PLRoundedButton.h"

@implementation PLRoundedButton

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.layer setCornerRadius:5.0f];
}

@end
