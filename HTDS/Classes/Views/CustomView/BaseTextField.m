//
//  BaseTextField.m
//  MobilityDashBoard
//
//  Created by Tuan on 5/18/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTextField.h"

@implementation BaseTextField

- (void) awakeFromNib { 
    [self setBackground:[[UIImage imageNamed:@"img_bg_username_textfield"] resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2)]];
    
}

- (void) setRightViewTextFieldWithImageView:(NSString*) image andFrameRightView:(CGRect) frame {
    UIImageView *rightImage = [[UIImageView alloc] initWithFrame:frame];
    [rightImage setContentMode:UIViewContentModeCenter];
    [rightImage setImage:[UIImage imageNamed:image]];
    self.leftView = rightImage;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void) setLeftTextFieldWithImageView:(NSString*) image andFrameRightView:(CGRect) frame {
    UIImageView *leftImage = [[UIImageView alloc] initWithFrame:frame];
    [leftImage setContentMode:UIViewContentModeCenter];
    [leftImage setImage:[UIImage imageNamed:image]];
    self.rightView = leftImage;
    self.rightViewMode = UITextFieldViewModeAlways;
}

+ (BaseTextField*) getTextFieldView {
    BaseTextField *basetextField = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    basetextField = [nibs objectAtIndex:0];
    return basetextField;
}

@end
