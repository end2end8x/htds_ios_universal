//
//  BDAvatarImageView.m
//  BongDa
//
//  Created by Phung Khac Tuan on 2/10/15.
//  Copyright (c) 2015 VEGA Corp. All rights reserved.
//

#import "HTAvatarImageView.h"

@implementation HTAvatarImageView

- (void) awakeFromNib {
    [super awakeFromNib];
    [self.layer setCornerRadius:self.frame.size.width/2];
    self.layer.borderWidth = 2.0f;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.masksToBounds = NO;
    self.clipsToBounds = YES;
}

@end
