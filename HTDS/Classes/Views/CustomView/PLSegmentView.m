//
//  IZSegmentView.m
//  Imuzik3G
//
//  Created by MAC on 7/22/14.
//  Copyright (c) 2014 Tuanpk. All rights reserved.
//

#import "PLSegmentView.h"
#import "PLConstants.h"

const NSInteger kDividerSize            = 1;
const NSInteger kDividerTopMargin       = 10;
const NSInteger kSelectedIndicatorSize  = 2;

// lable
const NSInteger kHeightSizeLable  = 21;

// image
const NSInteger ORIGIN_Y_Image  = 3;
const NSInteger kHeightSizeImage  = 24;
const NSInteger kWithSizeImage    = 24;

const NSInteger ORIGIN_X_INDICATOR  = 0;




@implementation PLSegmentView


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelectButtonAtIndex:(NSInteger)index userPressed:(BOOL) isPress {
   
    for (UIButton *button in _buttons) {
        if (button.tag == index) {
            [[self.labels objectAtIndex:index] setTextColor:bgColorApp];
            button.enabled = NO;
        } else {
            button.enabled = YES;
            self.tag = index;
            [[self.labels objectAtIndex:button.tag] setTextColor:UIColorFromRGB(0x2f3133)];
        }
    }
    NSInteger numberOfSegment = _buttons.count;
    float segmentWidth = self.frame.size.width / numberOfSegment;
    
    CGRect frame = _selectedIndicatorView.frame;
    frame.origin.x = index * segmentWidth + ORIGIN_X_INDICATOR;
    frame.size.width = frame.size.width - ORIGIN_X_INDICATOR;
    [UIView animateWithDuration:0.2 animations:^{
        _selectedIndicatorView.frame = frame;
    }];
    
    
    if (isPress) {
        [_delegate segmentedViewDidSelectItemAtIndex:index];
    } 
}

- (void)setButtonTitles:(NSArray *)titles {
    [self cleanup];
    if (titles.count == 0) {
        return;
    }
    
    NSLog(@"sub view: %@",self.subviews);
    
    
    NSInteger numberOfSegment = titles.count;
    float segmentWidth = self.frame.size.width / numberOfSegment;
    float segmentHeight = self.frame.size.height;
    self.titles = [NSMutableArray arrayWithArray:titles];
    
    // Add buttons
    self.buttons = [NSMutableArray arrayWithCapacity:numberOfSegment];
    
    self.labels  = [NSMutableArray arrayWithCapacity:numberOfSegment];
    for (int i = 0; i < numberOfSegment; i++) {
        
        UIButton *image = [[UIButton alloc] initWithFrame:CGRectMake(i*segmentWidth + (segmentWidth - kWithSizeImage)/2, ORIGIN_Y_Image, kWithSizeImage, kHeightSizeImage)];
//        [image.layer setCornerRadius:image.frame.size.width/2];
//        image.layer.borderWidth = 2.0f;
//        image.layer.borderColor = [UIColor whiteColor].CGColor;
//        image.layer.masksToBounds = NO;
        image.clipsToBounds = YES;
        NSString *imageName = nil;
        switch (i) {
            case 0:
                imageName = @"icon_folder_document";
            break;
            case 1:
                imageName = @"icon_folder_bookMark";
                break;
            case 2:
                imageName = @"icon_folder_history";
                break;
                
            default:
                break;
        }
        [image setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(i * segmentWidth,0, segmentWidth, segmentHeight)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor =  bgColorApp;
        titleLabel.font = [UIFont fontWithName:kFontArial size:SIZE_09];
        id title = [_titles objectAtIndex:i];
        if ([title isKindOfClass:[NSString class]]) {
            titleLabel.text = title;
        } else {
            titleLabel.attributedText = title;
        }
        [self addSubview:titleLabel];
//        [self addSubview:image];
        [_labels addObject:titleLabel];
        [_images addObject:image];
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        button.frame = CGRectMake(i * segmentWidth, 0, segmentWidth, segmentHeight);
        [button addTarget:self action:@selector(buttonPressed:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundColor:[UIColor clearColor]];
        [self addSubview:button];
        
        [_buttons addObject:button];
    }
    
    // Add dividers
    for (int i = 1; i < numberOfSegment; i++) {
        UIView *horizontalDivider;
        horizontalDivider = [[UIView alloc] initWithFrame:CGRectMake(i * segmentWidth, kDividerTopMargin, 0.5, segmentHeight - 2 * kDividerTopMargin)];
        horizontalDivider.backgroundColor = UIColorFromRGB(0xcccccc);
        [self addSubview:horizontalDivider];
    }
    
//    UIView *verticalDivider = [[UIView alloc] initWithFrame:CGRectMake(ORIGIN_X_INDICATOR, segmentHeight - kDividerSize, self.frame.size.width, kDividerSize)];
//    verticalDivider.backgroundColor = UIColorFromRGB(0xcccccc);
//    [self addSubview:verticalDivider];
    
    UIView *selectedIndicator = [[UIView alloc] initWithFrame:CGRectMake(ORIGIN_X_INDICATOR,segmentHeight - kSelectedIndicatorSize, segmentWidth - ORIGIN_X_INDICATOR, kSelectedIndicatorSize)];
    
    selectedIndicator.backgroundColor =   bgColorApp;
    [self addSubview:selectedIndicator];
    _selectedIndicatorView = selectedIndicator;
    
    [self setSelectButtonAtIndex:0 userPressed:NO];
}

- (void)setButtonHomeTitles:(NSArray *)titles {
    [self cleanup];
    if (titles.count == 0) {
        return;
    }
    
    NSLog(@"sub view: %@",self.subviews);
    
    
    NSInteger numberOfSegment = titles.count;
    float segmentWidth = self.frame.size.width / numberOfSegment;
    float segmentHeight = self.frame.size.height;
    self.titles = [NSMutableArray arrayWithArray:titles];
    
    // Add buttons
    self.buttons = [NSMutableArray arrayWithCapacity:numberOfSegment];
    
    self.labels  = [NSMutableArray arrayWithCapacity:numberOfSegment];
    for (int i = 0; i < numberOfSegment; i++) {
        
        UIButton *image = [[UIButton alloc] initWithFrame:CGRectMake(i*segmentWidth + (segmentWidth - kWithSizeImage)/2, ORIGIN_Y_Image, kWithSizeImage, kHeightSizeImage)];
        //        [image.layer setCornerRadius:image.frame.size.width/2];
        //        image.layer.borderWidth = 2.0f;
        //        image.layer.borderColor = [UIColor whiteColor].CGColor;
        //        image.layer.masksToBounds = NO;
        image.clipsToBounds = YES;
        NSString *imageName = nil;
        switch (i) {
            case 0:
                imageName = @"icon_folder_document";
                break;
            case 1:
                imageName = @"icon_folder_bookMark";
                break;
            case 2:
                imageName = @"icon_folder_history";
                break;
            default:
                break;
        }
        
        [image setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(i * segmentWidth,segmentHeight - kHeightSizeLable, segmentWidth, kHeightSizeLable)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor =  bgColorApp;
        titleLabel.font = [UIFont fontWithName:kFontArial size:SIZE_09];
        id title = [_titles objectAtIndex:i];
        if ([title isKindOfClass:[NSString class]]) {
            titleLabel.text = title;
        } else {
            titleLabel.attributedText = title;
        }
        [self addSubview:titleLabel];
        [self addSubview:image];
        [_labels addObject:titleLabel];
        [_images addObject:image];
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        button.frame = CGRectMake(i * segmentWidth, 0, segmentWidth, segmentHeight);
        [button addTarget:self action:@selector(buttonPressed:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundColor:[UIColor grayColor]];
        [self addSubview:button];
        
        [_buttons addObject:button];
    }
    
    // Add dividers
    for (int i = 1; i < numberOfSegment; i++) {
        UIView *horizontalDivider;
        horizontalDivider = [[UIView alloc] initWithFrame:CGRectMake(i * segmentWidth, kDividerTopMargin, 0.5, segmentHeight - 2 * kDividerTopMargin)];
        horizontalDivider.backgroundColor = UIColorFromRGB(0xcccccc);
        [self addSubview:horizontalDivider];
    }
    
    //    UIView *verticalDivider = [[UIView alloc] initWithFrame:CGRectMake(ORIGIN_X_INDICATOR, segmentHeight - kDividerSize, self.frame.size.width, kDividerSize)];
    //    verticalDivider.backgroundColor = UIColorFromRGB(0xcccccc);
    //    [self addSubview:verticalDivider];
    
    UIView *selectedIndicator = [[UIView alloc] initWithFrame:CGRectMake(ORIGIN_X_INDICATOR,segmentHeight - kSelectedIndicatorSize, segmentWidth - ORIGIN_X_INDICATOR, kSelectedIndicatorSize)];
    
    selectedIndicator.backgroundColor =   bgColorApp;
    [self addSubview:selectedIndicator];
    _selectedIndicatorView = selectedIndicator;
    
   [self setSelectButtonAtIndex:0 userPressed:NO];
    
}

- (void)cleanup {
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            continue;
        } else if ([view isKindOfClass:[UIScrollView class]]){
            continue;
        }
        [view removeFromSuperview];
    }
}

- (void)buttonPressed:(UIButton *)sender {
    NSInteger index = sender.tag;
    [self setSelectButtonAtIndex:index userPressed:YES];
    
}



- (void)setTitle:(id)title forButtonAtIndex:(NSInteger)index {
    UILabel *titleLabel = _labels[index];
    if ([title isKindOfClass:[NSString class]]) {
        titleLabel.text = title;
    } else {
        titleLabel.attributedText = title;
    }
}



@end
