//
//  ImageDetailView.m
//  HTDS
//
//  Created by TuanTD on 7/29/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import "ImageDetailView.h"
#import "SlideNavigationController.h"

@implementation ImageDetailView



+ (ImageDetailView*) getViewPopUP {
    ImageDetailView *imageViewPopUp = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    imageViewPopUp = [nibs objectAtIndex:0];
    return imageViewPopUp;
}


- (IBAction)dissmissButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate dissmissImageDetail:self];
    }
}


@end
