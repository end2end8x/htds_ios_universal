//
//  HTContractTableViewCell.h
//  HTDS
//
//  Created by TuanTD on 7/13/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "HTInvoiceModel.h"

@interface HTInvoiceTableViewCell : BaseTableViewCell


@property (nonatomic,strong) HTInvoiceModel *invoiceModel;


@property (weak, nonatomic) IBOutlet UILabel *invoiceSeriLable;
@property (weak, nonatomic) IBOutlet UILabel *taxCodeLable;
@property (weak, nonatomic) IBOutlet UILabel *cyleDateLable;
@property (weak, nonatomic) IBOutlet UILabel *amountTotalLable;


@end
