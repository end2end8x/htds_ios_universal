//
//  HTContractTableViewCell.m
//  HTDS
//
//  Created by TuanTD on 7/13/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTInvoiceTableViewCell.h"

@implementation HTInvoiceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    
}

- (void) setCellData:(id) data {
    [super setCellData:data];
    self.invoiceModel = (HTInvoiceModel*) data;
    self.invoiceSeriLable.text = self.invoiceModel.invoiceSerial;
    self.taxCodeLable.text = self.invoiceModel.taxCode;
    self.cyleDateLable.text = self.invoiceModel.invoiceDate;
    self.amountTotalLable.text = [NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:self.invoiceModel.amountTotal]];
}

@end
