//
//  HTHistoryTableViewCell.m
//  HTDS
//
//  Created by TuanTD on 7/16/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTHistoryTableViewCell.h"
#import "HTHistoryModel.h"

@implementation HTHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
}

- (void) setCellData:(id) data {
    HTHistoryModel *historyModel = (HTHistoryModel*) data;
    self.userNameLable.text = historyModel.userUpdated;
    self.actionLable.text = historyModel.actionCode;
    self.dateLable.text = historyModel.logDate;
    self.tellNumberLale.text = historyModel.tellNumber;
    self.noteLable.text = historyModel.note;
}
@end
