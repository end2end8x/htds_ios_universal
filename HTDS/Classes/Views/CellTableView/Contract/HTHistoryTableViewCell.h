//
//  HTHistoryTableViewCell.h
//  HTDS
//
//  Created by TuanTD on 7/16/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface HTHistoryTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userNameLable;
@property (weak, nonatomic) IBOutlet UILabel *actionLable;
@property (weak, nonatomic) IBOutlet UILabel *dateLable;
@property (weak, nonatomic) IBOutlet UILabel *tellNumberLale;
@property (weak, nonatomic) IBOutlet UILabel *noteLable;



@end
