//
//  HTDocumentHistoryTableViewCell.h
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "HTInfoDocumentModel.h"

@interface HTDocumentHistoryTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (nonatomic,strong) HTInfoDocumentModel *infoDocument;

- (IBAction)historyButtonPressed:(id)sender;

@end
