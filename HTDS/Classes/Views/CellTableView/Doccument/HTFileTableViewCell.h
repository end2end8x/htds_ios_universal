//
//  HTFileTableViewCell.h
//  HTDS
//
//  Created by Anhpt67 on 7/10/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "HTInfoDocumentModel.h"

@interface HTFileTableViewCell : BaseTableViewCell

@property (nonatomic,strong) HTInfoDocumentModel *doccumentModel;

- (IBAction)fileButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *fileButton;

@end
