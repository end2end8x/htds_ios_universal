//
//  HTDoccumentItemTableViewCell.m
//  HTDS
//
//  Created by Anhpt67 on 7/7/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTDoccumentItemTableViewCell.h"
#import "HTInfoDocumentModel.h"

#import "PLConstants.h"

@implementation HTDoccumentItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    
}

- (void) setCellData:(id) data {
    self.infoDocument = (HTInfoDocumentModel*)data;
    self.titleLable.text = self.infoDocument.titleDisplay;
    self.descriptionLable.text = self.infoDocument.descriptionDisplay;
}

+(CGFloat)heightForCellWithObject:(id)baseModel {
    HTInfoDocumentModel *infoDocumentModel = (HTInfoDocumentModel*) baseModel;
    float heightCell = (infoDocumentModel.numberLine == 1?ROW_HEIGHT_INFOR_DOCUMENT_25:ROW_HEIGHT_INFOR_DOCUMENT_36);
    
    CGSize titleSize = [infoDocumentModel.descriptionDisplay sizeWithFont:[UIFont fontWithName:kFontArial size:SIZE_14]
                                          constrainedToSize:CGSizeMake(172, CGFLOAT_MAX)
                                              lineBreakMode:NSLineBreakByWordWrapping];
    if (titleSize.height > heightCell) {
        heightCell  = titleSize.height;
    }
    return heightCell;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.titleLable.frame;
    float heightCell = (self.infoDocument.numberLine == 1?ROW_HEIGHT_INFOR_DOCUMENT_25:ROW_HEIGHT_INFOR_DOCUMENT_36);
    frame.size.height = heightCell;
    self.titleLable.frame = frame;
    
//    frame = self.descriptionLable.frame;
//    frame.size.height = [HTDoccumentItemTableViewCell heightForCellWithObject:self.infoDocument] - 3;
//    self.descriptionLable.frame = frame;
    
}


@end
