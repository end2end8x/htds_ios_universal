//
//  HTDocumentHistoryTableViewCell.m
//  HTDS
//
//  Created by TuanTD on 7/20/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTDocumentHistoryTableViewCell.h"
#import "HTDoccumentDetailViewController.h"


@implementation HTDocumentHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
}

- (void) setCellData:(id) data {
    self.infoDocument = data;
    [self.titleButton setImage:[UIImage imageNamed:self.infoDocument.thumbImage] forState:UIControlStateNormal];
    [self.titleButton setTitle:self.infoDocument.titleDisplay forState:UIControlStateNormal];
}

- (IBAction)historyButtonPressed:(id)sender {
    if (self.controller && [self.controller respondsToSelector:@selector(historyButtonPressedDelegate:)]) {
        [self.controller historyButtonPressedDelegate:_infoDocument];
    } 
}
@end



















