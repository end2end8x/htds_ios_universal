//
//  HTFileTableViewCell.m
//  HTDS
//
//  Created by Anhpt67 on 7/10/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTFileTableViewCell.h"
#import "HTDoccumentDetailViewController.h"

@implementation HTFileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    
}

- (void) setCellData:(id) data {
    self.doccumentModel = (HTInfoDocumentModel*) data;
    [self.fileButton setTitle:self.doccumentModel.titleDisplay forState:UIControlStateNormal];
    [self.fileButton setImage:[UIImage imageNamed:self.doccumentModel.thumbImage] forState:UIControlStateNormal];
    [self.fileButton setTag: [self.doccumentModel.jasperId intValue]];
//    [self.fileButton setTag:self.doccumentModel.jasperCode forState:UIControlStateNormal];
}

- (IBAction)fileButtonPressed:(id)sender {
    UIButton* fileButton = (UIButton* ) sender;
    
    NSLog(@"fileButtonPressed jasperId %ld", fileButton.tag);
    
    if (self.controller && [self.controller respondsToSelector:@selector(downLoadFileDelegate:)]) {
        [self.controller downLoadFileDelegate: [NSString stringWithFormat:@"%ld", fileButton.tag]];
    }
    
}
@end
