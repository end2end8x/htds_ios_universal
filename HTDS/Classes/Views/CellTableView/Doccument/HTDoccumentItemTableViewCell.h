//
//  HTDoccumentItemTableViewCell.h
//  HTDS
//
//  Created by Anhpt67 on 7/7/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "HTInfoDocumentModel.h"

@interface HTDoccumentItemTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLable;
@property (nonatomic,strong) HTInfoDocumentModel *infoDocument;

@end
