//
//  BDLeageTableViewCell.m
//  BongDa
//
//  Created by Phung Khac Tuan on 3/2/15.
//  Copyright (c) 2015 VEGA Corp. All rights reserved.
//

#import "HTCategoriesTableViewCell.h"
#import "PLConstants.h"

@implementation HTCategoriesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    
}

- (void) setCellData:(id) data {
    _categorie = (HTBaseCategories*) data;
    self.titleLabel.text = _categorie.titleDisplay;
}

+(CGFloat)heightForCellWithObject:(id)baseModel {
    HTBaseCategories *baseCategorie = (HTBaseCategories*) baseModel;
    
    CGFloat heightCell = ROW_HEIGHT_MENU_ITEM;
    
    CGSize titleSize = [baseCategorie.titleDisplay sizeWithFont:[UIFont fontWithName:kFontArial size:SIZE_17]
                                                        constrainedToSize:CGSizeMake(275, CGFLOAT_MAX)
                                                            lineBreakMode:NSLineBreakByWordWrapping];
    if (titleSize.height > 21) {
        heightCell  = titleSize.height + 23;
    }
    return heightCell;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    CGRect frame = _titleLabel.frame;
    frame.size.height = [HTCategoriesTableViewCell heightForCellWithObject:_categorie] - 23;
    _titleLabel.frame = frame;
    
    frame = _line.frame;
    frame.origin.y = [HTCategoriesTableViewCell heightForCellWithObject:_categorie] - 1;
    _line.frame = frame;
}

@end
