//
//  BDLeageTableViewCell.h
//  BongDa
//
//  Created by Phung Khac Tuan on 3/2/15.
//  Copyright (c) 2015 VEGA Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "HTBaseCategories.h"

@interface HTCategoriesTableViewCell : BaseTableViewCell


@property (nonatomic,strong) HTBaseCategories *categorie;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *line;


@end
