//
//  HTDoccumentTableViewCell.h
//  HTDS
//
//  Created by Anhpt67 on 7/6/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "HTDocumentModel.h"

@interface HTDoccumentTableViewCell : BaseTableViewCell

@property (nonatomic,strong) HTDocumentModel *doccumentModel;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *dateChargLable;
@property (weak, nonatomic) IBOutlet UILabel *serviceLable;
@property (weak, nonatomic) IBOutlet UILabel *contractNumberLable;
@property (weak, nonatomic) IBOutlet UILabel *statusLable;
@property (weak, nonatomic) IBOutlet UILabel *createTimeLable;
@property (weak, nonatomic) IBOutlet UIButton *bookMarkbutton;
@property (weak, nonatomic) IBOutlet UIImageView *signatureImageView;


- (IBAction)bookMarkbuttonPressed:(id)sender;



@end
