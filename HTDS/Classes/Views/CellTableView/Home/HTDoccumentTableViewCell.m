//
//  HTDoccumentTableViewCell.m
//  HTDS
//
//  Created by Anhpt67 on 7/6/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTDoccumentTableViewCell.h"
#import "HTDoccumentDetailViewController.h"
#import "BaseViewController.h"
#import "HTReportBookMarkManaged.h"
#import "HTEmployViewController.h"
#import "HTManagerViewController.h"
#import "HTSearchViewController.h"
#import "PLConstants.h"
#import "HTAccount.h"
#import "HTPreferences.h"


@implementation HTDoccumentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.doccumentModel = nil;
}

- (void) setCellData:(id) data {
    self.doccumentModel = (HTDocumentModel*) data;
    self.titleLable.text = self.doccumentModel.contractName;
    self.dateChargLable.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"TITLE_DATE_CHARG", nil),self.doccumentModel.icCycleDate];
    self.serviceLable.text   = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"TITLE_SERVICE", nil),self.doccumentModel.serviceName];
    self.contractNumberLable.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"TITLE_NUMBER_CONTRACT", nil),self.doccumentModel.contractNo];
    NSString *status = [NSString stringWithFormat:@"status%@",self.doccumentModel.status];
    self.statusLable.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"TITLE_STATUS", nil),NSLocalizedString(status, nil)];
    self.createTimeLable.text      = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"TITLE_CREATE_DATE", nil),self.doccumentModel.createDateTime];
    [self.bookMarkbutton setSelected:_doccumentModel.isBookmark];
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSString *imageName = @""; 
    BaseViewController *baseViewController = (BaseViewController*)self.controller;
    if (self.doccumentModel.status.integerValue == STATUS_WAITING_REVIEW) {
        imageName = @"img_icon_unapprove";
    } else if (self.doccumentModel.status.integerValue == STATUS_WAITING_SIGNATURE) {
        if (account.partnerUserType == PARTNER_TYPE_ADMIN) {
            if ([baseViewController isKindOfClass:[HTBaseTabViewController class]]) {
                HTBaseTabViewController *baseTabViewController = (HTBaseTabViewController*) baseViewController;
                if (baseTabViewController.segmentView.tag == TAB_REPORT_HISTORY ||
                    [baseTabViewController isKindOfClass:[HTEmployViewController class]]) {
                    imageName = @"img_icon_approve";
                } else {
                    imageName = @"img_icon_unsignature";
                }
            } 
        } else if (account.partnerUserType == PARTNER_TYPE_MANAGER) {
            imageName = @"img_icon_unsignature";
        } else {
            imageName = @"img_icon_approve";
        }
    } else if (self.doccumentModel.status.integerValue == STATUS_MANAGER_SIGN) {
        if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
            imageName = @"img_icon_unsignature";
        } else {
            imageName = @"img_icon_signature";
        }
    } else if (self.doccumentModel.status.intValue == STATUS_VIETTEL) {
        imageName = @"img_icon_signature";
    }
    [self.signatureImageView setImage:[UIImage imageNamed:imageName]];
}

- (IBAction)showDetailDoccument:(id)sender {
    UIStoryboard *mainStoryboad = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BaseViewController *baseController = (BaseViewController*) self.controller; 
    HTDoccumentDetailViewController *doccumentDetailViewController = [mainStoryboad instantiateViewControllerWithIdentifier:NSStringFromClass([HTDoccumentDetailViewController class])];
    doccumentDetailViewController.shouldDisplayBackButton = YES;
    doccumentDetailViewController.documentModel = self.doccumentModel;
    if ([baseController isKindOfClass:[HTEmployViewController class]]) {
        doccumentDetailViewController.typeStaff = MENU_ITEM_EMPLOYER;
    } else if ([baseController isKindOfClass:[HTManagerViewController class]]) {
        doccumentDetailViewController.typeStaff = MENU_ITEM_MANAGER;
    } else if ([baseController isKindOfClass:[HTSearchViewController class]]) {
        HTSearchViewController *searchViewController = (HTSearchViewController*)baseController;
        HTBaseTabViewController *baseTabController   = (HTBaseTabViewController*) searchViewController.delegate;
        if ([baseTabController isKindOfClass:[HTEmployViewController class]]) {
            doccumentDetailViewController.typeStaff = MENU_ITEM_EMPLOYER;
        } else if ([baseTabController isKindOfClass:[HTManagerViewController class]]) {
            doccumentDetailViewController.typeStaff = MENU_ITEM_MANAGER;
        }
    }
    
    [baseController.navigationController pushViewController:doccumentDetailViewController animated:YES];
}

- (IBAction)bookMarkbuttonPressed:(id)sender { 
    [self.doccumentModel bookMarkOrUnBookMark];
}
@end
