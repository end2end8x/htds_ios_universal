//
//  PLMenuItemTableViewCell.h
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/9/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "HTMenuItem.h"

@interface PLMenuItemTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;




@end
