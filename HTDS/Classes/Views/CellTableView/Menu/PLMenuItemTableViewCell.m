//
//  PLMenuItemTableViewCell.m
//  PhapLuat
//
//  Created by Phùng Khắc Tuấn  on 9/9/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "PLMenuItemTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "PLFunctions.h"


@implementation PLMenuItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void) prepareForReuse {
    [super prepareForReuse];
    [self.menuItemImageView setImage:nil];
}

- (void) setCellData:(id)data {
    [super setCellData:data];
    HTMenuItem *menuItem = (HTMenuItem*) data;
    self.titleLabel.text = menuItem.titleDisplay.capitalizedString;
    [self.menuItemImageView setImage:imageWithPath(menuItem.thumbUrlString)];
}

@end
