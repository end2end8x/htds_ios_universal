//
//  HTDS
//
//  Created by Tuanpk on 6/6/15.
//  Copyright (c) 2015 Viettel Corp. All rights reserved.
//

#import "HTAppDelegate.h"
#import "GAI.h"
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "HTMenuViewController.h"
#import "RequestOperationManager.h"
#import "HTContractModel.h"
#import "HTServiceModel.h"
#import "HTStatusModel.h"
#import "PLFunctions.h"
#import "PLConstants.h"
#import "HTHomeViewController.h"
#import "HTBaseTabViewController.h"
#import "HTPartnerUserModel.h"
#import "HTPartnerModel.h"
#import "HTStaffModel.h"
#import "HTAccount.h"
#import "HTPreferences.h"
#import <Parse/Parse.h>
#import <ParseCrashReporting/ParseCrashReporting.h>


const CGFloat SLIDEMENU_MIN_SCALE                   = .8f;

@implementation HTAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma mark - Common
+ (HTAppDelegate *)sharedDelegate {
    static HTAppDelegate *_sharedDelegate;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDelegate = [[UIApplication sharedApplication] delegate];
    });
    return _sharedDelegate;
}

- (BOOL)hasInternetConnection {
    NetworkStatus networkStatus = [_networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    }
    return YES;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

//- (void)initializeGoogleAnalytics {
//    // Optional: automatically send uncaught exceptions to Google Analytics.
//    [GAI sharedInstance].trackUncaughtExceptions = YES;
//    
//    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
//    [GAI sharedInstance].dispatchInterval = 20;
//    
//    // Optional: set Logger to VERBOSE for debug information.
//    // [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
//    
//    // Initialize tracker. Replace with your tracking ID.
//    [[GAI sharedInstance] trackerWithTrackingId:kGoogleAnalyticsId];
//}

- (NSString *)uuidString {
    // Returns a UUID
    
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    
    return uuidString;
}

#pragma mark - App Lifecycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSLog(@"font :%@",[UIFont familyNames]);
//    [self initializeGoogleAnalytics];
    
    _listService = [NSMutableArray array];
    _listContract = [NSMutableArray array];
    _listStatus   = [NSMutableArray array];
    _listEmployer = [NSMutableArray array];
    _listPartners = [NSMutableArray array];
    _listStaff = [NSMutableArray array];
    
    HTContractModel *contractAll = [HTContractModel new];
    contractAll.objectId         = [NSNumber numberWithInteger:-1];
    contractAll.titleDisplay     = NSLocalizedString(@"TITLE_ALL", nil);
    [_listContract addObject:contractAll];
    
    HTServiceModel *serviceAll = [HTServiceModel new];
    serviceAll.objectId         = [NSNumber numberWithInteger:-1];
    serviceAll.titleDisplay     = NSLocalizedString(@"TITLE_ALL", nil);
    [_listService addObject:serviceAll];
    
    HTStatusModel *statusAll = [HTStatusModel new];
    statusAll.objectId         = [NSNumber numberWithInteger:-1];
    statusAll.titleDisplay     = NSLocalizedString(@"TITLE_ALL", nil);
    [_listStatus addObject:statusAll];
    
    HTStaffModel *staff = [HTStaffModel new];
    staff.objectId = [NSNumber numberWithInt:-1];
    staff.titleDisplay = NSLocalizedString(@"TITLE_ALL", nil);
    [_listStaff addObject:staff];
    
    HTPartnerModel *partner = [HTPartnerModel new];
    partner.objectId         = [NSNumber numberWithInteger:-1];
    partner.titleDisplay     = NSLocalizedString(@"TITLE_ALL", nil);
    [_listPartners addObject:partner];
    
    HTPartnerUserModel *employer = [HTPartnerUserModel new];
    employer.objectId         = [NSNumber numberWithInteger:-1];
    employer.titleDisplay     = NSLocalizedString(@"TITLE_ALL", nil);
    [_listEmployer addObject:employer];
    
    if ((NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"HasLaunchedOnce"]) {
        // app already launched
        NSLog(@"abc");
    } else {
        NSString *uuidString = [self uuidString];
        NSLog(@"string uuid: %@",uuidString);
        [[NSUserDefaults standardUserDefaults] setObject:uuidString forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    // Network notifier
    self.networkReachability = [Reachability reachabilityForInternetConnection];
    [_networkReachability startNotifier];
  
    
/*
    // register notification
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    
    // check notification
    [self checkNotificationForLaunchOptions:launchOptions];
*/
    
// end register notification
    [ParseCrashReporting enable];
    [Parse setApplicationId:@"BncVL3BpDk4mb23DnGhJ5oE5ovjoRKJAkGb24sFr"
                  clientKey:@"KjAGsc26ryExYIhFRdougwdWf7i91ks3FukR9D3L"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // Register for Push Notitications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];

    //MARK: uncomment to run
    [self managedObjectContext];
// end init core data
    
    
    // Other works here
    [self setupSlideMenu];
    
    return YES;
}

-(void) setupSlideMenu {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
															 bundle: nil];
    NSString *className = NSStringFromClass([HTMenuViewController class]);
	HTMenuViewController *leftMenuViewController = [mainStoryboard instantiateViewControllerWithIdentifier:className];
    HTHomeViewController *homeViewController = [[HTHomeViewController alloc] initWithNibName:NSStringFromClass([HTBaseTabViewController class]) bundle:nil];
	[SlideNavigationController sharedInstance].leftMenu = leftMenuViewController;

    NSArray *viewControllers = [NSArray arrayWithObjects:homeViewController, nil];
    [SlideNavigationController sharedInstance].viewControllers = viewControllers;
    id <SlideNavigationContorllerAnimator> revealAnimator = [[SlideNavigationContorllerAnimatorScale alloc]
                                                             initWithMinimumScale:SLIDEMENU_MIN_SCALE];
    [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
    [SlideNavigationController sharedInstance].avoidSwitchingToSameClassViewController = NO;
    customizeAppearance([SlideNavigationController sharedInstance]);
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void) backgroundHandler {
     NSLog(@"backgroundHandler");
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
//    RequestOperationManager *asynSocket = [RequestOperationManager sharedManager];
//    if (![asynSocket isConnected] || ![asynSocket isDisconnected]) {
//        [asynSocket setDelegate:nil];
//        [asynSocket disconnect];
//    }
    
    //MARK: TUANTD7 giu ket noi duoi background
    BOOL backgroundAccepted = [[UIApplication sharedApplication] setKeepAliveTimeout:600 handler:^{ [self backgroundHandler]; }];
    if (backgroundAccepted) {
        NSLog(@"VOIP backgrounding accepted");
    } else {
        showLocalNotification(@"Backgrounding denied", 0, [NSDate dateWithTimeIntervalSinceNow:1]);
    }
    
}

- (void)applicationWillEnterForeground:(UIApplication *) application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    RequestOperationManager *asynSocket = [RequestOperationManager sharedManager];
    asynSocket.delegate = self;
    if (![asynSocket isConnected] || [asynSocket isDisconnected]) {
        [asynSocket connecttoHost:self];
        showMBProgressHUD([HTAppDelegate sharedDelegate].window , @"RELOAD", YES, MBProgressHUDModeText, HTToastTimeOut);
    }
    
    if (_userInfo) {
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:NO];
        [HTMenuViewController sharedInstance].rowMenuItemSelected = 0;
        [[HTMenuViewController sharedInstance] loadMenuItems];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kPLUserDidLoginNotification object:nil];
        
        _userInfo = nil;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//    [RequestOperationManager sharedManager].delegate = nil;
//    [[RequestOperationManager sharedManager] disconnect];
    
    //MARK: uncomment khi dự án sử dụng Core Data
    [self saveContext];
}

#pragma mark - Notifications
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)tokenData {
    NSString *deviceToken = [[tokenData description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    //MARK: lưu lại device token để gửi lên server ..
    
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:tokenData];
    [currentInstallation saveEventually];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Err: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *) userInfo {
    [self processRemoteNotification:userInfo forState:application.applicationState];
    
    [PFPush handlePush:userInfo];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"userInfo: %@", jsonString);
    //MARK:TUANTD7 handle noi dung push gui den o day khi app dang chay
    
    if (userInfo) {
        _userInfo = [NSDictionary dictionaryWithDictionary:userInfo];
    }
    
}

- (void)checkNotificationForLaunchOptions:(NSDictionary *)launchOptions {
    if (launchOptions) {
        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo) {
            [self processRemoteNotification:userInfo forState:UIApplicationStateInactive];
        }
    }
}

- (void)processRemoteNotification:(NSDictionary *)userInfo forState:(UIApplicationState)state {
    
    //MARK: process notification
    //FIXME: cần kiểm tra người dùng đã đăng nhập ứng dụng hay chưa, để có xử lý phù hợp với yêu cầu về mặt kinh doanh của app
    if (state == UIApplicationStateInactive) { // trường hợp người dùng mở app từ notification
        
    } else { // state == UIApplicationStateActive, trường hợp app đang chạy và nhận được notification
        
    }
}

#pragma mark - Core Data
// Save data
- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"HTDS" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:kDatabaseFileName];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (void) socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    NSLog(@"Request didConnectToHost");
    //[[RequestOperationManager sharedManager] readDataWithTimeout:-1 tag:0];
    [sock performBlock:^{
        [sock enableBackgroundingOnSocket];
    }];
}


@end
