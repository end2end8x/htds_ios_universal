//
//  HTDS
//
//  Created by Tuanpk on 6/6/15.
//  Copyright (c) 2015 Viettel Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "GCDAsyncSocket.h"
#import <CoreData/CoreData.h>
#import "HTMutableArray.h"


@interface HTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (HTAppDelegate *)sharedDelegate;


@property (nonatomic,strong) HTMutableArray *listReason;
@property (nonatomic,strong) HTMutableArray *listEmployer;
@property (nonatomic,strong) HTMutableArray *listPartners;
@property (nonatomic,strong) HTMutableArray *listStaff;
@property (nonatomic,strong) HTMutableArray *listContract;
@property (nonatomic,strong) HTMutableArray *listService;
@property (nonatomic,strong) HTMutableArray *listStatus;
@property (nonatomic,strong) HTMutableArray *listReportType;
@property (nonatomic, strong) Reachability *networkReachability;

@property (nonatomic, strong) NSDictionary *userInfo;

@property (nonatomic,strong) NSTimer *timer;

// Core data
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readwrite) CGFloat  animatedDistance;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (BOOL)hasInternetConnection;

@end
