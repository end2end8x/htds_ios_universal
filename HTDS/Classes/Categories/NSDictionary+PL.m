//
//  NSDictionary+ImuzikVideo.m
//  ImuzikVideo
//
//  Created by Tuanpk on 4/28/14.
//  Copyright (c) 2014 VegaCorp. All rights reserved.
//

#import "NSDictionary+PL.h"

@implementation NSDictionary (PL)

- (NSString *)stringForKey:(NSString *)key {
    if ([self objectForKey:key] != nil && ![[self objectForKey:key] isKindOfClass:[NSNull null]]) {
        return [self objectForKey:key];
    }
    return @"";
}

- (NSInteger)integerForKey:(NSString *)key {
//    return [[self objectForKey:key] integerValue];
    NSObject* msgo = [self objectForKey:key];
    int value;
    if ([msgo isKindOfClass:[NSNull class]])
        value = 0;
    else value = [(NSNumber*)msgo intValue];
    
    return value;

}

- (NSNumber*)numberForKey:(NSString *)key {
    NSObject* msgo = [self objectForKey:key];
    NSNumber* value;
    if ([msgo isKindOfClass:[NSNull class]])
        value = 0;
    else value = (NSNumber*)msgo;
    return value;
}

- (BOOL)boolForKey:(NSString *)key {
    return [[self objectForKey:key] boolValue];
}


@end
