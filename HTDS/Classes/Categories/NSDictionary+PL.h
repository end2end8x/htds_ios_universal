//
//  NSDictionary+ImuzikVideo.h
//  ImuzikVideo
//
//  Created by Tuanpk on 4/28/14.
//  Copyright (c) 2014 VegaCorp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PL)

- (NSString *)stringForKey:(NSString *)key;
- (NSInteger)integerForKey:(NSString *)key;
- (NSNumber*) numberForKey:(NSString *)key;
- (BOOL)boolForKey:(NSString *)key;

@end
