//
//  Parameters.h
//  PhapLuat
//
//  Created by Tuanpk on 9/17/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString *const HTAPIBaseURLString;
extern const int HTPortConnect;
extern const int HTRequestTimeOut;
extern const int HTRequestSignTimeOut;
extern const int HTToastTimeOut;
extern const int HTTimerTimeOut;
extern const int HTSessionTimeOut;

extern const int TEXT_FIELD_LENGTH;
extern const int TEXT_VIEW_LENGTH;
extern const int TEXT_VIEW_REASON_LENGTH;

// method request
extern NSString *const kMethodGet;
extern NSString *const kMethodPost;

// path request
extern NSString *const kPathLogin;
extern NSString *const kPathSearchReport;
extern NSString *const kPathSearchReportViettelSign;
extern NSString *const kPathSearchContract;
extern NSString *const kPathSearchService;
extern NSString *const kPathDownLoadFileReport;
extern NSString *const kPathGetListInvoice;
extern NSString *const kPathDownloadFileInvoice;
extern NSString *const kPathAddInvoice;
extern NSString *const kPathExportReport;
extern NSString *const kPathSearchReportType;
extern NSString *const kPathSearchReportHistory;
extern NSString *const kPathSearchUserHistory;
extern NSString *const kPathSearchPartner;
extern NSString *const kPathSearchPartnerUser;
extern NSString *const kPathSearchStaff;
extern NSString *const kPathSignatureReport;
extern NSString *const kPathRefuseSignatureReport;
extern NSString *const kPathCancelSignatureReport;
extern NSString *const kPathReviewReport;
extern NSString *const kPathUnReviewReport;
extern NSString *const kPathRefuseApproveReport;
extern NSString *const kPathSearchReasons;


// key

extern NSString *const kCode;
extern NSString *const kMessage;
extern NSString *const kData;
extern NSString *const kTotal;

extern NSString *const kRest;
extern NSString *const kUsername;
extern NSString *const kSession;
extern NSString *const kPath;
extern NSString *const kMethod;
extern NSString *const kParams;
extern NSString *const kName;
extern NSString *const kValue;
extern NSString *const kOffset;
extern NSString *const kLimit;
extern NSString *const kBody; 


extern NSString *const kType;
extern NSString *const kIdentifier;
extern NSString *const kTitle;
extern NSString *const kDescription;
extern NSString *const kThumbnail;
extern NSString *const kObjectType;
extern NSString *const kObjectId; 

// key request
extern NSString *const kUser;
extern NSString *const kPass;

extern NSString *const kICReportId;
extern NSString *const kReportDateTime; 

extern NSString *const kContractName;
extern NSString *const kServiceName;
extern NSString *const kContractNo;

extern NSString *const kPartnerName;
extern NSString *const kAmountTotal;
extern NSString *const kICCycleDate;
extern NSString *const kAmountRevenue;
extern NSString *const kAmountBonus;
extern NSString *const kAmountFee;
extern NSString *const kAmountPayment;
extern NSString *const kTemplateId;

extern NSString *const kDeptId;
extern NSString *const kContractId;
extern NSString *const kContractCode;
extern NSString *const kServiceId;
extern NSString *const kPartnerId;
extern NSString *const kPartnerCode;
extern NSString *const kStaffCode;
extern NSString *const kIcReportType;
extern NSString *const kStatus;
extern NSString *const kTemplateId;
extern NSString *const kTemplateId;



extern NSString *const kSignedTimes;
extern NSString *const kFromDate;
extern NSString *const kToDate;
extern NSString *const kPrintedTimes;
extern NSString *const kCreateDateTime;
extern NSString *const kAmountTotalNotVAT;
extern NSString *const kAmountRevenueNotVAT;
extern NSString *const kAmountBonusNotVAT;
extern NSString *const kAmountFeeNotVAT;
extern NSString *const kAmountPaymentNotVAT;
extern NSString *const kSignedByPartner;
extern NSString *const kCreationType;

extern NSString *const kHardInvoice;
extern NSString *const kInStorage;
extern NSString *const kDeptId;
extern NSString *const kContractId;
extern NSString *const kContractCode;
extern NSString *const kServiceId;
extern NSString *const kServiceCode;
extern NSString *const kPartnerId;
extern NSString *const kPartnerCode;
extern NSString *const kStaffCode;
extern NSString *const kStatus;
extern NSString *const kIsBookmark;
extern NSString *const kAddress;
extern NSString *const kEmail;
extern NSString *const kPartnerUserType;
extern NSString *const kFullName;
extern NSString *const kTelNumber;
extern NSString *const kMd5File;

extern NSString *const kStaffName;
extern NSString *const kAmountNotTax;
extern NSString *const kAmountTax;
extern NSString *const kAttachFileName;
extern NSString *const kContent;
extern NSString *const kInvoiceDate;
extern NSString *const kInvoiceSerial;
extern NSString *const kInvoiceSign;
extern NSString *const kTaxCode;
extern NSString *const kVat;
extern NSString *const kUnitIssue;
extern NSString *const kSuggesstion;

extern NSString *const kReportCode;
extern NSString *const kReportEnum;
extern NSString *const kReportId;
extern NSString *const kReportName;
extern NSString *const kReportType;
extern NSString *const kActionCode;
extern NSString *const kLogDate;
extern NSString *const kUserUpdated;
extern NSString *const kPartnerUserId;
extern NSString *const kStaffId;
extern NSString *const kMenu;
extern NSString *const kRole;
extern NSString *const kObjectName;
extern NSString *const kObjectCode;
extern NSString *const kComment;
extern NSString *const kProvider;
extern NSString *const kNote;
extern NSString *const kReasonId;

extern NSString *const kReasonCode;
extern NSString *const kReasonName;

// TuanTD7 them key many File jasper
extern NSString *const kJasperId;
extern NSString *const kNumberFileJasper ;
extern NSString *const kListJasperId;
extern NSString *const kListJasperCode;
extern NSString *const kListJasperName;
extern NSString *const kHasInvoice;

extern NSString *const VIP;

extern NSString *const kDate;
 

typedef enum {
    REQUEST_FOR_DISCONNECT = 0,
    REQUEST_FOR_PING,
    REQUEST_FOR_MESSAGE,
    REQUEST_FOR_NOTIFY,
    REQUEST_FOR_CONNECT,
    
    REQUEST_FOR_LOGIN,
    REQUEST_FOR_GET_LIST_REPORT,
    REQUEST_FOR_GET_LIST_REPORT_VIETEL_SIGN,
    REQUEST_FOR_GET_REPORT_BOOKMARK,
    REQUEST_FOR_GET_REPORT_HISTORY,
    REQUEST_FOR_GET_ALL_CONTRACT,
    REQUEST_FOR_GET_ALL_EMPLOYER,
    REQUEST_FOR_GET_ALL_STAFF,
    REQUEST_FOR_GET_ALL_PARTNERS,
    REQUEST_FOR_GET_ALL_SERVICE,
    REQUEST_FOR_SERCH_REPORT,
    REQUEST_FOR_DOWNLOAD_FILE,
    REQUEST_FOR_GET_LIST_INVOICE,
    REQUEST_FOR_DOWNLOAD_FILE_INVOICE,
    REQUEST_FOR_ADD_INVOICE,
    REQUEST_FOR_EXPORT_REQUEST,
    REQUEST_FOR_GET_ALL_REPORTTYPE,
    REQUEST_FOR_GET_HISTORY_SIGN,
    REQUEST_FOR_APPROVE_REPORT,
    REQUEST_FOR_SIGNATURE_REPORT,
    REQUEST_FOR_UNSIGNATURE_REPORT,
    REQUEST_FOR_UNREVIEW_REPORT,
    REQUEST_FOR_GET_LIST_REASON
} RequestIdentifier;

enum {
    NoError                    = 0,    // Không có lỗi
    EXCEPTION,
    SESSION_TIMEOUT,
    INPUT_INVALID,
    LOGIN_FAIL,
    FILE_NOT_FOUND,
    FILE_DUPLICATE,
    SIMCA,
    NO_DATA_RESPONST,
    DATABASE,
    NETWORK,
    CONNECTION,
    TIME_OUT,
    
    SOCKET_READ_TIMEOUT = 4, // Loi doc du lieu qua time out
    SOCKET_CLOSE_BY_REMOTE = 7,    // lỗi connect socket 2 lan
    SOCKET_CONNECT_DUPLICATE = 8,    // lỗi connect socket 2 lan
    SOCKET_CONNECT_NETWORK_ERROR = 51,   // Lỗi mạng asyn socket
    SOCKET_CONNECT_REFUSED = 61
};




