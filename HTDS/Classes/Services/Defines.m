//
//  Parameters.m
//  PhapLuat
//
//  Created by Tuanpk on 9/17/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *const HTAPIBaseURLString = @"10.58.71.137";
//NSString *const HTAPIBaseURLString = @"192.168.1.12";
const int HTPortConnect = 8789; // post socket
const int HTRequestTimeOut = 20;
const int HTRequestSignTimeOut = 75;
const int HTToastTimeOut = 1;
const int HTTimerTimeOut = 60;
const int HTSessionTimeOut = 86400;

const int TEXT_FIELD_LENGTH = 20;
const int TEXT_VIEW_LENGTH = 50;
const int TEXT_VIEW_REASON_LENGTH = 200;

// method request
NSString *const kMethodGet    = @"GET";
NSString *const kMethodPost   = @"POST";

// path request
NSString *const kPathLogin    = @"login";
NSString *const kPathSearchReport   = @"searchReportTemplateJasper";
NSString *const kPathSearchReportViettelSign   = @"searchReportTemplateJasperViettelSign";
NSString *const kPathSearchContract    = @"searchContract";
NSString *const kPathSearchService     = @"searchService";
NSString *const kPathDownLoadFileReport     = @"downloadReport";
NSString *const kPathGetListInvoice      = @"searchReportInvoice";
NSString *const kPathDownloadFileInvoice     = @"downloadReportInvoice";
NSString *const kPathAddInvoice      = @"uploadReportInvoice";
NSString *const kPathExportReport      = @"exportReportMedia";
NSString *const kPathSearchReportType      = @"searchReportType";
NSString *const kPathSearchReportHistory      = @"searchReportHistory";
NSString *const kPathSearchUserHistory      = @"searchUserHistory";
NSString *const kPathSearchPartner         = @"searchPartner";
NSString *const kPathSearchPartnerUser         = @"searchPartnerUser";
NSString *const kPathSearchStaff               = @"searchStaff";
NSString *const kPathSignatureReport           = @"signature";
NSString *const kPathRefuseSignatureReport     = @"refuseSignature";
NSString *const kPathCancelSignatureReport     = @"cancelSignature";
NSString *const kPathReviewReport              = @"approve";
NSString *const kPathUnReviewReport            = @"cancelApprove";
NSString *const kPathRefuseApproveReport       = @"refuseApprove";
NSString *const kPathSearchReasons             = @"searchReason";

// key
NSString *const kCode    = @"code";
NSString *const kMessage = @"message";
NSString *const kData    = @"data";
NSString *const kTotal   = @"total";



NSString *const kRest    = @"rest";
NSString *const kUsername    = @"username";
NSString *const kSession    = @"session";
NSString *const kPath       = @"path";
NSString *const kMethod     = @"method";
NSString *const kParams     = @"params";
NSString *const kName       = @"name";
NSString *const kValue      = @"value";
NSString *const kOffset      = @"offset";
NSString *const kLimit      = @"limit";
NSString *const kBody      = @"body";


// key base
NSString *const kType          = @"type";
NSString *const kIdentifier    = @"id";
NSString *const kTitle         = @"title";
NSString *const kDescription   = @"description";
NSString *const kObjectType      = @"object_type";
NSString *const kThumbnail     = @"thumb_url";
NSString *const kObjectId     = @"objectId";

// key request
NSString *const kUser     = @"userName";
NSString *const kPass     = @"password";
NSString *const kICReportId     = @"icReportId";
NSString *const kReportDateTime     = @"reportDateTime";
NSString *const kContractName     = @"contractName";
NSString *const kServiceName     = @"serviceName";
NSString *const kContractNo     = @"contractNo";
NSString *const kPartnerName     = @"partnerName";
NSString *const kAmountTotal     = @"amountTotal";
NSString *const kICCycleDate     = @"icCycleDate";
NSString *const kAmountRevenue     = @"amountRevenue";
NSString *const kAmountBonus     = @"amountBonus";
NSString *const kAmountFee     = @"amountFee";
NSString *const kAmountPayment     = @"amountPayment";
NSString *const kTemplateId     = @"templateId";

NSString *const kDeptId     = @"deptId";
NSString *const kContractId = @"contractId";
NSString *const kContractCode     = @"contractCode";
NSString *const kServiceId     = @"serviceId";
NSString *const kServiceCode     = @"serviceCode";
NSString *const kPartnerId     = @"partnerId";
NSString *const kPartnerCode     = @"partnerCode";
NSString *const kStaffId     = @"staffId";
NSString *const kStaffCode     = @"staffCode";
NSString *const kIcReportType     = @"icReportType";
NSString *const kStatus     = @"status";

NSString *const kSignedTimes     = @"signedTimes";
NSString *const kFromDate = @"fromDate";
NSString *const kToDate     = @"toDate";
NSString *const kPrintedTimes     = @"printedTimes";
NSString *const kCreateDateTime     = @"createDateTime";
NSString *const kAmountTotalNotVAT     = @"amountTotalNotVAT";
NSString *const kAmountRevenueNotVAT     = @"amountRevenueNotVAT";
NSString *const kAmountBonusNotVAT     = @"amountBonusNotVAT";
NSString *const kAmountFeeNotVAT     = @"amountFeeNotVAT";
NSString *const kAmountPaymentNotVAT     = @"amountPaymentNotVAT";
NSString *const kSignedByPartner     = @"signedByPartner";

NSString *const kCreationType     = @"creationType";
NSString *const kHardInvoice     = @"hardInvoice";
NSString *const kInStorage     = @"inStorage";
NSString *const kIsBookmark     = @"isBookmark";
NSString *const kAddress     = @"address";
NSString *const kEmail     = @"email";
NSString *const kPartnerUserType     = @"partnerUserType";
NSString *const kFullName     = @"fullName";
NSString *const kTelNumber     = @"telNumber";
NSString *const kMd5File     = @"md5";

NSString *const kStaffName     = @"staffName";
NSString *const kAmountNotTax     = @"amountNotTax";
NSString *const kAmountTax     = @"amountTax";
NSString *const kAttachFileName     = @"attachFileName";
NSString *const kContent     = @"content";

NSString *const kInvoiceDate     = @"invoiceDate";
NSString *const kInvoiceSerial     = @"invoiceSerial";
NSString *const kInvoiceSign     = @"invoiceSign";
NSString *const kTaxCode     = @"taxCode";
NSString *const kVat     = @"vat";
NSString *const kUnitIssue     = @"unitIssue";
NSString *const kSuggesstion     = @"suggesstion";
NSString *const kReportCode     = @"reportCode";
NSString *const kReportEnum     = @"reportEnum";
NSString *const kReportId     = @"reportId";
NSString *const kReportName     = @"reportName";
NSString *const kReportType     = @"reportType";
NSString *const kActionCode     = @"actionCode";
NSString *const kLogDate     = @"logDate";
NSString *const kUserUpdated     = @"userUpdated";
NSString *const kPartnerUserId     = @"partnerUserId";
NSString *const kMenu     = @"menu";
NSString *const kRole     = @"role";
NSString *const kObjectName     = @"objectName";
NSString *const kObjectCode     = @"objectCode";
NSString *const kComment     = @"comment";
NSString *const kProvider     = @"provider";
NSString *const kNote     = @"note";
NSString *const kReasonId     = @"reasonId";
NSString *const kReasonCode     = @"reasonCode";
NSString *const kReasonName     = @"reasonName";


// TuanTD7 them key many File jasper
NSString *const kJasperId = @"jasperId";
NSString *const kNumberFileJasper = @"numberFileJasper";
NSString *const kListJasperId     = @"listJasperId";
NSString *const kListJasperCode   = @"listJasperCode";
NSString *const kListJasperName   = @"listJasperName";
NSString *const kHasInvoice   = @"hasInvoice";

NSString *const VIP   = @"truonggiang";

NSString *const kDate   = @"date";










