//
//  RequestOperationManager.h
//  PhapLuat
//
//  Created by Tuanpk on 9/17/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "GCDAsyncSocket.h"

@interface RequestOperationManager : GCDAsyncSocket

+ (instancetype)sharedManager;

- (void) connecttoHost:(id) delegate;

@end
