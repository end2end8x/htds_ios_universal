//
//  ParametersBuilder.m
//  PhapLuat
//
//  Created by Tuanpk on 9/17/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "DictionaryBuilder.h"
#import "Defines.h"
#import "HTBaseCategories.h"
#import "HTServiceModel.h"
#import "HTContractModel.h"
#import "HTStatusModel.h"
#import "HTAccount.h"
#import "HTDocumentModel.h"
#import "HTInvoiceModel.h"
#import "HTReportTypeModel.h"
#import "PLFunctions.h"
#import "HTPreferences.h"
#import "HTPartnerModel.h"
#import "HTStaffModel.h"
#import "PLConstants.h"
#import "HTReasonModel.h"

@implementation DictionaryBuilder
#pragma mark - Login
+ (NSData*) dictionaryForLogin:(NSString*) userName
                   andPassWord:(NSString*) passWord {
    
    NSArray *array = [NSArray arrayWithObjects:convertParam(kUsername, userName),
                      convertParam(kPass, passWord), nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathLogin andMethod:kMethodPost andParams:array];
    
}

#pragma mark - Report
+ (NSData*) dictionarySearchAllReport:(NSInteger) offset
                             andLimit:(NSInteger) limit
                         andCycleDate:(NSString*) strDate
                            andStatus:(NSString*) status {
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    NSArray *array = nil;
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        array = [NSArray arrayWithObjects:convertParam(kStatus,status),
                 convertParam(kICCycleDate,strDate),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 nil];
    } else {
        array = [NSArray arrayWithObjects:convertParam(kStatus,status),
                 convertParam(kICCycleDate,strDate),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 convertParam(kPartnerId,[NSNumber numberWithInteger:account.partnerId]),
                 nil];
    }
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReport andMethod:kMethodPost andParams:array];
    
}

+ (NSData*) dictionarySearchReportWithStatus:(NSInteger) offset
                                    andLimit:(NSInteger) limit
                                   andStatus:(NSString*) status {
    
    HTAccount *accout = [HTPreferences sharedPreferences].account;
    
    NSArray *array = [NSArray arrayWithObjects:
                      convertParam(kStatus,status),
                      convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      convertParam(kPartnerId,[NSNumber numberWithInteger:accout.partnerId]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReport andMethod:kMethodPost andParams:array];
    
}

+ (NSData*) dictionarySearchReportViettelSign:(NSInteger) offset
                                     andLimit:(NSInteger) limit
                                    andStatus:(NSString*) status
                                   hasInvoice:(NSInteger) hasInvoice {
    
    HTAccount *accout = [HTPreferences sharedPreferences].account;
    
    NSArray *array = [NSArray arrayWithObjects:
                      convertParam(kStatus,status),
                      convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      convertParam(kPartnerId,[NSNumber numberWithInteger:accout.partnerId]),
                      convertParam(kHasInvoice, [NSNumber numberWithInteger:hasInvoice]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReportViettelSign andMethod:kMethodPost andParams:array];
    
}

+ (NSData*) dictionarySearchReportViettelSignStatus:(NSString*) status
                                        andContract:(NSString*) contractId
                                         andService:(NSString*) serviceId
                                     andIcCycleDate:(NSString*) icCycleDate
                                         andPartner:(NSString*) partnerId
                                        andUsername:(NSString*) username
                                         hasInvoice:(NSInteger) hasInvoice
                                          andOffset:(NSInteger) offset
                                           andLimit:(NSInteger) limit {
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    NSArray *array = nil;
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        array = [NSArray arrayWithObjects:
                 convertParam(kStatus, status),
                 convertParam(kContractId, contractId),
                 convertParam(kServiceId, serviceId),
                 convertParam(kICCycleDate, icCycleDate),
                 convertParam(kPartnerId, partnerId),
                 convertParam(kUsername, username),
                 convertParam(kHasInvoice, [NSNumber numberWithInteger:hasInvoice]),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 nil];
        
    } else {
        array = [NSArray arrayWithObjects:
                 convertParam(kStatus, status),
                 convertParam(kContractId, contractId),
                 convertParam(kServiceId, serviceId),
                 convertParam(kICCycleDate, icCycleDate),
                 convertParam(kPartnerId, [NSNumber numberWithInteger:account.partnerId]),
                 convertParam(kUsername, account.userName),
                 convertParam(kHasInvoice, [NSNumber numberWithInteger:hasInvoice]),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 nil];
    }
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReportViettelSign andMethod:kMethodPost andParams:array];
    
}


+ (NSData*) dictionarySearchUserHistory:(NSInteger) offset
                               andLimit:(NSInteger) limit
                           andCycleDate:(NSString*) strDate
                              andStatus:(NSString*) status {
    
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    NSArray *array = nil;
    
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        array = [NSArray arrayWithObjects:
                 convertParam(kICCycleDate,strDate),
                 convertParam(kStatus,@""),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 convertParam(kUsername,account.userName),
                 convertParam(kStatus,status),
                 nil];
    } else {
        array = [NSArray arrayWithObjects:
                 convertParam(kICCycleDate,strDate),
                 convertParam(kStatus,@""),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 convertParam(kUsername,account.userName),
                 convertParam(kPartnerId, [NSString stringWithFormat:@"%ld", account.partnerId]),
                 convertParam(kStatus,status),
                 nil];
    }
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchUserHistory andMethod:kMethodPost andParams:array];
    
}

+ (NSData*) dictionarySearchReportUserHistoryWithKeyWord:(NSString*) keyWord
                                     andCategoriesFilter:(NSArray*) categories
                                            andDateCharg:(NSString*) date
                                               andOffset:(NSInteger) offset
                                                andLimit:(NSInteger) limit {
    HTAccount *accout = [HTPreferences sharedPreferences].account;
    
    NSString *contractId = @"";
    NSString *serviceId  = @"";
    NSString *statusId   = @"";
    NSString *partnerId = @"";
    NSString *staffCode = @"";
    
    for (HTBaseCategories *object in categories) {
        if (object.objectId.integerValue != -1) {
            if ([object isKindOfClass:[HTServiceModel class]]) {
                serviceId = [object.objectId stringValue];
            } else if ([object isKindOfClass:[HTContractModel class]]) {
                contractId = [object.objectId stringValue];
            } else if ([object isKindOfClass:[HTStatusModel class]]) {
                statusId = [object.objectId stringValue];
            } else if ([object isKindOfClass:[HTPartnerModel class]]) {
                partnerId = [object.objectId stringValue];
            } else if ([object isKindOfClass:[HTStaffModel class]]) {
                HTStaffModel *staff = (HTStaffModel*) object;
                staffCode = staff.code;
            }
        }
    }
    NSArray *array = nil;
    if (accout.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        array = [NSArray arrayWithObjects:convertParam(kContractId,contractId),
                 convertParam(kServiceId,serviceId),
                 convertParam(kStatus,statusId),
                 convertParam(kICCycleDate,date),
                 convertParam(kContractName,keyWord),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 convertParam(kPartnerId,partnerId),
                 convertParam(kUsername,accout.userName),
                 convertParam(kStaffCode,staffCode),
                 
                 nil];
        
    } else {
        array = [NSArray arrayWithObjects:convertParam(kContractId,contractId),
                 convertParam(kServiceId,serviceId),
                 convertParam(kStatus,statusId),
                 convertParam(kICCycleDate,date),
                 convertParam(kContractName,keyWord),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 convertParam(kPartnerId,[NSNumber numberWithInteger:accout.partnerId]),
                 nil];
        
    }
    
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchUserHistory andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySearchHistoryReport:(NSInteger) offset
                                 andLimit:(NSInteger) limit
                                andReport:(id) report {
    HTDocumentModel *reportModel = (HTDocumentModel*) report;
    NSArray *array = [NSArray arrayWithObjects:
                      convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      convertParam(kICReportId,[NSNumber numberWithInteger:reportModel.icReportId]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReportHistory andMethod:kMethodPost andParams:array];
    
}


+ (NSData*) dictionarySearchContract:(NSInteger) offset
                            andLimit:(NSInteger) limit {
    HTAccount *accout = [HTPreferences sharedPreferences].account;
    NSArray *array = [NSArray arrayWithObjects:convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      convertParam(kPartnerId,[NSNumber numberWithInteger:accout.partnerId]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchContract andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySearchPartners:(NSInteger) offset
                            andLimit:(NSInteger) limit {
    NSArray *array = [NSArray arrayWithObjects:convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchPartner andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySearchPartnerUser:(NSInteger) offset
                               andLimit:(NSInteger) limit {
    NSArray *array = [NSArray arrayWithObjects:convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchPartnerUser andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySearchStaff:(NSString*)username andOffset:(NSInteger)offset
                         andLimit:(NSInteger)limit {
    NSArray *array = [NSArray arrayWithObjects:
                      convertParam(kUsername, username),
                      convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchStaff andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySearchService:(NSInteger) offset
                           andLimit:(NSInteger) limit {
    HTAccount *accout = [HTPreferences sharedPreferences].account;
    NSArray *array = [NSArray arrayWithObjects:convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      convertParam(kPartnerId,[NSNumber numberWithInteger:accout.partnerId]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchService andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySearchReportStatus:(NSString*) status
                             andContract:(NSString*) contractId
                              andService:(NSString*) serviceId
                          andIcCycleDate:(NSString*) icCycleDate
                              andPartner:(NSString*) partnerId
                                andStaff:(NSString*) staffId
                               andOffset:(NSInteger) offset
                                andLimit:(NSInteger) limit {
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    NSArray *array = nil;
    if (account.partnerUserType == PARTNER_TYPE_VIETTEL_VIP) {
        array = [NSArray arrayWithObjects:
                 convertParam(kStatus, status),
                 convertParam(kContractId, contractId),
                 convertParam(kServiceId, serviceId),
                 convertParam(kICCycleDate, icCycleDate),
                 convertParam(kPartnerId, partnerId),
                 convertParam(kStaffId, staffId),
                 convertParam(kUsername, account.userName),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 nil];
        
    } else {
        array = [NSArray arrayWithObjects:
                 convertParam(kStatus, status),
                 convertParam(kContractId, contractId),
                 convertParam(kServiceId, serviceId),
                 convertParam(kICCycleDate, icCycleDate),
                 convertParam(kPartnerId, [NSNumber numberWithInteger:account.partnerId]),
                 convertParam(kUsername, account.userName),
                 convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                 convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                 nil];
        
    }
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReport andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionaryForDownloadFileReport:(id) doccument
                                 andMd5File:(NSString*) md5File andJasperId:(NSString*)jasperId {
    
    HTDocumentModel *documment = (HTDocumentModel*) doccument;
    NSArray *array = [NSArray arrayWithObjects:convertParam(kICReportId,[NSNumber numberWithInteger:documment.icReportId]),
                      convertParam(kMd5File,md5File),
                      convertParam(@"jasperId", jasperId)
                      ,nil];
    return [DictionaryBuilder mutableDictionaryBase:kPathDownLoadFileReport andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionaryGetListInvoice:(id) doccument
                           andOffset:(NSInteger) offset
                            andLimit:(NSInteger) limit {
    HTAccount *accout = [HTPreferences sharedPreferences].account;
    HTDocumentModel *documment = (HTDocumentModel*) doccument;
    NSArray *array = [NSArray arrayWithObjects:convertParam(kICReportId,[NSNumber numberWithInteger:documment.icReportId]),
                      convertParam(kPartnerId,[NSNumber numberWithInteger:accout.partnerId]),
                      convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      nil];
    return [DictionaryBuilder mutableDictionaryBase:kPathGetListInvoice andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionaryForDowloadReportInvoice:(id) invoice
                                   andMd5File:(NSString*) md5File
                               andReportModel:(id) doccument {
    HTInvoiceModel  *invoiceModel = (HTInvoiceModel*) invoice;
    HTDocumentModel *doccumentModel = (HTDocumentModel*) doccument;
    NSArray *array = [NSArray arrayWithObjects:convertParam(kICReportId,[NSNumber numberWithInteger:doccumentModel.icReportId]),
                      convertParam(kMd5File,md5File),
                      convertParam(kAttachFileName,invoiceModel.attachFileName),
                      convertParam(kIdentifier,[NSNumber numberWithInteger:invoiceModel.invoiceId]),
                      nil];
    return [DictionaryBuilder mutableDictionaryBase:kPathDownloadFileInvoice andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionaryForAddOrUpdateReportInvoice:(id) invoice
                                 andDocumentModel:(id) document
                                   andFileContent:(NSString*) fileContent
                                       andMd5File:(NSString*) md5 {
    HTAccount *accountModel = [HTPreferences sharedPreferences].account;
    HTInvoiceModel  *invoiceModel = (HTInvoiceModel*) invoice;
    HTDocumentModel *doccumentModel = (HTDocumentModel*) document;
    
    if (invoiceModel.attachFileName == nil) {
        invoiceModel.attachFileName = @"";
    }
    
    NSString *strIdInvoice = (invoiceModel.invoiceId == -1?@"":[NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:invoiceModel.invoiceId]]);
    
    NSArray *array = [NSArray arrayWithObjects:convertParam(kAddress,invoiceModel.address),
                      convertParam(kAmountNotTax,[NSNumber numberWithInteger:invoiceModel.amountNotTax]),
                      convertParam(kAmountTax,[NSNumber numberWithInteger:invoiceModel.amountTax]),
                      convertParam(kAmountTotal,[NSNumber numberWithInteger:invoiceModel.amountTotal]),
                      convertParam(kContent,invoiceModel.content),
                      convertParam(kICReportId,[NSNumber numberWithInteger:doccumentModel.icReportId]),
                      convertParam(kInvoiceDate,invoiceModel.invoiceDate),
                      convertParam(kInvoiceSerial,invoiceModel.invoiceSerial),
                      convertParam(kInvoiceSign,invoiceModel.invoiceSign),
                      convertParam(kTaxCode,invoiceModel.taxCode),
                      convertParam(kVat,[invoiceModel.vat stringValue]),
                      convertParam(kUnitIssue,invoiceModel.unitIssue),
                      convertParam(kPartnerId,[NSNumber numberWithInteger:accountModel.partnerId]),
                      convertParam(kAttachFileName,invoiceModel.attachFileName),
                      convertParam(kMd5File,md5),
                      convertParam(kIdentifier,strIdInvoice),
                      nil];
    return [DictionaryBuilder mutableDictionaryBaseForUploadFile:kPathAddInvoice andMethod:kMethodPost andParams:array andDataFileBase64:fileContent];
}

+ (NSData*) dictionaryForExportReport:(id) service
                        andReportType:(id) reportType
                          andFromDate:(NSString*) fromDate
                              endDate:(NSString*) endDate
                           andMd5File:(NSString*) md5File {
    
    HTServiceModel *serviceModel = (HTServiceModel*) service;
    HTReportTypeModel *reportModel = (HTReportTypeModel*) reportType;
    HTAccount *accountModel = [HTPreferences sharedPreferences].account;
    NSArray *array = [NSArray arrayWithObjects:convertParam(kPartnerId,[NSNumber numberWithInteger:accountModel.partnerId]),
                      convertParam(kServiceId,serviceModel.objectId),
                      convertParam(kServiceName,serviceModel.titleDisplay),
                      convertParam(kFromDate,fromDate),
                      convertParam(kToDate,endDate),
                      convertParam(kUsername,accountModel.userName),
                      convertParam(kPartnerId,[NSNumber numberWithInteger:accountModel.partnerId]),
                      convertParam(kReportId,reportModel.objectId),
                      convertParam(kAttachFileName,@"pdf"),
                      convertParam(kMd5File,md5File),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathExportReport andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySearchReportType:(NSInteger) offset
                              andLimit:(NSInteger) limit {
    NSArray *array = [NSArray arrayWithObjects:convertParam(kOffset, [NSNumber numberWithInteger:offset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      nil];
    
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReportType andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionarySignatureReport:(id)report andPath:(NSString*)path andReason:(id)reason andNote:(NSString*)note {
    HTDocumentModel *document = (HTDocumentModel*) report;
    HTAccount *account = [HTPreferences sharedPreferences].account;
    
    if (!note) {
        note = @"";
    }
    NSString *reasoonCode = @"";
    NSString *reasoonName = @"";
    if (reason) {
        HTReasonModel *reasoonModel = (HTReasonModel*) reason;
        reasoonCode = reasoonModel.reasonCode;
        reasoonName = reasoonModel.titleDisplay;
    }
    
    
    NSArray *array = [NSArray arrayWithObjects:convertParam(kICReportId, [NSNumber numberWithInteger:document.  icReportId]),
                      convertParam(kTelNumber, account.tellNumber),
                      convertParam(kUsername, account.userName),
                      convertParam(kReasonCode, reasoonCode),
                      convertParam(kReasonName, reasoonName),
                      convertParam(kNote, note),
                      nil];
    return [DictionaryBuilder mutableDictionaryBase:path andMethod:kMethodPost andParams:array];
}

+ (NSData*) dictionaryReason:(NSInteger) limit andOffset:(NSInteger) ofset {
    NSArray *array = [NSArray arrayWithObjects:
                      convertParam(kOffset, [NSNumber numberWithInteger:ofset]),
                      convertParam(kLimit, [NSNumber numberWithInteger:limit]),
                      nil];
    return [DictionaryBuilder mutableDictionaryBase:kPathSearchReasons andMethod:kMethodPost andParams:array];
}

#pragma mark - Dictionary base
+ (NSData*) mutableDictionaryBase:(NSString*) path
                        andMethod:(NSString*) method
                        andParams:(NSArray*) listParams {
    NSString *userName = @"";
    NSString *session = @"";
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    if (account) {
        userName = account.userName;
        session = account.session;
    }
    
    NSMutableDictionary *dictBase = [NSMutableDictionary new];
    [dictBase setObject:kRest
                 forKey:kCode];
    [dictBase setObject:userName
                 forKey:kUsername];
    [dictBase setObject:session
                 forKey:kSession];
    [dictBase setObject:@{
                          kPath:path,
                          kMethod:method,
                          kParams:listParams
                          } forKey:kRest];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dictBase options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    myString = [NSString stringWithFormat:@"%@ \r\n",myString];
    NSLog(@"REQUEST_PARAMS:%@",myString);
    NSData* myData = [myString dataUsingEncoding:NSUTF8StringEncoding];
    return myData;
}

+ (NSData*) mutableDictionaryBaseForUploadFile:(NSString*) path
                                     andMethod:(NSString*) method
                                     andParams:(NSArray*) listParams
                             andDataFileBase64:(NSString*) fileContent {
    NSString *userName = @"";
    NSString *session = @"";
    
    HTAccount *account = [HTPreferences sharedPreferences].account;
    if (account) {
        userName = account.userName;
        session = account.session;
    }
    
    NSMutableDictionary *dictBase = [NSMutableDictionary new];
    [dictBase setObject:kRest
                 forKey:kCode];
    [dictBase setObject:userName
                 forKey:kUsername];
    [dictBase setObject:session
                 forKey:kSession];
    [dictBase setObject:@{
                          kPath:path,
                          kMethod:method,
                          kParams:listParams,
                          kBody:fileContent
                          } forKey:kRest];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dictBase options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    myString = [NSString stringWithFormat:@"%@ \r\n",myString];
    NSLog(@"REQUEST_PARAMS:%@",myString);
    NSData* myData = [myString dataUsingEncoding:NSUTF8StringEncoding];
    return myData;
}



@end
