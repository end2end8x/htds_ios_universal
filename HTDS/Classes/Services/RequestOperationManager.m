//
//  RequestOperationManager.m
//  PhapLuat
//
//  Created by Tuanpk on 9/17/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "RequestOperationManager.h"
#import "Defines.h"

@implementation RequestOperationManager

+ (instancetype) sharedManager {
    static RequestOperationManager *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[RequestOperationManager alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    });
    
    return _sharedClient;
}

- (void) connecttoHost:(id) delegate {
    [self setDelegate:nil];
    [self disconnect];
    self.delegate = delegate;
    NSError *err = nil;
    if (![self connectToHost:HTAPIBaseURLString onPort:HTPortConnect error:&err]) // Asynchronous!
    {
        // If there was an error, it's likely something like "already connected" or "no delegate set"
        NSLog(@"RequestOperationManager ERROR %@ ################### ", err);
    }
}

//+ (void) deleteCookie {
//    NSHTTPCookie *cookie;
//    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    for (cookie in [storage cookies]) {
//        [storage deleteCookie:cookie];
//    }
//}

//- (void)loadCachedSessionUid {
//    NSArray *storedCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:IMVAPIBaseURLString]];
//    for (NSHTTPCookie *cookie in storedCookies) {
//        if ([cookie.name isEqualToString:@"PHPSESSID"]) {
//            //            NSLog(@">>> %@", [NSString stringWithFormat:@"%@=%@", cookie.name, cookie.value]);
//            [self.requestSerializer setValue:[NSString stringWithFormat:@"%@=%@", cookie.name, cookie.value] forHTTPHeaderField:@"Cookie"];
//            break;
//        }
//    }
//}

@end
