//
//  ParametersBuilder.h
//  PhapLuat
//
//  Created by Tuanpk on 9/17/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DictionaryBuilder : NSObject

#pragma mark - Login
+ (NSData*) dictionaryForLogin:(NSString*) userName
                   andPassWord:(NSString*) passWord;

#pragma mark - Report
+ (NSData*) dictionarySearchAllReport:(NSInteger) offset
                          andLimit:(NSInteger) limit
                         andCycleDate:(NSString*) strDate
                            andStatus:(NSString*) status;

+ (NSData*) dictionarySearchReportWithStatus:(NSInteger) offset
                                     andLimit:(NSInteger) limit
                                    andStatus:(NSString*) status;

+ (NSData*) dictionarySearchReportStatus:(NSString*) status
                             andContract:(NSString*) contractId
                              andService:(NSString*) serviceId
                          andIcCycleDate:(NSString*) icCycleDate
                              andPartner:(NSString*) partnerId
                                andStaff:(NSString*) staffId
                               andOffset:(NSInteger) offset
                                andLimit:(NSInteger) limit;

+ (NSData*) dictionarySignatureReport:(id)report andPath:(NSString*)path andReason:(id)reason andNote:(NSString*)note;

+ (NSData*) dictionaryForDownloadFileReport:(id) doccument
                                 andMd5File:(NSString*) md5File andJasperId:(NSString*)jasperId;

#pragma mark - Report History
+ (NSData*) dictionarySearchHistoryReport:(NSInteger) offset
                                 andLimit:(NSInteger) limit
                                andReport:(id) report;

#pragma mark - Report Invoice
+ (NSData*) dictionaryForDowloadReportInvoice:(id) invoice
                                   andMd5File:(NSString*) md5File
                               andReportModel:(id) doccument;

+ (NSData*) dictionaryGetListInvoice:(id) doccument
                           andOffset:(NSInteger) offset
                            andLimit:(NSInteger) limit;

#pragma mark - Export
+ (NSData*) dictionarySearchReportType:(NSInteger) offset
                              andLimit:(NSInteger) limit;

+ (NSData*) dictionaryForExportReport:(id) service
                        andReportType:(id) reportType
                          andFromDate:(NSString*) fromDate
                              endDate:(NSString*) endDate
                           andMd5File:(NSString*) md5File;


#pragma mark - Viettel Sign
+ (NSData*) dictionarySearchReportViettelSign:(NSInteger) offset
                                    andLimit:(NSInteger) limit
                                   andStatus:(NSString*) status
                                   hasInvoice:(NSInteger) hasInvoice;

+ (NSData*) dictionarySearchReportViettelSignStatus:(NSString*) status
                                        andContract:(NSString*) contractId
                                         andService:(NSString*) serviceId
                                     andIcCycleDate:(NSString*) icCycleDate
                                         andPartner:(NSString*) partnerId
                                        andUsername:(NSString*) username
                                         hasInvoice:(NSInteger) hasInvoice
                                          andOffset:(NSInteger) offset
                                           andLimit:(NSInteger) limit;

#pragma mark - User History
+ (NSData*) dictionarySearchUserHistory:(NSInteger) offset
                               andLimit:(NSInteger) limit
                           andCycleDate:(NSString*) strDate
                              andStatus:(NSString*) status;

+ (NSData*) dictionarySearchReportUserHistoryWithKeyWord:(NSString*) keyWord
                               andCategoriesFilter:(NSArray*) categories
                                      andDateCharg:(NSString*) date
                                         andOffset:(NSInteger) offset
                                          andLimit:(NSInteger) limit;

#pragma mark - Search Report Params
+ (NSData*) dictionarySearchContract:(NSInteger) offset
                            andLimit:(NSInteger) limit;

+ (NSData*) dictionarySearchPartners:(NSInteger) offset
                            andLimit:(NSInteger) limit;

+ (NSData*) dictionarySearchPartnerUser:(NSInteger) offset
                               andLimit:(NSInteger) limit;

+ (NSData*) dictionarySearchStaff:(NSString*)username andOffset:(NSInteger)offset
                               andLimit:(NSInteger)limit;

+ (NSData*) dictionarySearchService:(NSInteger) offset
                           andLimit:(NSInteger) limit;

+ (NSData*) dictionaryReason:(NSInteger) limit andOffset:(NSInteger) ofset;


+ (NSData*) dictionaryForAddOrUpdateReportInvoice:(id) invoice
                                 andDocumentModel:(id) document
                                   andFileContent:(NSString*) fileContent
                                       andMd5File:(NSString*) md5;

+ (NSData*) mutableDictionaryBase:(NSString*) path
                        andMethod:(NSString*) method
                        andParams:(NSArray*) listParams;


@end
