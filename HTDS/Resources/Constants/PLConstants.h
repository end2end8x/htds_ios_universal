//
//  PLConstants.h
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//


#import <Foundation/Foundation.h>


// limt
extern const int LIMIT;
extern const int LIMIT_LOAD_CATEGORY;



// heght cell menu
extern const int ROW_HEIGHT_MENU_ITEM;
extern const int ROW_HEIGHT_DOCCOUMENT_ITEM;
extern const int ROW_HEIGHT_DOCCOUMENT_ITEM_INFO;
extern const int ROW_HEIGHT_104;


extern const int ROW_HEIGHT_INFOR_DOCUMENT_25;
extern const int ROW_HEIGHT_INFOR_DOCUMENT_36;
extern const int ROW_HEIGHT_INFOR_52;
extern const int ROW_HEIGHT_DOCUMENT_HISTORY_117;



// Notification keys
extern NSString *const kPLUserDidLoginNotification;
extern NSString *const kPLLeftMenuOpenNotification;
extern NSString *const kPLCheckBookMarksNotification;
extern NSString *const kPLGetAllServiceNotification;
extern NSString *const kPLAddInvoiceNotification;
extern NSString *const kPLSignatureReportNotification; 



// menuItem JsonKey
extern NSString *const KMenuClickable;
extern NSString *const KMenuLevel;
extern NSString *const KMenuClassName;




// Google Analytics
extern NSString *const kGoogleAnalyticsId;

// Database
extern NSString *const kDatabaseFileName;

// Key save Account
extern NSString *const KAppData;
extern NSString *const KSaveAccount;
extern NSString *const KPassword; 
extern NSString *const KCheckAccount;
extern NSString *const KPhoneNumberSaveUserDefault;



// font
extern NSString *const kFontArial;
extern NSString *const kFontArialBold; 


// size

extern const int SIZE_15;
extern const int SIZE_14;
extern const int SIZE_17; 
extern const int SIZE_09; 
// message
extern NSString *const kFolderReport;
extern NSString *const kFolderReportInvoice;
extern NSString *const kFolderExportReportCDR;
extern NSString *const kFolderExportReport; 


#define bgColorApp UIColorFromRGB(0x22936f)

// Macros
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBA(rgbValue, a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


// home

enum {
    TAB_REPORT,
    TAB_REPORT_HISTORY,
    TAB_REPORT_BOOKMARK,
    
};


enum {
    BUTTON_SIGN, // ký
    BUTTON_REFUSE, // từ chối
    BUTTON_FORSIGNED, // Huỷ ký
};

enum { 
    PARTNER_TYPE_MANAGER = 1,
    PARTNER_TYPE_EMPLOY,
    PARTNER_TYPE_ADMIN,
    PARTNER_TYPE_VIETTEL_VIP
};

enum {
    REPORT_TYPE_SERVICE = 8,
    REPORT_TYPE_CDR = 10
    
};

enum { // trạng thái biên bản
    // nhân viên 2: chờ duyệt -> 5 nhân viên duyệt biên bản
    // lãnh đạo: 5 chờ ký -> 7 lãnh đạo đối soát ký
    // anh Giang: 7 chờ ký -> 8 viettel đã ký
    
    // trạng thái đối soát
    STATUS_WAITING_REVIEW = 2, // trạng thái Chuyển nhân viên đối soát xác nhận
    STATUS_WAITING_SIGNATURE = 5,  // trạng thái Chuyển Lãnh đạo đối tác
    STATUS_MANAGER_SIGN = 7, //Biên bản được chuyển đến cho lãnh đạo trung tâm đối soát
    STATUS_VIETTEL = 8, // Biên bản được chuyển đến cho Phòng tài chính xem/nhận biên bản
    
    
    
    
    // các trạng thái khác
    STATUS_REPORT_NEWS = 0, // biên bản mới tạo
    STATUS_SWITCH_MANAGER = 1, // trạng thái Chuyển sang cho quản lý
    STATUS_SWITCH_PARTNER_USER_CONFIRM_TECHNICAL = 3, // trạng thái Chuyển nhân viên Kỹ thuật xác nhận
    STATUS_SWITCH_PARTNER_USER_FINANCE = 4, // trạng thái Chuyển nhân viên Tài chính xác nhận
    STATUS_SWITCH_PARTNER_USER_CONTROL_REVENUE = 6, // trạng thái Chuyển nhân viên Kiểm soát doanh thu
    STATUS_ACCEPTED = 9,  // Biên bản đã được nghiệm thu
    STATUS_SWITCH_MANAGER_BOARDS_FOR_CONTROL = 10 // Biên bản được chuyển đến cho lãnh đạo Ban đối soát ký/duyệt
};

enum {
    TYPE_REQUEST_EMPLOYER = 4,
};




