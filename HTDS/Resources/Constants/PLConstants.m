//
//  PLConstants.m
//  PhapLuat
//
//  Created by Tuanpk on 9/4/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "PLConstants.h"
#import <Foundation/Foundation.h>

// limit
const int LIMIT = 20;
const int LIMIT_LOAD_CATEGORY = 100;


// height cell menu

const int ROW_HEIGHT_MENU_ITEM = 44;
const int ROW_HEIGHT_DOCCOUMENT_ITEM = 135;
const int ROW_HEIGHT_DOCCOUMENT_ITEM_INFO = 22;
const int ROW_HEIGHT_104 = 104;

const int ROW_HEIGHT_INFOR_DOCUMENT_25 = 25;
const int ROW_HEIGHT_INFOR_DOCUMENT_36 = 36;
const int ROW_HEIGHT_INFOR_52          = 52;
const int ROW_HEIGHT_DOCUMENT_HISTORY_117 = 117;


// Notification keys
NSString *const kPLUserDidLoginNotification = @"kPLUserDidLoginNotification";
NSString *const kPLLeftMenuOpenNotification = @"kLeftMenuOpenNotification";
NSString *const kPLCheckBookMarksNotification = @"kPLCheckBookMarksNotification";
NSString *const kPLGetAllServiceNotification = @"kPLGetAllServiceNotification";
NSString *const kPLAddInvoiceNotification = @"kPLAddInvoiceNotification";
NSString *const kPLSignatureReportNotification = @"kPLSignatureReportNotification";



// menuItem JsonKey 
NSString *const KMenuClickable          = @"isClick";
NSString *const KMenuLevel              = @"level";
NSString *const KMenuClassName          = @"class_name";


// Google Analytics
NSString *const kGoogleAnalyticsId = @"UA-56051763-11";

// Database
NSString *const kDatabaseFileName = @"CoreData.sqlite";

// key save account
NSString *const KAppData = @"appData";
NSString *const KSaveAccount = @"HTAccount";
NSString *const KPassword     = @"password";
NSString *const KCheckAccount = @"hasAccountInfo";
NSString *const KPhoneNumberSaveUserDefault = @"KPhoneNumberSaveUserDefault";

// font

NSString *const kFontArial = @"ArialMT";
NSString *const kFontArialBold     = @"Arial-BoldMT";

// size

const int SIZE_15 = 15;
const int SIZE_14 = 14;
const int SIZE_17 = 17;
const int SIZE_09 = 9;


// foderName
NSString *const kFolderReport = @"ReportFile";
NSString *const kFolderReportInvoice = @"ReportInvoiceFile";
NSString *const kFolderExportReportCDR  = @"ExportReportCDR";
NSString *const kFolderExportReport  = @"ExportReport";

























