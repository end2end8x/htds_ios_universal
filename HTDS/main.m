//
//  main.m
//  HTDS
//
//  Created by TuanTD on 7/28/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HTAppDelegate class]));
    }
}
