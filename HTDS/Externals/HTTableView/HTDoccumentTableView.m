//
//  HTDoccumentTableView.m
//  HTDS
//
//  Created by Anhpt67 on 7/6/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "HTDoccumentTableView.h"
#import "HTDoccumentTableViewCell.h"
#import "HTHomeViewController.h"
#import "HTViettelSignViewController.h"

#import "PLConstants.h"

@implementation HTDoccumentTableView

- (void) awakeFromNib {
    [super awakeFromNib];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ROW_HEIGHT_DOCCOUMENT_ITEM;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *newsCellIdentifier = @"HTDoccumentTableViewCell";
    HTDoccumentTableViewCell *cell = (HTDoccumentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:newsCellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.controller = self.controller;
    HTDocumentModel *document = self.listData[indexPath.row];
    [cell setCellData:document];
    
    return cell;
}



@end
