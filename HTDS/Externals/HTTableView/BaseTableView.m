//
//  PLBaseTableView.m
//  PhapLuat
//
//  Created by Tuanpk on 11/11/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import "BaseTableView.h"


@implementation BaseTableView

- (void) awakeFromNib {
    self.delegate = self;
    self.dataSource = self;
    self.listData = [NSMutableArray array];  
    self.clipsToBounds = YES;
//    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - delegate tableview

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"didSelectRowAtIndexPath table: %@",self.description);
}



@end
