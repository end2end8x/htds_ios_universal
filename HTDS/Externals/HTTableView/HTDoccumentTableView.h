//
//  HTDoccumentTableView.h
//  HTDS
//
//  Created by Anhpt67 on 7/6/15.
//  Copyright (c) 2015 Viettel. All rights reserved.
//

#import "BaseTableView.h"

@interface HTDoccumentTableView : BaseTableView<UITableViewDataSource,UITableViewDelegate>

@end
