//
//  PLBaseTableView.h
//  PhapLuat
//
//  Created by Tuanpk on 11/11/14.
//  Copyright (c) 2014 VEGA Corp. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface BaseTableView : UITableView <UITableViewDataSource,UITableViewDelegate>


@property (nonatomic,weak) id controller;
@property (nonatomic,strong) NSMutableArray *listData;


@end
