//
//  HTMutableArray.h
//  HTDS
//
//  Created by TuanTD on 8/11/15.
//  Copyright (c) 2015 Tuanpk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTMutableArray : NSMutableArray


@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, assign) NSInteger tagPressed;

@end
