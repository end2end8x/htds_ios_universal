//
//  ConnectionErrorView.m
//  VideoClient
//
//  Created by Thao Nguyen Huy on 3/5/14.
//
//

#import "LostConnectionView.h"
#import "Reachability.h"


@implementation LostConnectionView

- (IBAction)retryButtonPressed:(id)sender {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus != NotReachable) {
        [self hideWithCompletionBlock:^(BOOL finished) {
            if ([_delegate conformsToProtocol:@protocol(LostConnectionViewDelegate)] &&
                [_delegate respondsToSelector:@selector(userDidPressRetry)]) {
                [_delegate userDidPressRetry];
            }
        }];        
    }
}

+ (void)showInController:(id <LostConnectionViewDelegate>)controller {
    LostConnectionView *errorView = [LostConnectionView lostConnectionView];
    errorView.tag = kLostConnectionViewTag;
    errorView.delegate = controller;
    errorView.frame = ((UIViewController *)controller).view.bounds;
    errorView.alpha = 0.0;
    [((UIViewController *)controller).view addSubview:errorView];
    
    [UIView animateWithDuration:0.2 animations:^{
        errorView.alpha = 1.0;
    }];
}

- (void)hideWithCompletionBlock:(void (^)(BOOL finished))block {
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if (block) {
            block(finished);
        }
    }];
}

+ (LostConnectionView *)lostConnectionView { 
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"LostConnectionView" owner:self options:nil];
    LostConnectionView *errorView = [views objectAtIndex:0];
    return errorView;
}

@end
