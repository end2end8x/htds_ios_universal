//
//  ConnectionErrorView.h
//  VideoClient
//
//  Created by Thao Nguyen Huy on 3/5/14.
//
//

#import <UIKit/UIKit.h>

static NSInteger kLostConnectionViewTag = 4774;

@class LostConnectionView;

@protocol LostConnectionViewDelegate <NSObject>

- (void)userDidPressRetry;

@end

@interface LostConnectionView : UIView

@property (nonatomic, assign) id <LostConnectionViewDelegate> delegate;

- (IBAction)retryButtonPressed:(id)sender;

+ (void)showInController:(id <LostConnectionViewDelegate>)controller;
- (void)hideWithCompletionBlock:(void (^)(BOOL finished))block;
+ (LostConnectionView *)lostConnectionView;

@end
